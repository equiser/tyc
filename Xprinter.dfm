�
 TPREVIEW 0�	  TPF0TPreviewPreviewLeftTop4WidthFHeight�CaptionVista PreviaColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style OldCreateOrder	Position	poDefaultWindowStatewsMaximizedOnCreate
FormCreateOnResize
FormResizePixelsPerInch`
TextHeight TPanelPanel1Left Top Width>Height%AlignalTop
BevelOuterbvNoneTabOrder  TSpeedButtonsbFirstLeftTopWidthHeightHint   Primera Página
AllowAllUp	Flat		NumGlyphsParentShowHintShowHint	OnClick
FirstClick  TSpeedButtonsbPriorLeft!TopWidthHeightHint   Página Anterior
AllowAllUp	Flat		NumGlyphsParentShowHintShowHint	OnClick
PriorClick  TSpeedButtonsbNextLeft=TopWidthHeightHint   Página Siguiente
AllowAllUp	Flat		NumGlyphsParentShowHintShowHint	OnClick	NextClick  TSpeedButtonsbLastLeftYTopWidthHeightHint   Ultima Página
AllowAllUp	Flat		NumGlyphsParentShowHintShowHint	OnClick	LastClick  TSpeedButtonsbPrintLeft~TopWidthHeightHintImprimir
AllowAllUp	Flat	ParentShowHintShowHint	OnClick
PrintClick  TSpeedButtonsbPrintPageLeft� TopWidthHeightHint   Imprimir Página
AllowAllUp	Flat	ParentShowHintShowHint	OnClickPrintPageClick  TSpeedButtonsbPageWidthLeft� TopWidthHeightHint   Ancho de Página
GroupIndexDown	Flat	ParentShowHintShowHint	OnClickPageWidthClick  TSpeedButton
sbPageFullLeft� TopWidthHeightHint   Página Completa
GroupIndexFlat	ParentShowHintShowHint	OnClickPageFullClick  TSpeedButtonsbCloseLeftTopWidthHeightHintCerrar Vista Previa
AllowAllUp	Flat		NumGlyphsParentShowHintShowHint	OnClick
CloseClick  TSpeedButtonpbSaveLeft� TopWidthHeightHintSalvar Pagina en Disco
AllowAllUp	Flat	ParentShowHintShowHint	OnClick	SaveClick   
TStatusBar	StatusBarLeft TopPWidth>HeightPanelsWidth�  BevelpbNoneWidth2    TPanelDesktopLeft Top%Width>Height+AlignalClient
BevelOuter	bvLoweredTabOrder 
TScrollBoxsbLeftTopWidth<Height)HorzScrollBar.IncrementVertScrollBar.IncrementAlignalClientBorderStylebsNoneTabOrder OnClickPaintBox1Click 	TPaintBox	PaintBox1Left� TopWidthmHeight�CursorcrCrossColorclWhiteParentColorOnClickPaintBox1ClickOnPaintPaintBox1Paint     