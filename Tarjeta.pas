Unit Tarjeta;

Interface

	Uses
		Classes, DBTables, Base, Codigo, Printers, IniFiles, SysUtils,
    Recuadro, Articulo, Imagen, Graphics, XPrinter, db;

  Const
    ctCDM21 = 0;
    ctCDM30 = 1;
    MaxNumObj = 50;

	Type
		TArrayRectangle = Array[1..MaxNumObj] Of TRectangle;
		TArrayTextBlock = Array[1..MaxNumObj] Of TTextBlock;
		TArrayXGraphic = Array[1..MaxNumObj] Of TXGraphic;

	Type
		TCard = Class(TArea)
		Private
			FMark: Boolean;
			FRCPType: Integer;
      FPrefix: String;
			FDisplayFormat: String;
			FBarCode: TBarCode;
			FRectangle: TArrayRectangle;
			FTextBlock: TArrayTextBlock;
			FXGraphic: TArrayXGraphic;
      FMColor: TColor;
			Procedure SetRCPType(Value: Integer);
      Procedure SetPrefix(Value: String);
		Public
			Property MColor:TColor Read FMColor Write FMColor;
			Property Mark:Boolean Read FMark Write FMark;
			Property RCPType: Integer Read FRCPType Write SetRCPType;
			Property DisplayFormat: String Read FDisplayFormat;
			Property BarCode:TBarCode Read FBarCode;
      Property Prefix: String Read FPrefix Write SetPrefix;
			Property Rectangle: TArrayRectangle Read FRectangle;
			Property TextBlock: TArrayTextBlock Read FTextBlock;
			Property XGraphic: TArrayXGraphic Read FXGraphic;

			Constructor Create;
			Procedure Assign(Source: TCard);
			Procedure Load(Source: TCustomIniFile);
			Procedure Print(Table: TTable; Destination: TXPrinter; Mirror: Boolean);
		End;

Implementation

  {--------------------------------------------------------------------------}
	Procedure TCard.SetRCPType;
	Begin
		If (Value > 0) And (Value < 2) Then FRCPType := Value;
	End;
  {--------------------------------------------------------------------------}
  Procedure TCard.SetPrefix;
  Begin
    if FRCPType = ctCDM21 then begin
     	if FBarCode.Validator(Value) then FPrefix := Copy(Value,1,5);
      //solo 5 digitos de pref.
    end else
     	if FBarCode.Validator(Value) then FPrefix := Copy(Value,1,7);
      //solo 7 digitos de pref.
	End;
	{-------------------------------------------------------------------------}
	Constructor TCard.Create;
	Var
		Count: Integer;
	Begin
		Inherited Create;

		Width := 85;
		Height := 55;
		FMark := False;
		FRCPType := ctCDM21;
		FDisplayFormat := '00000'; {CDM21}

		FBarCode := TBarCode.Create;
		For Count := 1 To MaxNumObj Do
			Begin
				FRectangle[Count] := TRectangle.Create;
				FTextBlock[Count] := TTextBlock.Create;
      	FXGraphic[Count] := TXGraphic.Create;
      End;
  End;
   {--------------------------------------------------------------------------}
	Procedure TCard.Assign;
  Var
   	Count: Integer;
  Begin
   	Inherited Assign(Source);
    FMark := Source.Mark;
    FRCPType := Source.RCPType;
		FDisplayFormat := Source.DisplayFormat;
		FBarCode.Assign(Source.BarCode);
    FPrefix := Source.Prefix;
    // importante: leer primero tipo RCP y luego prefijo
		For Count := 1 To MaxNumObj Do
			Begin
      	FRectangle[Count].Assign(Source.Rectangle[Count]);
      	FTextBlock[Count].Assign(Source.TextBlock[Count]);
				FXGraphic[Count].Assign(Source.FXGraphic[Count]);
			End;
	End;
	{--------------------------------------------------------------------------}
	Procedure TCard.Load;
	Var
		Count: Integer;
		Prefijo: String;
	Begin
		Inherited Load('Tarjeta', Source);

		FBarCode.Load('Codigo', Source);

		FMark := Source.ReadBool('Tarjeta', 'Marcas', False);
		FRCPType := Source.ReadInteger('Tarjeta', 'Tipo RCP', ctCDM21);
		Source.WriteInteger('Tarjeta', 'Tipo RCP', FRCPType);
		Prefix := Source.ReadString('Codigo', 'Prefijo', '');
    // importante: leer primero tipo RCP y luego prefijo

		For Count := 1 To MaxNumObj Do
		Begin
			FRectangle[Count].Load('Recuadro ' + IntToStr(Count), Source);
			FTextBlock[Count].Load('Marco ' + IntToStr(Count), Source);
			FXGraphic[Count].Load('Imagen ' + IntToStr(Count), Source)
		End;

		if RCPType = ctCDM21 then	FDisplayFormat := '00000'
		else begin
			FDisplayFormat := '0000 0000 0000';
			if FPrefix <> '' then begin
				Prefijo := FPrefix;
				if Length(Prefijo) > 4 then Insert(' ',Prefijo,5);
				if Length(Prefijo) > 9 then Insert(' ',Prefijo,10);
				FDisplayFormat :=  Prefijo + Copy(FDisplayFormat, Length(Prefijo) +1, Length(FDisplayFormat));
			end;
		end;
	End;
	{--------------------------------------------------------------------------}
	Procedure TCard.Print;
	Var
    Codigo: String;
		Count: Integer;
		Border: TBorder;
	Begin
		If FMark Then
		Begin
			Border := TBorder.Create;
			Border.Width := 0.1;
			Border.Color := 0;

/// Marcas hacia afuera
			Destination.Line(Left, Top, Left - 10, Top, Border);
/// Marcas hacia adentro
//			Destination.Line(Left, Top, Left - 10, Top, Border);
			Destination.Line(Left, Top, Left, Top + 10, Border);
/// Marcas hacia afuera
			Destination.Line(Left + Width, Top, Left + Width + 10, Top, Border);
/// Marcas hacia adentro
//			Destination.Line(Left + Width, Top, Left + Width - 10, Top, Border);
			Destination.Line(Left + Width, Top, Left + Width, Top + 10, Border);
/// Marcas hacia afuera
			Destination.Line(Left, Top + Height, Left - 10, Top + Height, Border);
/// Marcas hacia adentro
//			Destination.Line(Left, Top + Height, Left + 10, Top + Height, Border);
			Destination.Line(Left, Top + Height, Left, Top + Height - 10, Border);
/// Marcas hacia afuera
			Destination.Line(Left + Width, Top + Height, Left + Width + 10, Top + Height, Border);
/// Marcas hacia adentro
//			Destination.Line(Left + Width, Top + Height, Left + Width - 10, Top + Height, Border);
			Destination.Line(Left + Width, Top + Height, Left + Width, Top + Height - 10, Border);

			Border.Free;
		End;

		TFloatField(Table.FieldByName('Tarjeta')).EditFormat := '';
		TFloatField(Table.FieldByName('Tarjeta')).DisplayFormat := FDisplayFormat;

		For Count := 1 To MaxNumObj Do
		Begin
			FXGraphic[Count].Print(Left, Top, Table, Destination);
			FRectangle[Count].Print(Left, Top, Destination);
			FTextBlock[Count].Print(Left, Top, FMColor,Table, Destination);
		End;
		TFloatField(Table.FieldByName('Tarjeta')).EditFormat := '';
		if RCPType = ctCDM21 then
			TFloatField(Table.FieldByName('Tarjeta')).DisplayFormat := '0000000000'
		else
			TFloatField(Table.FieldByName('Tarjeta')).DisplayFormat := '000000000000';

    Codigo := TFloatField(Table.FieldByName('Tarjeta')).Text;
		Codigo := FPrefix + Copy(Codigo, Length(FPrefix) +1, Length(Codigo));
		FBarCode.Print (Left, Top, Codigo, Destination);

		TFloatField(Table.FieldByName('Tarjeta')).DisplayFormat := FDisplayFormat;
		TFloatField(Table.FieldByName('Tarjeta')).EditFormat := '00000';
	End;
	 {--------------------------------------------------------------------------}
Begin
End.
