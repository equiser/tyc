Unit Recuadro;

Interface

	Uses
		SysUtils, WinTypes, WinProcs, Classes, Graphics,IniFiles, Base, XPrinter;

	Type
		TRectangle = Class(TArea)
      Private
      	FRadius: Real;
         FBorder: TBorder;
         FFill: TFill;
         Procedure SetRadius(Value: Real);
      Public
      	Property Border: TBorder Read FBorder;
         Property Fill: TFill Read FFill;
			Property Radius: Real Read FRadius Write SetRadius;

      	Constructor Create;
         Procedure Assign(Source: TRectangle);
         Procedure Load(Section: String; Source: TCustomIniFile);
      	Procedure Print(OffsetX, OffsetY: Real; Destination: TXPrinter);
      End;

Implementation

	{Metodos Privados ---------------------------------------------------------}
   Procedure TRectangle.SetRadius;
   Begin
    	If Value >= 0 Then
      	If Width <= Height Then
         	If Value > (Width / 2) Then
            	FRadius := Width / 2
            Else
            	FRadius := Value
         Else
         	If Value > (Height / 2) Then
            	FRadius := Height / 2
            Else
            	FRadius := Value
      Else
      	Radius := 0;
   End;
	{Metodos Publicos ---------------------------------------------------------}
   Constructor TRectangle.Create;
   Begin
   	Inherited Create;
      FBorder := TBorder.Create;
      FFill := TFill.Create;
      FRadius := 0;
   End;

   Procedure TRectangle.Assign;
   Begin
   	Inherited Assign(Source);
      FBorder.Assign(Source.Border);
      FFill.Assign(Source.Fill);
      FRadius := Source.Radius;
   End;

   Procedure TRectangle.Load;
   Begin
   	Inherited Load(Section, Source);
      FRadius := Source.ReadInteger(Section, 'Radio', 0);
      FBorder.Load(Section, Source);
      FFill.Load(Section, Source);
   End;

	Procedure TRectangle.Print;
   Begin
   	If Visible Then
	      Destination.Rectangle(OffsetX + Left, OffsetY + Top,
         	OffsetX + Left + Width, OffsetY + Top + Height, Radius, Border, Fill);
   End;

Begin
End.
