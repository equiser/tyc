Unit Base;

Interface
	Uses
  	Classes, IniFiles, Graphics, SysUtils, WinProcs;

   Type
		TArea = Class(TObject)
    Private
      FVisible: Boolean;
      FLeft: Real;
      FTop: Real;
      FWidth: Real;
      FHeight: Real;

      Procedure SetLeft(Value: Real);
      Procedure SetTop(Value: Real);
      Procedure SetWidth(Value: Real);
      Procedure SetHeight(Value: Real);
    Public
      Property Visible: Boolean Read FVisible Write FVisible;
      Property Left:Real Read FLeft Write SetLeft;
      Property Top:Real Read FTop Write SetTop;
      Property Width:Real Read FWidth Write SetWidth;
      Property Height:Real Read FHeight Write SetHeight;

      Constructor Create;
      Procedure Assign(Source: TArea);
      Procedure Load(Section: String; Source: TCustomIniFile);
    End;

    TBorder = Class(TObject)
    Private
      FWidth: Real;
      FColor: TColor;
      Procedure SetWidth(Value: Real);
    Public
      Property Width: Real Read FWidth Write SetWidth;
      Property Color: TColor Read FColor Write FColor;

      Constructor Create;
      Procedure Assign(Source: TBorder);
      Procedure Load(Section: String; Source: TCustomIniFile);
    End;

   TFill = Class(TObject)
    Private
      FStyle: Byte;
      FColor: TColor;
      Procedure SetStyle(Value: Byte);
      Function GetBrushStyle: TBrushStyle;
    Public
      Property Style: Byte Read FStyle Write SetStyle;
      Property Color: TColor Read FColor Write FColor;
      Property BrushStyle: TBrushStyle Read GetBrushStyle;

      Constructor Create;
      Procedure Assign(Source: TFill);
      Procedure Load(Section:String; Source: TCustomIniFile);
    End;

    Function StrToReal(Cadena: String; Default: Real): Real;
    Function InStr(Cadena: String; Caracter: Char): Integer;
    Function UCase(Cadena: String): String;

    Procedure SetRValue(Var Color:TColor; Value: Byte);
    Procedure SetGValue(Var Color:TColor; Value: Byte);
    Procedure SetBValue(Var Color:TColor; Value: Byte);

Implementation

  Function StrToReal;
  Var
    Valor: Real;
    Error: Boolean;
    Posicion: Integer;
    Decimales: LongInt;
  Begin
    Valor := 0;
    Error := False;
    Decimales := 0;
    For Posicion := 1 To Length(Cadena) Do
      Case Cadena[Posicion] Of
         '0'..'9':
               If Decimales = 0 Then
                  Valor := 10 * Valor + Ord(Cadena[Posicion])- 48
               Else
               Begin
                  Valor := Valor + (Ord(Cadena[Posicion])- 48) / Decimales;
                  Decimales := 10 * Decimales
               End;
          '.': Decimales := 10;
          ' ':;
          Else Error := True;
    End;
    If Error Then StrToReal := Default Else StrToReal := Valor;
  End;

  Function InStr;
  Var
     Posicion: Integer;
  Begin
     If Length(Cadena) > 0 Then
    Begin
        Posicion := 1;
        While (Cadena[Posicion] <> Caracter) And (Posicion <= Length(Cadena)) Do
           Posicion := Posicion + 1;
        If Cadena[Posicion] = Caracter Then InStr := Posicion Else InStr := 0;
    End Else InStr := 0;
  End;

	Function UCase;
   Var
      Salida: String;
   	Posicion: Integer;
   Begin
   	Salida := Cadena;
   	For Posicion := 1 To Length(Cadena) Do
      	Salida[Posicion] := UpCase(Salida[Posicion]);
      UCase := Salida;
   End;

   Procedure SetRValue;
   Begin
   	Color := RGB(Value, GetGValue(Color), GetBValue(Color));
   End;

   Procedure SetGValue;
   Begin
   	Color := RGB(GetRValue(Color), Value, GetBValue(Color));
   End;

   Procedure SetBValue;
   Begin
   	Color := RGB(GetRValue(Color), GetGValue(Color), Value);
   End;

   { Definición del objeto TArea ---------------------------------------------}
	Procedure TArea.SetLeft;
   Begin
   	If Value >= 0 Then FLeft := Value;
   End;

	Procedure TArea.SetTop;
   Begin
   	If Value >= 0 Then FTop := Value;
   End;

   Procedure TArea.SetWidth;
   Begin
		If Value >= 0 Then FWidth := Value;
   End;

   Procedure TArea.SetHeight;
   Begin
		If Value >= 0 Then FHeight := Value;
   End;

	Constructor TArea.Create;
   Begin
   	Inherited Create;

   	FVisible := False;
   	FLeft := 0;
     FTop := 0;
   	FWidth := 0;
     FHeight := 0;
   End;

   Procedure TArea.Assign;
   Begin
   	FVisible := Source.Visible;
   	FTop := Source.Top;
     FLeft := Source.Left;
   	FWidth := Source.Width;
     FHeight := Source.Height;
   End;

   Procedure TArea.Load;
   Var
      Coma: Integer;
   	Cadena: String;
   Begin
   	FVisible := False;
   	FLeft := 0;
     FTop := 0;
   	FWidth := 0;
     FHeight := 0;

   	FVisible := Source.ReadBool(Section, 'Visible', False);

   	Cadena := Source.ReadString(Section, 'Posicion', '');
     Coma := InStr(Cadena,',');
     If Coma > 1 Then
       FLeft := StrToReal(Copy(Cadena, 1, Coma - 1), 0);
     If Coma < Length(Cadena) Then
       FTop := StrToReal(Copy(Cadena, Coma + 1, Length(Cadena)), 0);

   	Cadena := Source.ReadString(Section, 'Tamano', '');
     Coma := InStr(Cadena,',');
     If Coma > 1 Then
       FWidth := StrToReal(Copy(Cadena, 1, Coma - 1), 0);
     If Coma < Length(Cadena) Then
       FHeight := StrToReal(Copy(Cadena, Coma + 1, Length(Cadena)), 0);
   End;

   { Definición del objeto TBorder --------------------------------------------}
   Constructor TBorder.Create;
   Begin
   	Inherited Create;

   	FWidth := 0;
		FColor := clBlack;
   End;

   Procedure TBorder.SetWidth;
   Begin
		If Value > 0 Then FWidth := Value;
   End;

	Procedure TBorder.Assign;
   Begin
   	FWidth := Source.Width;
     FColor := Source.Color
   End;

   Procedure TBorder.Load;
   Var
      Coma: Integer;
      Cadena: String;
      Posicion: Integer;
      SubCadena: String;
   Begin
   	FWidth := 0;
   	FColor := clBlack;

   	Cadena := Source.ReadString(Section,'Borde','')+',';
   	Posicion := 0;
   	Coma := InStr(Cadena, ',');
   	While Coma <> 0 Do
     Begin
       Subcadena := Copy(Cadena, 1, Coma - 1);
       Cadena := Copy(Cadena, Coma + 1, Length(Cadena));
       Posicion := Posicion + 1;
       Coma := InStr(Cadena, ',');
       Case Posicion Of
         1: Width := StrToReal(Subcadena, 0);
         2: If Subcadena <> '' Then SetRValue(FColor, StrToInt(Subcadena));
         3: If Subcadena <> '' Then SetGValue(FColor, StrToInt(Subcadena));
         4: If Subcadena <> '' Then SetBValue(FColor, StrToInt(Subcadena));
       End;
     End;
   End;

   { Definición del objeto TFill ----------------------------------------------}
	 Procedure TFill.SetStyle;
   Begin
   	If (Value > 0) And (Value < 8) Then FStyle := Value;
   End;

   Function TFill.GetBrushStyle;
   Begin
		Case FStyle Of
			0: Result := bsClear;
        1: Result := bsSolid;
        2: Result := bsHorizontal;
        3: Result := bsVertical;
        4: Result := bsCross;
        5: Result := bsBDiagonal;
        6: Result := bsFDiagonal;
        7: Result := bsDiagCross
      End;
   End;

   Constructor TFill.Create;
   Begin
   	Inherited Create;

   	FStyle := 0;
		FColor := clBlack;
   End;

	Procedure TFill.Assign;
   Begin
   	FStyle := Source.Style;
     FColor := Source.Color;
   End;

   Procedure TFill.Load;
   Var
     Coma: Integer;
     Cadena: String;
     Posicion: Integer;
     SubCadena: String;
   Begin
   	FStyle := 0;
		FColor := clBlack;

   	Cadena := Source.ReadString(Section,'Relleno','')+',';
   	Posicion := 0;
   	Coma := InStr(Cadena, ',');
   	While Coma <> 0 Do
		Begin
      	Subcadena := Copy(Cadena, 1, Coma - 1);
      	Cadena := Copy(Cadena, Coma + 1, Length(Cadena));
      	Posicion := Posicion + 1;
      	Coma := InStr(Cadena, ',');
      	Case Posicion Of
	         1: If Subcadena <> '' Then
            		Style := StrToInt(Subcadena)
            	Else
						Style := 0;
         	2: If Subcadena <> '' Then
            		SetRValue(FColor, StrToInt(Subcadena))
               Else
               	SetRValue(FColor, 0);
         	3: If Subcadena <> '' Then
            		SetGValue(FColor, StrToInt(Subcadena))
            	Else
               	SetGValue(FColor, 0);
         	4: If Subcadena <> '' Then
            		SetBValue(FColor, StrToInt(Subcadena))
            	Else
               	SetBValue(FColor, 0);
       End;
    End;
  End;

Begin

End.
