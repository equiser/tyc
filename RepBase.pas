unit RepBase;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, Examinar, quickrpt, Qrctrls, ExtCtrls;

type
  TFormPreviewBase = class(TForm)
    QuickRepBaseDatos: TQuickRep;
    BandTitle: TQRBand;
    BandDetail: TQRBand;
    BandPageFooter: TQRBand;
    QRSysFecha: TQRSysData;
    QRSysBase: TQRSysData;
    QRLblBase: TQRLabel;
    QRDBLegajo: TQRDBText;
    BandColumnHeader: TQRBand;
    QRLblLegajo: TQRLabel;
    QRLblApellidoyNom: TQRLabel;
    QRLblTarjeta: TQRLabel;
    QRLblDocumento: TQRLabel;
    QRLblSeccion: TQRLabel;
    QRLblArea: TQRLabel;
    QRDBTarjeta: TQRDBText;
    QRDBDocumento: TQRDBText;
    QRDBSeccion: TQRDBText;
    QRDBArea: TQRDBText;
    QRSysData1: TQRSysData;
    BandChild: TQRChildBand;
    QRLblNombre: TQRLabel;
    QRDBApellido: TQRDBText;
    QRDBNombre: TQRDBText;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormPreviewBase: TFormPreviewBase;

implementation

{$R *.DFM}













end.

