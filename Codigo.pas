Unit Codigo;

Interface
	Uses Classes, Base, SysUtils, Printers, Graphics, IniFiles, XPrinter;

   Const
   	tcInterleave25 = 0;

   Type
   	TBarCode = Class(TArea)
    Private
     	FCodeType: Integer;
			FPrefix: String;
			FDelimiter: Boolean;
      FVerifier: Boolean;

      Procedure SetCodeType(Value: Integer);
      Procedure SetPrefix(Value: String);

      Function CheckSum(Value: String): String;
    	Procedure PrintI25(OffsetX, OffsetY: Real; Value: String; Destination: TXPrinter);
    	Procedure WriteI25(OffsetX, OffsetY: Real; Value: String; Destination: TXPrinter);
    Public
      Function Validator(Value: String): Boolean;
     	Property CodeType: Integer Read FCodeType Write SetCodeType;
      Property Prefix: String Read FPrefix Write SetPrefix;
      Property Delimiter: Boolean Read FDelimiter Write FDelimiter;
      Property Verifier: Boolean Read FVerifier Write FVerifier;

      Constructor Create;
      Procedure Assign(Source: TBarCode);
      Procedure Load(Section: String; Source: TCustomIniFile);
    	Procedure Print(OffsetX, OffsetY: Real; Value: String; Destination: TXPrinter);
    End;

Implementation
	{Metodos Privados ---------------------------------------------------------}
	Procedure TBarCode.SetCodeType;
  Begin
   	If Value = 0 Then FCodeType := Value;
	End;
  {--------------------------------------------------------------------------}
 Procedure TBarCode.SetPrefix;
  Begin
   	If Validator(Value) Then FPrefix := Value;
	End;
	{-------------------------------------------------------------------------}
	Function TBarCode.CheckSum;
	Var
		Resta: LongInt;
		Count: Integer;
	Begin
    if Verifier then begin
  		Resta := 0;
	  	For Count := 1 To Length(Value) Do
		  	Resta := Resta + (Ord(Value[Count]) - 48) * (1 + 2 * (Count Mod 2));
  		Result := Value + '0' + IntToStr((10 - (Resta Mod 10)) Mod 10);
    end else begin
      Result := Value;
    end;
	End;
	{-------------------------------------------------------------------------}
	procedure TBarCode.WriteI25;
	const
   	Definition: array[0..9] of String = ('00110','10001','01001',
      	'11000','00101','10100','01100','00011','10010','01010');
	var
    X: Real;
    Bar: Integer;
    Digit: Integer;
    Encoded: String;
    NewFont: TFont;
  begin
  	if Delimiter then Encoded := '0      0 0 ' else Encoded := '0 0 ';
		Digit := 1;
    while Digit <= Length(Value) do begin
    	for Bar := 1 to 5 do begin
				if Definition[StrToInt(Value[Digit]), Bar] = '0' then
					Encoded := Encoded + '0'
				else Encoded := Encoded + '1';
				if Definition[StrToInt(Value[Digit + 1]), Bar] = '0' then
					Encoded := Encoded + ' '
				else Encoded := Encoded + '  ';
      end;
      Digit := Digit + 2;
    end;
  	if Delimiter then Encoded := Encoded + '1 0      0'
    else Encoded := Encoded + '1 0';

      with Destination do begin
        NewFont := TFont.Create;
        try
          NewFont.Name := 'Bar Code Interleave 25';
          NewFont.Color := clBlack;
          NewFont.Size := 10;
          Font := NewFont;
          NewFont.Size := 10 * XMilimeters(Width) div TextWidth(Encoded);
          Font := NewFont;
        finally
          NewFont.Free;
        end;

        X := (Width - MilimetersX(TextWidth(Encoded))) / 2;
        for Bar := 0 to YMilimeters(Height) div (TextHeight(Encoded) div 2) - 1 do
{          TextRect(OffsetX + Left + X,
                   OffsetY + Top + MilimetersY(Bar * TextHeight(Encoded)),
                   OffsetX + Left,
                   OffsetY + Top ,
                   OffsetX + Left + Width,
                   OffsetY + Top + Height, Encoded);}
          TextOut(OffsetX + Left + X,
                   OffsetY + Top + MilimetersY(Bar * (TextHeight(Encoded) div 2)),
                   Encoded);
        TextOut(OffsetX + Left + X,
                OffsetY + Top + Height - MilimetersY(TextHeight(Encoded)),
                Encoded);
      end;{-with Destination-}
  end;
	{-------------------------------------------------------------------------}
	Procedure TBarCode.PrintI25;
	Const
   	Definition: Array[-1..10] Of String = ('00','00110','10001','01001',
      	'11000','00101','10100','01100','00011','10010','01010','10');
	Var
    X, Y: Real;
    Module: Real;
    Bar: Integer;
    Fill: TFill;
    Digit: Integer;
  Begin
    Fill := TFill.Create;
    Fill.Style := 1;
    Fill.Color := clBlack;

		X := OffsetX + Left; Y := OffsetY + Top;
   	If Delimiter Then	Module := Width / (24 + 7 * Length(Value))
    Else Module := Width / (10 + 7 * Length(Value));

		{ Impresión del Preambulo de Digicard ----------------------------------}
    If Delimiter Then
    Begin
       Destination.FillArea (X, Y, X + Module, Y + Height, Fill);
       X := X + 7 * Module;
    End;
    { Impresión del Inicio del Codigo --------------------------------------}
    Destination.FillArea (X, Y, X + Module, Y + Height, Fill);
    X := X + 2 * Module;
    Destination.FillArea (X, Y, X + Module, Y + Height, Fill);
    X := X + 2 * Module;
    { Impresión del Codigo -------------------------------------------------}
    Digit := 1;
    Repeat
    	For Bar := 1 To 5 Do
      Begin
        Destination.FillArea (X, Y, X + Module, Y + Height, Fill);
        X := X + Module;
        If Definition[StrToInt(Value[Digit]), Bar] = '1' Then
        Begin
	        Destination.FillArea (X, Y, X + Module, Y + Height, Fill);
          X := X + Module;
        End;
        X := X + Module;
	      If Definition[StrToInt(Value[Digit + 1]), Bar] = '1' Then X := X + Module;
       End;
      Digit := Digit + 2
    Until Digit > Length(Value);
    { Impresión del Final del Codigo ---------------------------------------}
    Destination.FillArea (X, Y, X + Module, Y + Height, Fill);
    X := X + Module;
    Destination.FillArea (X, Y, X + Module, Y + Height, Fill);
    X := X + 2 * Module;
    Destination.FillArea (X, Y, X + Module, Y + Height, Fill);
    X := X + Module;
		{ Impresión del Preambulo de Digicard ----------------------------------}
    If Delimiter Then
   	Begin
      X := X + 6 * Module;
      Destination.FillArea (X, Y, X + Module, Y + Height, Fill);
    End;
    Fill.Free;
  End;

  {Metodos Publicos --------------------------------------------------------}
	Function TBarCode.Validator;
	Const
		I25: Set Of Char = ['0'..'9'];
	Var
		Valid: Boolean;
		Count: Integer;
	Begin
		Valid := True;
		Case CodeType Of
			tcInterleave25:
					For Count := 1 To Length(Value) Do
							Valid := Valid And (Value[Count] In I25);
		End;
		Result := Valid;
	End;
	{-------------------------------------------------------------------------}
  Constructor TBarCode.Create;
  Begin
    Inherited Create;
    FCodeType := 0;
    FPrefix := '';
    FDelimiter := True;
    FVerifier := True;

    Left := 15; Top := 52;
    Width := 55; Height := 10;
  End;
  {-------------------------------------------------------------------------}
  Procedure TBarCode.Assign;
  Begin
    Inherited Assign(Source);
    FCodeType := Source.CodeType;
    FPrefix := Source.Prefix;
    FDelimiter := Source.Delimiter;
    FVerifier := Source.Verifier;
  End;
	{---------------------------------------------------------------------------}
  Procedure TBarCode.Load;
  Begin
   	Inherited Load(Section, Source);
    FCodeType := Source.ReadInteger(Section, 'Tipo', 0);
    FPrefix := Source.ReadString(Section, 'Prefijo', '');
    FDelimiter := Source.ReadBool(Section, 'Preambulo', True);
    FVerifier := Source.ReadBool(Section, 'Verificador', True);
  End;
  {-------------------------------------------------------------------------}
	Procedure TBarCode.Print;
  Begin
   	If Visible And Validator(Value) Then
    Begin
	   	Case CodeType Of
	      	tcInterleave25: Begin
						If Length(Value) Mod 2 = 0 Then
(*               	PrintI25(OffsetX, OffsetY, CheckSum(Value), Destination);*)
               	WriteI25(OffsetX, OffsetY, CheckSum(Value), Destination);
          End;
	    End;
    End;
  End;
End.
