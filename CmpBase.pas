unit CmpBase;

interface

	uses Windows, Controls, Classes, Forms, ExtCtrls;

	const
    MINZOOM = 0.25;
    MAXZOOM = 4;
    MINSCAL = 0;
    MAXSCAL = 10000;

		mpMoveResize = -1;
		mpNone = 0;
		mpResize = 1;

	type
    TAreaState = (asSelected,asMoving,asResizing);
	 	TMovePosibility = mpMoveResize..mpResize;

    TAreaStateEvent = procedure(Sender: TObject; State: TAreaState;
                             const Left, Top, Width, Height: Single) of object;

		// Clase base para la edicion en tiempo de ejecución
  	TEdicion = class;
  	TArea = class(TGraphicControl)
  	private
			fScale: Single;
			fScaledLeft: Single;
			fScaledTop: Single;
			fScaledWidth: Single;
			fScaledHeight: Single;
			fZoom: Single;
			fDraft: Boolean;
			fEditor: TEdicion;
			procedure SetZoom(Value: Single);
			procedure SetScale(Value: Single);
			function GetLeft: Integer;
			procedure SetLeft(Value: Integer);
			procedure SetScaledLeft(Value: Single);
			function GetTop: Integer;
			procedure SetTop(Value: Integer);
			procedure SetScaledTop(Value: Single);
			function GetWidth: Integer;
			procedure SetWidth(Value: Integer);
			procedure SetScaledWidth(Value: Single);
			function GetHeight: Integer;
			procedure SetHeight(Value: Integer);
			procedure SetScaledHeight(Value: Single);
			function GetBoundsRect:TRect;
			procedure SetBoundsRect(Value: TRect);
	    procedure SetDraft(Value: Boolean);
			procedure SetEditor(Value: tEdicion);
			procedure Paint; override;
		public
			constructor Create(AOwner: TComponent); override;
		published
			property Zoom: Single read FZoom write SetZoom;
			property Scale: Single read FScale write SetScale;
			property Left: integer read GetLeft write SetLeft;
			property ScaledLeft: Single read FScaledLeft Write SetScaledLeft;
			property Top: integer read GetTop write SetTop;
			property ScaledTop: Single read FScaledTop write SetScaledTop;
			property Width: integer read GetWidth write SetWidth;
			property ScaledWidth: Single read FScaledWidth write SetScaledWidth;
			property Height: integer read GetHeight write SetHeight;
			property ScaledHeight: Single read FScaledHeight write SetScaledHeight;
			property BoundsRect: TRect read GetBoundsRect write SetBoundsRect;
	    property Draft: Boolean read FDraft write SetDraft;
			property Editor: tEdicion read fEditor write SetEditor;
		end;

		// Clase base para las equinas del objeto Edicion
		TEsquina = class(TPanel)
		public
			Vertical: tMovePosibility;
			Horizontal: tMovePosibility;
			constructor Create(AOwner: TEdicion);
		end;
    // Clase auxiliar del objeto Edicion
    TStatusLabels = class(TPersistent)
    	private
	    	FLeft: string;
  	    FTop : string;
	      FWidth : string;
	      FHeight: string;
  	    FSeparator: string;
    	published
      	procedure Assign(Source: TPersistent);
	    	property Left: string read FLeft write FLeft;
	    	property Top: string read FTop write FTop;
	    	property Width: string read FWidth write FWidth;
	    	property Height: string read FHeight write FHeight;
	    	property Separator: string read FSeparator write FSeparator;
    end;
		// Clase para la edición en tiempo de ejecución de objetos tBase
		TEdicion = class(TComponent)
		private
			FXo: Integer;
			FYo: Integer;
			FSupIzq: TEsquina;
			FSupMed: TEsquina;
			FSupDer: TEsquina;
			FMedIzq: TEsquina;
			FMedDer: TEsquina;
			FInfIzq: TEsquina;
			FInfMed: TEsquina;
			FInfDer: TEsquina;
			FChildren: TArea;
			FSizeStatus: TPanel;
			FPositionStatus: TPanel;
      FLabels: TStatusLabels;
      FOnStateChange: TAreaStateEvent;
			procedure RefreshControls;
      procedure RefreshStatus(ChildState: TAreaState);
      procedure SetLabels(Value: TStatusLabels);
		public
    	constructor Create(AOwner: TComponent); override;
			procedure Select(Sender: TArea);
      procedure Click(Sender: TObject);
			procedure MouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
      procedure MouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
			procedure MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
			property Children: TArea read FChildren;
    published
			property SizeStatus: TPanel read FSizeStatus write FSizeStatus;
			property PositionStatus: TPanel read FPositionStatus write FPositionStatus;
      property Labels: TStatusLabels read FLabels write SetLabels;
      property OnStateChange: TAreaStateEvent read FOnStateChange write FOnStateChange;
		end;

    // Procedimiento para registrar los componentes
    procedure Register;

implementation

	uses Graphics, Sysutils;

  // Implementación del objeto Area
	//----------------------------------------------------------------------------
	constructor TArea.Create;
	begin
		inherited Create(AOwner);
		FZoom := 1;
		FScale := 1;
    If Width = 0 Then Width := 65;
    If Height = 0 Then Height := 65;

		FScaledLeft := inherited Left / (Scale * Zoom);
		FScaledTop := inherited Top / (Scale * Zoom);
		FScaledWidth := inherited Width / (Scale * Zoom);
		FScaledHeight := inherited Height / (Scale * Zoom);
    FDraft := False;
	end;
	//----------------------------------------------------------------------------
	procedure TArea.SetZoom;
	begin
		if (Value >= MINZOOM) And (Value <= MAXZOOM) then
		begin
			FZoom := Value;
			inherited Left := Round(ScaledLeft * (Scale * Zoom));
			inherited Top := Round(ScaledTop * (Scale * Zoom));
			inherited Width := Round(ScaledWidth * (Scale * Zoom));
			inherited Height  := Round(ScaledHeight * (Scale * Zoom));
		end;
	end;
	//----------------------------------------------------------------------------
	procedure TArea.SetScale;
	begin
		If (Value >= MINSCAL) And (Value <= MAXSCAL) Then
		Begin
			FScale := Value;
			inherited Left := Round(ScaledLeft * (Scale * Zoom));
			inherited Top := Round(ScaledTop * (Scale * Zoom));
			inherited Width := Round(ScaledWidth * (Scale * Zoom));
			inherited Height  := Round(ScaledHeight * (Scale * Zoom));
		End;
	end;
	//----------------------------------------------------------------------------
	function TArea.GetLeft;
	begin
		Result := inherited Left
	end;

	procedure TArea.SetLeft;
	begin
		inherited Left := Value;
		FScaledLeft := inherited Left / (Scale * Zoom);
	end;
	//----------------------------------------------------------------------------
	procedure TArea.SetScaledLeft;
	begin
		If Value > 0 Then
		Begin
			FScaledLeft := Value;
			inherited Left := Round(FScaledLeft * (Scale * Zoom));
		End;
	end;
	//----------------------------------------------------------------------------
	function TArea.GetTop;
	begin
		Result := inherited Top
	end;
	//----------------------------------------------------------------------------
	procedure TArea.SetTop;
	begin
		inherited Top := Value;
		FScaledTop := inherited Top / (Scale * Zoom);
	end;
	//----------------------------------------------------------------------------
	procedure TArea.SetScaledTop;
	begin
		If Value > 0 Then
		Begin
			FScaledTop := Value;
			inherited Top := Round(FScaledTop * (Scale * Zoom));
		End;
	end;
	//----------------------------------------------------------------------------
	function TArea.GetWidth;
	begin
		Result := inherited Width
	end;
	//----------------------------------------------------------------------------
	procedure TArea.SetWidth;
	begin
		inherited Width := Value;
		FScaledWidth := inherited Width / (Scale * Zoom);
	end;
	//----------------------------------------------------------------------------
	procedure TArea.SetScaledWidth;
	begin
		If Value > 0 Then
		Begin
			FScaledWidth := Value;
			inherited Width := Round(ScaledWidth * (Scale * Zoom));
		End;
	end;
	//----------------------------------------------------------------------------
	function TArea.GetHeight;
	begin
		Result := inherited Height
	end;
	//----------------------------------------------------------------------------
	procedure TArea.SetHeight;
	begin
		inherited Height := Value;
		FScaledHeight := inherited Height / (Scale * Zoom);
	end;
	//----------------------------------------------------------------------------
	procedure TArea.SetScaledHeight;
	begin
		If Value > 0 Then
		Begin
			FScaledHeight := Value;
			inherited Height  := Round(ScaledHeight * (Scale * Zoom));
		End;
	end;
	//----------------------------------------------------------------------------
	function TArea.GetBoundsRect;
	begin
		Result := inherited BoundsRect;
	end;
	//----------------------------------------------------------------------------
	procedure TArea.SetBoundsRect;
	begin;
		inherited BoundsRect := Value;
		FScaledLeft := inherited Left / (Scale * Zoom);
		FScaledTop := inherited Top / (Scale * Zoom);
		FScaledWidth := inherited Width / (Scale * Zoom);
		FScaledHeight := inherited Height / (Scale * Zoom);
	end;
	//----------------------------------------------------------------------------
  procedure TArea.SetDraft;
  begin
  	if FDraft <> Value then begin// evitamos que se repinte si no es necesario
      FDraft := Value;
      Repaint;
    end;  
  end;
	//----------------------------------------------------------------------------
  procedure TArea.SetEditor;
  begin
  	fEditor := Value;
    if Value <> nil then
    	OnClick := Value.Click
    else
    	OnClick := nil;
  end;
	//----------------------------------------------------------------------------
	procedure TArea.Paint;
  begin
  	inherited;
		if Draft then with Canvas do begin
    	Pen.Color := clGray;
	    Pen.Mode := pmXor;
			Pen.Width := 3;
			Brush.Style := bsClear;
			Rectangle(0, 0, Width, Height);
    end;
  	{if (fEditor <> nil) and (fEditor.Children = Self) then fEditor.Paint}
  end;
	//----------------------------------------------------------------------------
	// Implementación del Objeto Etiquetas de Estado
	//----------------------------------------------------------------------------
	procedure TStatusLabels.Assign;
  begin
	  if Source is TStatusLabels then begin
	  		FLeft := TStatusLabels(Source).Left;
		    FTop  := TStatusLabels(Source).Top;
		    FWidth  := TStatusLabels(Source).Width;
		    FHeight := TStatusLabels(Source).Height;
		    FSeparator := TStatusLabels(Source).Separator;
    end else
		  inherited Assign(Source);
	end;
	//----------------------------------------------------------------------------
	// Implementación del Objeto Esquina
	//----------------------------------------------------------------------------
	constructor TEsquina.Create;
	begin
		inherited Create(AOwner);
	  Width := 6;
		Height := 6;
		Color := clBlack;
		BevelOuter := bvNone;

		Vertical := mpNone;
		Horizontal := mpNone;
		Visible := False;

	  OnMouseDown := AOwner.MouseDown;
	  OnMouseMove := AOwner.MouseMove;
		OnMouseUp := AOwner.MouseUp;
	end;

	//----------------------------------------------------------------------------
	// Implementación del Objeto para Edicion
	//----------------------------------------------------------------------------
	constructor TEdicion.Create;
  begin
  	inherited Create(AOwner);
  	FLabels := TStatusLabels.Create;
  end;
	//----------------------------------------------------------------------------
	procedure TEdicion.Select;
	begin
		if FChildren <> nil then begin
			FChildren.OnMouseUp := nil;
			FChildren.OnMouseDown := nil;
			FChildren.OnMouseMove := nil;
			FSupIzq.Free;
			FSupMed.Free;
			FSupDer.Free;
			FMedIzq.Free;
			FMedDer.Free;
			FInfIzq.Free;
			FInfMed.Free;
			FInfDer.Free;
		end;

		FChildren := TArea(Sender);

	  if FChildren <> nil then begin
			FChildren.OnMouseUp := MouseUp;
			FChildren.OnMouseDown := MouseDown;
			FChildren.OnMouseMove := MouseMove;

		  FSupIzq := TEsquina.Create(Self);
			FSupIzq.Parent := fChildren.Parent;
			FSupIzq.Cursor := crSizeNWSE;
			FSupIzq.Horizontal := mpMoveResize;
			FSupIzq.Vertical := mpMoveResize;

	  	FSupMed := TEsquina.Create(Self);
			FSupMed.Parent := fChildren.Parent;
			FSupMed.Cursor := crSizeNS;
		  FSupMed.Horizontal := mpNone;
		  FSupMed.Vertical := mpMoveResize;

		  FSupDer := TEsquina.Create(Self);
			FSupDer.Parent := fChildren.Parent;
			FSupDer.Cursor := crSizeNESW;
		  FSupDer.Horizontal := mpResize;
		  FSupDer.Vertical := mpMoveResize;

		  FMedIzq := TEsquina.Create(Self);
			FMedIzq.Parent := fChildren.Parent;
			FMedIzq.Cursor := crSizeWE;
		  FMedIzq.Horizontal := mpMoveResize;
		  FMedIzq.Vertical := mpNone;

		  FMedDer := TEsquina.Create(Self);
			FMedDer.Parent := fChildren.Parent;
			FMedDer.Cursor := crSizeWE;
			FMedDer.Horizontal := mpResize;
			FMedDer.Vertical := mpNone;

			FInfIzq := TEsquina.Create(Self);
			FInfIzq.Parent := fChildren.Parent;
			FInfIzq.Cursor := crSizeNESW;
		  FInfIzq.Horizontal := mpMoveResize;
			FInfIzq.Vertical := mpResize;

			FInfMed := TEsquina.Create(Self);
			FInfMed.Parent := fChildren.Parent;
			FInfMed.Cursor := crSizeNS;
		  FInfMed.Horizontal := mpNone;
		  FInfMed.Vertical := mpResize;

			FInfDer := TEsquina.Create(Self);
			FInfDer.Parent := fChildren.Parent;
			FInfDer.Cursor := crSizeNWSE;
			FInfDer.Horizontal := mpResize;
			FInfDer.Vertical := mpResize;

      RefreshStatus(asSelected);
		end;
	end;
	//----------------------------------------------------------------------------
	procedure TEdicion.RefreshControls;
	begin
		if not FChildren.Draft then begin
			FSupIzq.Left := FChildren.Left - 3;
			FSupIzq.Top := FChildren.Top - 3;
	  	FSupIzq.Visible := True;
	  	FSupMed.Left := FChildren.Left + (FChildren.Width - 6) Div 2;
			FSupMed.Top := FChildren.Top - 3;
			FSupMed.Visible := True;
			FSupDer.Left := FChildren.Left + FChildren.Width - 3;
			FSupDer.Top := FChildren.Top - 3;
			FSupDer.Visible := True;

			FMedIzq.Left := FChildren.Left - 3;
			FMedIzq.Top := FChildren.Top + (FChildren.Height - 6) Div 2;
			FMedIzq.Visible := True;
			FMedDer.Left := FChildren.Left + FChildren.Width - 3;
			FMedDer.Top := FChildren.Top + (FChildren.Height - 6) Div 2;
			FMedDer.Visible := True;

			FInfIzq.Left := FChildren.Left - 3;
			FInfIzq.Top := FChildren.Top + FChildren.Height - 3;
			FInfIzq.Visible := True;
			FInfMed.Left := FChildren.Left + (FChildren.Width - 6) Div 2;
			FInfMed.Top := FChildren.Top + FChildren.Height - 3;
			FInfMed.Visible := True;
			FInfDer.Left := FChildren.Left + FChildren.Width - 3;
			FInfDer.Top := FChildren.Top + FChildren.Height - 3;
			FInfDer.Visible := True;
		end;
  end;
	//----------------------------------------------------------------------------
	procedure TEdicion.RefreshStatus;
  begin
    if not (csDesigning in ComponentState) and Assigned(OnStateChange) then
      OnStateChange(Self, ChildState, FChildren.Left, FChildren.Top,
                                     FChildren.Width,FChildren.Height);


	  if FPositionStatus <> nil then
			FPositionStatus.Caption := FLabels.Left + FloatToStr(FChildren.Left) + FLabels.Separator +
      													 FLabels.Top  + FloatToStr(FChildren.Top);
	  if FSizeStatus <> nil then
	  	FSizeStatus.Caption := FLabels.Width  + IntToStr(FChildren.Width) + FLabels.Separator +
														 FLabels.Height + IntToStr(FChildren.Height);
  end;
	//----------------------------------------------------------------------------
	procedure TEdicion.MouseUp;
	begin
		FChildren.Draft := False;
//	  FChildren.Repaint; al cambiar Draf se repinta solo
    RefreshControls;
	end;
  //----------------------------------------------------------------------------
	procedure TEdicion.MouseDown;
	begin
  	FXo := X;
    FYo := Y;
    FChildren.Draft := True;

		FSupIzq.Visible := False;
		FSupMed.Visible := False;
		FSupDer.Visible := False;
		FMedIzq.Visible := False;
		FMedDer.Visible := False;
		FInfIzq.Visible := False;
		FInfMed.Visible := False;
		FInfDer.Visible := False;
  end;
	//----------------------------------------------------------------------------
	procedure TEdicion.MouseMove;
  var
    ChildStatus: TAreaState;
	begin
		if FChildren.Draft then	begin
			(Sender as TControl).Left := (Sender as TControl).Left + X - FXo;
			(Sender as TControl).Top := (Sender as TControl).Top + Y - FYo;
      ChildStatus := asMoving;
			if (Sender is TEsquina) then begin
				if (Sender as TEsquina).Horizontal = mpResize then begin
					FChildren.Width := (Sender as TEsquina).Left - FChildren.Left + 3;
          ChildStatus := asResizing;
        end;
				if (Sender as TEsquina).Horizontal = mpMoveResize then begin
					FChildren.Width := FChildren.Width + FChildren.Left - (Sender as TEsquina).Left - 3;
					FChildren.Left := (Sender as TEsquina).Left + 3;
          ChildStatus := asResizing;
        end;
				if (Sender as TEsquina).Vertical = mpResize then begin
					FChildren.Height := (Sender as TEsquina).Top - FChildren.Top + 3;
          ChildStatus := asResizing;
        end;
				if (Sender as TEsquina).Vertical = mpMoveResize then begin
					FChildren.Height := FChildren.Height + FChildren.Top - (Sender as TEsquina).Top - 3;
					FChildren.Top := (Sender as TEsquina).Top + 3;
          ChildStatus := asResizing;
        end;
			end;
      RefreshStatus(ChildStatus);
			FChildren.Repaint
		end;
	end;
	//----------------------------------------------------------------------------
	procedure TEdicion.Click;
  begin
  	if (Sender is TArea) then Select(Sender As TArea);
  end;
	//----------------------------------------------------------------------------
	procedure TEdicion.SetLabels;
  begin
  	FLabels.Assign(Value);
  end;

  //---------------------------------------------------------------------------
  // Registracion de los Componentes
  //---------------------------------------------------------------------------
	procedure Register;
	begin
		RegisterComponents('Esteban', [tArea]);
		RegisterComponents('Esteban', [tEdicion]);
	end;

end.
