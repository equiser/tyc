(*{$DEFINE DEMOVERSION}*)
unit Xprinter;

interface

Uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, StdCtrls, Printers, ExtCtrls, Buttons, Dialogs, Base, ComCtrls;

Const
   atLeft = 0;
	 atCenter = 1;
   atRight = 2;
   atBottom = 0;
   atMiddle = 4;
   atTop = 8;

Type
  TRealPoint = Record
    X: Real;
    Y: Real;
  End;

  TXPrinter = class;
  TPreview = class(TForm)
    Panel1: TPanel;
    sbFirst: TSpeedButton;
		sbPrior: TSpeedButton;
    sbNext: TSpeedButton;
    sbLast: TSpeedButton;
    sbPrint: TSpeedButton;
    sbPrintPage: TSpeedButton;
    sbPageWidth: TSpeedButton;
    sbPageFull: TSpeedButton;
    sbClose: TSpeedButton;
    StatusBar: TStatusBar;
    Desktop: TPanel;
    sb: TScrollBox;
    PaintBox1: TPaintBox;
    pbSave: TSpeedButton;
    procedure FirstClick(Sender: TObject);
    procedure PriorClick(Sender: TObject);
    procedure NextClick(Sender: TObject);
    procedure LastClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PageFullClick(Sender: TObject);
    procedure PaintBox1Paint(Sender: TObject);
		procedure PageWidthClick(Sender: TObject);
    procedure PrintClick(Sender: TObject);
    procedure PrintPageClick(Sender: TObject);
    procedure PaintBox1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
{    procedure Button3Click(Sender: TObject);}
    procedure CloseClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure PaintBox1Click(Sender: TObject);
    procedure SaveClick(Sender: TObject);
  private
    { Private declarations }
    Ry: TXPrinter;
    PageDisplaying: Integer;
  public
    { Public declarations }
  end;

   { Objeto XPrinter ------------------------------------------------------------------------------}
	TXPrinter = class(TObject)
	Private
		FCanvases: TList;
		FMetafiles: TList;
		FCurrentPage: Integer;

		FPreview: TPreview;
		FDonePrinting: Boolean;

    FOffset: TPoint;
		FDotPerInch: TPoint;
		FResolution: TRealPoint;
		FOrientation: TPrinterOrientation;
    FMirror: Boolean;

		Function GetCanvas(Index: Integer): TMetafileCanvas;
		Function GetMetafile(Index: Integer): TMetafile;
		Procedure SetCurrentPage(Index: Integer);
		Function GetPageCount: Integer;
  	Procedure SetOrientation(Value: TPrinterOrientation);
    Function GetFont: TFont;
    procedure SetFont(Value: TFont);
	Public
		{ Propiedades -------------------------------------------------------------------------------}
    	Title: String;
    	Property Canvases[Index: Integer]: TMetafileCanvas Read GetCanvas;
    	Property Metafiles[Index: Integer]: TMetafile Read GetMetafile;
    	Property PageCount: Integer Read GetPageCount;
    	Property CurrentPage: Integer Read FCurrentPage Write SetCurrentPage;

      Property Offset: TPoint Read FOffset;
      Property Mirror: Boolean read FMirror;
      Property DotPerInch: TPoint Read FDotPerInch;
      Property Resolution: TRealPoint Read FResolution;
    	Property Orientation: TPrinterOrientation Read FOrientation Write SetOrientation;
      Property Font: TFont Read GetFont Write SetFont;
		{ Metodos de Creacci�n ----------------------------------------------------------------------}
    	Constructor Create;
    	Procedure NewJob(Mirror: Boolean);
    	Function NewPage: Integer;
    	Procedure DonePrinting;
    	Destructor Destroy; Override;

		{ Metodos de Manipulaci�n -------------------------------------------------------------------}
			Procedure Preview;
    	Procedure PrintIt;
      Procedure DisplayPage(Page: Integer);
    	Procedure PrintPage(PageNum: Integer);
			Procedure SaveToFile(FileName: String);

    	{ Funciones de informaci�n y Conversi�n -----------------------------------------------------}
    	Function PageSize: TPoint;
    	Function PrintSize: TPoint;
    	Function TextWidth(Text: String): Integer;
    	Function TextHeight(Text: String): Integer;

    	Function XInch(Value: Real): Integer;
    	Function YInch(Value: Real): Integer;
    	Function XMilimeters(Value: Real): Integer;
    	Function YMilimeters(Value: Real): Integer;
    	Function MilimetersX(Value: Integer): Real;
    	Function MilimetersY(Value: Integer): Real;

    	{ Metodos de Dibujo -------------------------------------------------------------------------}
    	Procedure Line(X1, Y1, X2, Y2: Real; Border: TBorder);
			Procedure QuartCircle(Cx, Cy, Radius: Real; Cuadrant: Integer; Border: TBorder);
    	Procedure Rectangle(X1, Y1, X2, Y2, Radius: Real; Border: TBorder; Fill: TFill);
    	Procedure FillArea(X1, Y1, X2, Y2: Real; Fill: TFill);
    	Procedure TextOut(X1, Y1: Real; Text: String);
     	Procedure TextRect(X ,Y, RX1, RY1, RX2, RY2: Real; Text: String);
    	Procedure TextBlock(X1, Y1, X2, Y2: Real; Align: Integer; NewFont: TFont; Angle: Integer; Text: String; MColor: TColor);
    	Procedure Bitmap(X1, Y1, X2, Y2: Real; Graph: TBitmap);
    	Procedure Icon(X1, Y1, X2, Y2: Real; Graph: TIcon);
    	Procedure Metafile(X1, Y1, X2, Y2: Real; Graph: TMetafile);

{    	Procedure TextOut(X, Y: Integer; Text: String);
    	Procedure TextOutRight(X, Y: Integer; Text: String);
    	Procedure TextOutCenter(X, Y: Integer; const Text: String);
   	Function MemoOut(aRect: TRect; p: PChar; DisposePChar: Boolean; DontPrint: Boolean): Integer;
    	Procedure PutPageNums(X, Y: Integer; Alignment: TAlignment);
    	Procedure TextRect(aRect: TRect; const X, Y: Integer; Text: String);
    	Function TextWidth(Text: String): Integer;
    	Function TextHeight: Integer;
    	Procedure Line(a, b: TPoint);
    	Procedure Rectangle(aRect: TRect);
    	Function MemoOut(aRect: TRect; p: PChar; DisposePChar: Boolean; DontPrint: Boolean): Integer;}
	End;

implementation

{$R *.DFM}
  uses Windows;

  {-----------------------------------------------------------------------------------------------}
	{ Propiedades 																												}
  {-----------------------------------------------------------------------------------------------}
	Function TXPrinter.GetCanvas;
	Begin
		If (Index > 0) And (Index <= PageCount) Then
      	Result := TMetafileCanvas(FCanvases[Index - 1]);
	End;
   {-----------------------------------------------------------------------------------------------}
	Function TXPrinter.GetMetafile(Index: Integer): TMetafile;
	Begin
		if (Index > 0) And (Index <= PageCount) And FDonePrinting Then
      	Result := TMetafile(FMetafiles[Index - 1]);
	End;
   {-----------------------------------------------------------------------------------------------}
	Function TXPrinter.GetPageCount: Integer;
	Begin
		Result := FCanvases.Count;
	End;
   {-----------------------------------------------------------------------------------------------}
	Procedure TXPrinter.SetCurrentPage(Index: Integer);
	Begin
		If (Index <= PageCount) And (Index > 0) Then
      	FCurrentPage := Index;
	end;
   {-----------------------------------------------------------------------------------------------}
	Procedure TXPrinter.SetOrientation(Value: TPrinterOrientation);
	Begin
		FOrientation := Value;
		Printer.Orientation := Value;
	End;
   {-----------------------------------------------------------------------------------------------}
  Function TXPrinter.GetFont: TFont;
	Begin
		If (FCurrentPage > 0) And (Not FDonePrinting) Then
      Result := Canvases[FCurrentPage].Font
    Else Result := nil;
	End;
   {-----------------------------------------------------------------------------------------------}
  procedure TXPrinter.SetFont(Value: TFont);
	Begin
		If (FCurrentPage > 0) And
       (Not FDonePrinting) And
       (Canvases[FCurrentPage].Font <> Value) Then
       Canvases[FCurrentPage].Font.Assign(Value);
	End;
   {-----------------------------------------------------------------------------------------------}
   {Metodos de Creaci�n 																									}
   {-----------------------------------------------------------------------------------------------}
	Constructor TXPrinter.Create;
	Begin
		Inherited Create;
		FCanvases := TList.Create;
		FMetafiles := TList.Create;
		FCurrentPage := 0;
		FDonePrinting := False;

		Escape(Printer.Handle, GETPRINTINGOFFSET, 0, nil, @FOffset);
   	FDotPerInch.X := GetDeviceCaps(Printer.Handle, LogPixelsX);
   	FDotPerInch.Y := GetDeviceCaps(Printer.Handle, LogPixelsY);
    FResolution.X := FDotPerInch.X / 25.4;
    FResolution.Y := FDotPerInch.X / 25.4;

		FOrientation := poPortrait;
	End;
   {-----------------------------------------------------------------------------------------------}
	Procedure TXPrinter.NewJob;
	Var
		i: Integer;
	Begin
		For i := FCanvases.Count - 1 downto 0 Do
			TCanvas(FCanvases[i]).Free;
		For i := FMetafiles.Count - 1 downto 0 Do
			TMetafile(FMetafiles[i]).Free;

    FMirror := Mirror;
   	FCanvases.Clear;
		FMetafiles.Clear;
		FCurrentPage := 0;
		FDonePrinting := False;

		Escape(Printer.Handle, GETPRINTINGOFFSET, 0, nil, @FOffset);
   	FDotPerInch.X := GetDeviceCaps(Printer.Handle, LogPixelsX);
   	FDotPerInch.Y := GetDeviceCaps(Printer.Handle, LogPixelsY);
    FResolution.X := FDotPerInch.X / 25.4;
		FResolution.Y := FDotPerInch.X / 25.4;

		NewPage;
	End;
   {-----------------------------------------------------------------------------------------------}
	Function TXPrinter.NewPage;
	Begin
		If Not FDonePrinting Then
      Begin
			Result := FMetafiles.Add(TMetafile.Create) + 1;
			FCurrentPage := Result;
      TMetafile(FMetafiles[Result - 1]).Width := Printer.PageWidth;
      TMetafile(FMetafiles[Result - 1]).Height := Printer.PageHeight;

      FCanvases.Add(TMetafileCanvas.Create(TMetafile(FMetafiles[Result - 1]), 0));

			With Canvases[Result] Do
         Begin
				Brush.style := bsClear;
				Font.Name := 'Arial';
				Font.PixelsPerInch := DotPerInch.Y;
				Font.Size := 10;
			End;
		End;
	End;
   {-----------------------------------------------------------------------------------------------}
	Procedure TXPrinter.DonePrinting;
	Var
		i: integer;
	Begin
		If Not FDonePrinting Then
    Begin
      For i := 1 To PageCount Do
      	Canvases[i].Free;
			FDonePrinting := True;
		End;
	End;
   {-----------------------------------------------------------------------------------------------}
	Destructor TXPrinter.Destroy;
   Var
		i: Integer;
	Begin
		For i := FMetafiles.Count - 1 downto 0 Do
			TMetafile(FMetafiles[i]).Free;
		FCanvases.Free;
		FMetafiles.Free;
		Inherited Destroy;
	End;

   {-----------------------------------------------------------------------------------------------}
   { Metodos de Manipulaci�n 																								}
   {-----------------------------------------------------------------------------------------------}
	Procedure TXPrinter.Preview;
	Begin
		If Not FDonePrinting Then DonePrinting;
    FPreview := TPreview.Create(Application);
		FPreview.Ry := Self;
		FPreview.ShowModal;
		FPreview.Release;
		FPreview := nil;
	End;
   {-----------------------------------------------------------------------------------------------}
	Procedure TXPrinter.PrintIt;
	Var
		i: Integer;
		s: String;
	Begin
  {$IFNDEF DEMOVERSION}
		If Not FDonePrinting Then DonePrinting;
		If PageCount > 0 Then
      Begin
			Printer.Title := Title;
			Printer.BeginDoc;
			i := 1;
			If Assigned(FPreview) Then s := FPreview.StatusBar.Panels[0].Text;
  			Try
  				if Assigned(FPreview) Then FPreview.StatusBar.Panels[0].Text := Format('Imprimiendo P�gina %d de %d',[1, PageCount]);
          if Mirror then begin
    				Printer.Canvas.StretchDraw(Rect(Printer.PageWidth, 0, 0, Printer.PageHeight), Metafiles[i]);
          end else begin
    				Printer.Canvas.StretchDraw(Rect(0, 0, Printer.PageWidth, Printer.PageHeight), Metafiles[i]);
          end;
  				For i := 2 to PageCount Do
            Begin
						If Assigned(FPreview) Then FPreview.StatusBar.Panels[0].Text := Format('Imprimiendo P�gina %d de %d',[1, PageCount]);
    				Printer.NewPage;
            if Mirror then begin
    				Printer.Canvas.StretchDraw(Rect(Printer.PageWidth, 0, 0, Printer.PageHeight), Metafiles[i]);
            end else begin
              Printer.Canvas.StretchDraw(Rect(0, 0, Printer.PageWidth, Printer.PageHeight), Metafiles[i]);
            end;
    			End;
  			Finally
  				Printer.EndDoc;
            If Assigned(FPreview) Then FPreview.StatusBar.Panels[0].Text := s;
  			End;
  		End;
    {$ENDIF}
	End;
   {---------------------------------------------------------------------------}
	Procedure TXPrinter.DisplayPage(Page: Integer);
	Var
  		p: TPoint;
  		Sc: Single;
  		r: TRect;
	Begin
		If (Page > 0) AND (Page <= PageCount) Then
			Begin
			Sc := FPreview.PaintBox1.Width / PageSize.X;

			FPreview.PaintBox1.Canvas.Rectangle(0, 0,
         	FPreview.PaintBox1.Width,FPreview.PaintBox1.Height);
  			FPreview.PaintBox1.Canvas.FillRect(Rect(1, 1,
         	FPreview.PaintBox1.Width - 2, FPreview.PaintBox1.Height - 2));

  			Escape(Printer.Handle, GETPRINTINGOFFSET, 0, nil, @p);

        if Mirror then begin
          r.Right := Trunc(p.X * Sc);
	  		  r.Left := r.Right + Trunc(Printer.PageWidth * Sc);
        end else begin
          r.Left := Trunc(p.X * Sc);
	  		  r.Right := r.Left + Trunc(Printer.PageWidth * Sc);
        end;
			  r.Top := Trunc(p.Y * Sc);
			  r.Bottom := r.Top + Trunc(Printer.PageHeight * Sc);

  			FPreview.PaintBox1.Canvas.StretchDraw(r, Metafiles[Page]);
	      FPreview.StatusBar.Panels[0].Text := Format('P�gina %d de %d', [Page, PageCount]);
	  		FPreview.PageDisplaying := Page;
	      If Page = 1 Then
				Begin
	    		FPreview.sbFirst.Enabled := False;
	    		FPreview.sbPrior.Enabled := False;
	    	End
	      Else
	      Begin
	    		FPreview.sbFirst.Enabled := True;
	    		FPreview.sbPrior.Enabled := True;
	    	End;

	  		If PageCount > Page Then
	      Begin
	    		FPreview.sbNext.Enabled := True;
	    		FPreview.sbLast.Enabled := True;
	    	End
	      Else
	      Begin
	    		FPreview.sbNext.Enabled := False;
	    		FPreview.sbLast.Enabled := False;
				End;
      End;
   End;
   {-----------------------------------------------------------------------------------------------}
	Procedure TXPrinter.PrintPage;
	Var
		S: String;
	Begin
  {$IFNDEF DEMOVERSION}
		If Not FDonePrinting Then DonePrinting;
		If (PageNum > 0) And (PageNum <= PageCount) Then
      Begin
  			Printer.Title := Title;
  			Printer.BeginDoc;
  			If Assigned(FPreview) Then s := FPreview.StatusBar.Panels[0].Text;
  			Try
  				If Assigned(FPreview) Then
			    	FPreview.StatusBar.Panels[0].Text := Format('Imprimiendo P�gina %d de %d', [PageNum, PageCount]);
            Printer.Canvas.StretchDraw(Rect(0,0,Printer.PageWidth, Printer.PageHeight), Metafiles[PageNum]);
  			Finally
					Printer.EndDoc;
  				If Assigned(FPreview) Then FPreview.StatusBar.Panels[0].Text := s;
  			End;
  		End;
    {$ENDIF}
	End;
   {-----------------------------------------------------------------------------------------------}
   Procedure TXPrinter.SaveToFile;
	var
		i: integer;
	begin
  {$IFNDEF DEMOVERSION}
		If FDonePrinting Then
		  For i := 1 to PageCount Do
		    Metafiles[i].SaveToFile(Format('%6s%.2d', [FileName, i]));
    {$ENDIF}
   End;

   {-----------------------------------------------------------------------------------------------}
   { Funciones de Informaci�n y Conversi�n 																			}
   {-----------------------------------------------------------------------------------------------}
	Function TXPrinter.PageSize: TPoint;
	Begin
		Escape(Printer.Handle, GETPHYSPAGESIZE, 0, nil, @Result);
   End;
   {-----------------------------------------------------------------------------------------------}
	Function TXPrinter.PrintSize: TPoint;
	Begin
		Result.X := Printer.PageWidth;
		Result.Y := Printer.PageHeight;
   End;
   {-----------------------------------------------------------------------------------------------}
	Function TXPrinter.TextWidth;
	Begin
		If (FCurrentPage > 0) And (Not FDonePrinting) Then
      Begin
         Printer.Canvas.Font := Canvases[FCurrentPage].Font;
			Result := Printer.Canvas.TextWidth(Text);
      End
      Else
      	Result := -1;
	End;
   {-----------------------------------------------------------------------------------------------}
	Function TXPrinter.TextHeight;
	Begin
		If (FCurrentPage > 0) And (Not FDonePrinting) Then
      Begin
         Printer.Canvas.Font := Canvases[FCurrentPage].Font;
			Result := Printer.Canvas.TextHeight(Text);
      End
      Else
      	Result := -1;
	End;
   {-----------------------------------------------------------------------------------------------}
   Function TXPrinter.XMilimeters;
   Begin
   	Result := Round(Value * Resolution.X);
   End;
   {-----------------------------------------------------------------------------------------------}
   Function TXPrinter.YMilimeters;
	 Begin
   	Result := Round(Value * Resolution.Y);
   End;
   {-----------------------------------------------------------------------------------------------}
 	 Function TXPrinter.MilimetersX(Value: Integer): Real;
	 Begin
   	Result := Value / Resolution.X;
   End;
   {-----------------------------------------------------------------------------------------------}
 	 Function TXPrinter.MilimetersY(Value: Integer): Real;
	 Begin
   	Result := Value / Resolution.Y;
   End;
   {-----------------------------------------------------------------------------------------------}
	Function TXPrinter.XInch;
	Begin
		Result := Trunc(DotPerInch.X * Value);
	End;
   {-----------------------------------------------------------------------------------------------}
	function TXPrinter.YInch;
	begin
		Result := Trunc(DotPerInch.Y * Value);
	end;
   {-----------------------------------------------------------------------------------------------}
   { Metodos de Dibujo 							 																			}
   {-----------------------------------------------------------------------------------------------}
   Procedure TXPrinter.Line;
   Begin
		If (Border.Width > 0) And (FCurrentPage > 0) And (Not FDonePrinting) Then
				With Canvases[FCurrentPage] Do
         Begin
      		Pen.Width := Round(Border.Width * (Resolution.X + Resolution.Y) / 2);
         	Pen.Color := Border.Color;

      		MoveTo(XMilimeters(X1) - Offset.X, YMilimeters(Y1) - Offset.Y);
      		LineTo(XMilimeters(X2) - Offset.X, YMilimeters(Y2) - Offset.Y);
      	End;
   End;
   {-----------------------------------------------------------------------------------------------}
	Procedure TXPrinter.QuartCircle;
   Var
		X1, Y1: Integer;
    X2, Y2: Integer;
   Begin
		If (Border.Width > 0) And (FCurrentPage > 0) And (Not FDonePrinting) Then
      	With Canvases[FCurrentPage] Do
	      Begin
   			Pen.Width := Round(Border.Width * (Resolution.X + Resolution.Y) / 2);
					Pen.Color := Border.Color;
      		X1 := XMilimeters(Cx - Radius) - Offset.X;
            Y1 := YMilimeters(Cy - Radius) - Offset.Y;
      		X2 := XMilimeters(Cx + Radius) - Offset.X;
            Y2 := YMilimeters(Cy + Radius) - Offset.Y;
         	Case Cuadrant Of
         		1: Arc(X1, Y1, X2, Y2, X2, (Y1 + Y2) Div 2, (X1 + X2) Div 2, Y1);
         		2: Arc(X1, Y1, X2, Y2, (X1 + X2) Div 2, Y2, X2, (Y1 + Y2) Div 2);
         		3: Arc(X1, Y1, X2, Y2, X1, (Y1 + Y2) Div 2, (X1 + X2) Div 2, Y2);
         		4: Arc(X1, Y1, X2, Y2, (X1 + X2) Div 2, Y1, X1, (Y1 + Y2) Div 2);
         	End;
      	End;
   End;
   {-----------------------------------------------------------------------------------------------}
   Procedure TXPrinter.Rectangle;
   Begin
   	If(FCurrentPage > 0) And (Not FDonePrinting) Then
      With Canvases[FCurrentPage] Do
      Begin
			Pen.Width := Round(Border.Width * (Resolution.X + Resolution.Y) / 2);
   		If Border.Width = 0 Then
	      	Pen.Color := Fill.Color
	      Else
	      	Pen.Color := Border.Color;

			  Brush.Color := Fill.Color;
      	Brush.Style := Fill.BrushStyle;
      	if Fill.Style = 0 then begin
      		Line(X1 + Radius, Y1, X2 - Radius, Y1, Border);
      		Line(X1 + Radius, Y2, X2 - Radius, Y2, Border);
      		Line(X1, Y1 + Radius, X1, Y2 - Radius, Border);
      		Line(X2, Y1 + Radius, X2, Y2 - Radius, Border);
            if Radius > 0 then begin
         		QuartCircle(X1 + Radius, Y1 + Radius, Radius, 4, Border);
         		QuartCircle(X2 - Radius, Y1 + Radius, Radius, 1, Border);
         		QuartCircle(X2 - Radius, Y2 - Radius, Radius, 2, Border);
						QuartCircle(X1 + Radius, Y2 - Radius, Radius, 3, Border);
            end;
      	end	else
	      	RoundRect(XMilimeters(X1) - Offset.X, YMilimeters(Y1) - Offset.Y,
            	XMilimeters(X2) - Offset.X, YMilimeters(Y2) - Offset.Y,
               XMilimeters(Radius), XMilimeters(Radius));
   	End;
   End;
   {-----------------------------------------------------------------------------------------------}
   Procedure TXPrinter.FillArea;
   Begin
   	If(FCurrentPage > 0) And (Not FDonePrinting) Then
      With Canvases[FCurrentPage] Do
      Begin
			Brush.Color := Fill.Color;
      	Brush.Style := Fill.BrushStyle;
      	FillRect(Rect(XMilimeters(X1) - Offset.X, YMilimeters(Y1) - Offset.Y,
         	XMilimeters(X2) - Offset.X, YMilimeters(Y2) - Offset.Y));
			End;
   End;
   {-----------------------------------------------------------------------------------------------}
 	Procedure TXPrinter.TextOut;
  begin
		if(FCurrentPage > 0) And (Not FDonePrinting) then
			with Canvases[FCurrentPage] do
				WinProcs.TextOut(Handle, XMilimeters(X1) - Offset.X,
                         YMilimeters(Y1) - Offset.Y, @Text[1], Length(Text));
  end;
   {-----------------------------------------------------------------------------------------------}
 	Procedure TXPrinter.TextRect;
  var
    R: TRect;
  begin
		if(FCurrentPage > 0) And (Not FDonePrinting) then
			with Canvases[FCurrentPage] do begin
        Brush.Style := bsClear;
        R := Rect(XMilimeters(RX1) - Offset.X,
             YMilimeters(RY1) - Offset.Y,
             XMilimeters(RX2) - Offset.X,
             YMilimeters(RY2) - Offset.Y);
				WinProcs.ExtTextOut(Handle,
                            XMilimeters(X) - Offset.X,
                            YMilimeters(Y) - Offset.Y,
                            ETO_CLIPPED, @R, @Text[1], Length(Text),nil);
      end;
  end;
   {-----------------------------------------------------------------------------------------------}
	Procedure TXPrinter.TextBlock;
	Const
		Convert = Pi / 180;
	Var
		X, Y: Integer;
		XA, YA: Integer;
		XB, YB: Integer;
		Name: String;
		hNewFont: HFont;
		hOldFont: HFont;
	Begin
		If(FCurrentPage > 0) And (Not FDonePrinting) Then
			Begin
				With Canvases[FCurrentPage] Do
				Begin
					Font := NewFont;
					Name := Font.Name + #0;
          if Mirror and (Angle > 0) then begin
					  hNewFont := CreateFont(Font.Height, 0, 10 * (180 + Angle), 0,
							Ord(fsBold In Font.Style) * FW_BOLD, Ord(fsItalic In Font.Style),
							 Ord(fsUnderline In Font.Style), Ord(fsStrikeOut in Font.Style),
							 DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
							 DEFAULT_QUALITY, DEFAULT_PITCH + FF_DONTCARE, @Name[1]);
          end else begin
					  hNewFont := CreateFont(Font.Height, 0, 10 * Angle, 0,
							Ord(fsBold In Font.Style) * FW_BOLD, Ord(fsItalic In Font.Style),
							 Ord(fsUnderline In Font.Style), Ord(fsStrikeOut in Font.Style),
							 DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
							 DEFAULT_QUALITY, DEFAULT_PITCH + FF_DONTCARE, @Name[1]);
          end;
					hOldFont := SelectObject(Handle, hNewFont);
				End;

				with Canvases[FCurrentPage] do begin

          if MColor <> clNone then begin
            XA := Round(XMilimeters(X1) + XMilimeters(X2 -X1) * Cos(Convert * Angle));
            YA := Round(YMilimeters(Y1) - XMilimeters(X2 -X1) * Sin(Convert * Angle));
            XB := Round(XMilimeters(X1) + YMilimeters(Y2 -Y1) * Sin(Convert * Angle));
            YB := Round(YMilimeters(Y1) + YMilimeters(Y2 -Y1) * Cos(Convert * Angle));
            X := XA + XB - XMilimeters(X1);
            Y := YA + YB - YMilimeters(Y1);

            Pen.Width := Round(0.1 * (Resolution.X + Resolution.Y) / 2);
            Pen.Color := MColor;
            Pen.Style := psDot;
            MoveTo(XMilimeters(X1) - Offset.X, YMilimeters(Y1) - Offset.Y);
            LineTo(XA - Offset.X, YA - Offset.Y);  // rectangulo
            LineTo(X - Offset.X, Y - Offset.Y);
            LineTo(XB - Offset.X, YB - Offset.Y);
            LineTo(XMilimeters(X1) - Offset.X, YMilimeters(Y1) - Offset.Y);
            LineTo(X - Offset.X, Y - Offset.Y);   // diagonal
            Pen.Style := psSolid;
          end;

          case (Align And 3) of
            atLeft:   begin
                        XA := XMilimeters(X1);
                        YA := YMilimeters(Y1);
                      end;
            atCenter: begin
                        XA := Round(XMilimeters(X1) + (XMilimeters(X2 -X1)
                              - TextWidth(Text))/2 * Cos(Convert * Angle));
                        YA := Round(YMilimeters(Y1) - (XMilimeters(X2 -X1)
                              - TextWidth(Text))/2 * Sin(Convert * Angle));
                      end;
            atRight:  begin
                        XA := Round(XMilimeters(X1) + (XMilimeters(X2 -X1)
                              - TextWidth(Text)) * Cos(Convert * Angle));
                        YA := Round(YMilimeters(Y1) - (XMilimeters(X2 -X1)
                              - TextWidth(Text))* Sin(Convert * Angle));
                      end;
          end;{-Case-}

          case (Align And 12) of
            atBottom: begin
                        XB := Round(XMilimeters(X1) + (YMilimeters(Y2 - Y1)
                              - TextHeight(Text)) * Sin(Convert * Angle));
                        YB := Round(YMilimeters(Y1) + (YMilimeters(Y2 - Y1)
                              - TextHeight(Text)) * Cos(Convert * Angle));
                      end;
            atMiddle: begin
                        XB := Round(XMilimeters(X1) + (YMilimeters(Y2 -Y1)
                              - TextHeight(Text))/2 * Sin(Convert * Angle));
                        YB := Round(YMilimeters(Y1) +(YMilimeters(Y2 -Y1)
                              - TextHeight(Text))/2 * Cos(Convert * Angle));
                      end;
            atTop:    begin
                        XB := XMilimeters(X1);
                        YB := YMilimeters(Y1);
                      end;
          end;{-Case-}

          if Mirror then begin
            X := XA + XB - XMilimeters(X1);
            Y := YA + YB - YMilimeters(Y1);
          end else begin
            X := XA + XB - XMilimeters(X1);
            Y := YA + YB - YMilimeters(Y1);
          end;
					Brush.Style := bsClear;
					SetTextColor(Handle, ColorToRGB(NewFont.Color));
					WinProcs.TextOut(Handle, X - Offset.X, Y - Offset.Y, @Text[1], Length(Text));

					SelectObject(Handle, hOldFont);
					DeleteObject(hNewFont);
				end;
			End;
	 End;
	 {-----------------------------------------------------------------------------------------------}
	 Procedure TXPrinter.Bitmap;
	 begin
    if(FCurrentPage > 0) And (Not FDonePrinting) then begin
      with Canvases[FCurrentPage] do
        StretchDraw(Rect(XMilimeters(X1) - Offset.X, YMilimeters(Y1) - Offset.Y,
          XMilimeters(X2) - Offset.X, YMilimeters(Y2) - Offset.Y), Graph);
      end;
	 end;
	 {-----------------------------------------------------------------------------------------------}
 	 Procedure TXPrinter.Icon;
   var
     Bitmap: Graphics.TBitmap;
     R: TRect;
	 begin
		if(FCurrentPage > 0) And (Not FDonePrinting) then
			with Canvases[FCurrentPage] do
        if ((XMilimeters(X2) - XMilimeters(X1)) <> Graph.Width) or
           ((YMilimeters(Y2) - YMilimeters(Y1)) <> Graph.Height) then begin
					Bitmap := Graphics.TBitmap.Create;
					try
						R := Rect(XMilimeters(X1) - Offset.X, YMilimeters(Y1) - Offset.Y,
											XMilimeters(X2) - Offset.X, YMilimeters(Y2) - Offset.Y);
						Bitmap.Height := Graph.Height;
						Bitmap.Width := Graph.Width;
						Bitmap.Canvas.CopyRect(Rect(0,0,Graph.Width,Graph.Height),Canvases[FCurrentPage],R);
						Bitmap.Canvas.Draw(0,0,Graph);
						StretchDraw(R, Bitmap);
					finally
						Bitmap.Free;
					end;
        end else
          Draw(XMilimeters(X1) - Offset.X, YMilimeters(Y1) - Offset.Y, Graph);
	 end;
	 {-----------------------------------------------------------------------------------------------}
	 Procedure TXPrinter.Metafile;
{	 Var
			A,B: Integer;}
	 Begin
		If(FCurrentPage > 0) And (Not FDonePrinting) Then
			Begin
{					 A := Graph.Inch;}
					 Graph.Inch := 0;
{					 A := Graph.Inch;
					 Graph.Width :=  XMilimeters(X2) - XMilimeters(X1);
					 Graph.Height :=  XMilimeters(Y2) - XMilimeters(Y1);
					 Graph.Inch := DotPerInch.X + Dot}
					 With Canvases[FCurrentPage] Do
							 StretchDraw(Rect(XMilimeters(X1) - Offset.X, YMilimeters(Y1) - Offset.Y,
							XMilimeters(X2) - Offset.X, YMilimeters(Y2) - Offset.Y), Graph);
			End;
	 End;
	 {-----------------------------------------------------------------------------------------------}

{ 	Procedure TXPrinter.TextOut;
	Begin
		If (FCurrentPage > 0) AND (not FDonePrinting) Then
				Canvases[FCurrentPage].TextOut(X - Offset.X, Y - Offset.Y, Text);
	End;
	 {-----------------------------------------------------------------------------------------------}
{   Procedure TXPrinter.TextOutRight;
	 Begin
		If (FCurrentPage > 0) And (Not FDonePrinting) Then
				Canvases[FCurrentPage].TextOut(X - Self.TextWidth(Text) - Offset.X, Y - Offset.Y, Text);
	End;
	 {-----------------------------------------------------------------------------------------------}
{	Procedure TXPrinter.TextOutCenter;
	Begin
		If (FCurrentPage > 0) And (Not FDonePrinting) Then
				Canvases[FCurrentPage].TextOut(X - (Self.TextWidth(Text) div 2) - Offset.X, Y - Offset.Y, Text);
	End;
	 {-----------------------------------------------------------------------------------------------}
{	Procedure TXPrinter.TextRect;
	Begin
		If (FCurrentPage > 0) AND (not FDonePrinting) Then
			With Canvases[FCurrentPage] do
					TextRect(Rect(aRect.Left - XOff, aRect.Top - YOff, aRect.Right - XOff,
						aRect.Bottom - YOff), X - XOff, Y - YOff, Text);
	End;
	 {-----------------------------------------------------------------------------------------------}
{	Function TXPrinter.MemoOut;
	Var
		x, y: Integer;
		l: PChar;
		t: Integer;
		i: Longint;
		InMiddleOfReturn: Boolean;
		InMiddleOfWord: Boolean;
		WordBegin: PChar;

		Procedure DoTab;
		Var
				d: Integer;
			Begin
			d := aRect.Left;
			Repeat
				Inc(d, t);
			Until d > x;
			x := d;
		End;

		Procedure NextLine;
		Begin
			Inc(y, TextHeight('X'));
			x := aRect.Left;
		End;

		Function WordLength: Integer;
		Var
				Temp: PChar;
		Begin
			Temp := StrAlloc(l - WordBegin + 1);
			StrLCopy(temp, l, l - WordBegin);
			Result := TextWidth(StrPas(temp));
			StrDispose(temp);
		End;

		Procedure DoWord;
		Var
				Temp: PChar;
		Begin
			Temp := StrAlloc(l - WordBegin + 1);
			StrLCopy(temp, WordBegin, l - WordBegin);

			If (TextWidth(StrPas(temp)) + x) > (aRect.Right) Then NextLine;
			If Not DontPrint Then TextOut(x, y, StrPas(Temp));

			Inc(x, TextWidth(StrPas(temp)));
				StrDispose(temp);
				InMiddleOfWord := False;
		End;

	Begin
		If (FCurrentPage > 0) And (Not FDonePrinting) And Assigned(p) Then
			Begin
				t := TextWidth('        ');
			l := p;
			x := aRect.Left;
			y := aRect.Top;
			InMiddleOfWord := False;
			InMiddleOfReturn := False;

			For i := 0 To StrLen(p) Do
				 Begin
				Case l^ Of
				#9: Begin {it's a tab}
 {						if InMiddleOfWord then DoWord;
						DoTab;
						if x > (aRect.Right) then NextLine;
						InMiddleOfWord := False;
						InMiddleOfReturn := False;
					End;
					#10: Begin
						If InMiddleOfWord Then DoWord;
						If Not InMiddleOfReturn Then NextLine;
						InMiddleOfWord := False;
						InMiddleOfReturn := False;
							End;
				#13: Begin
						If InMiddleOfWord Then DoWord;
						NextLine;
						InMiddleOfWord := False;
						InMiddleOfReturn := True;
					End;
				#32: begin
						If InMiddleOfWord Then DoWord;
						Inc(x, TextWidth(' '));
						If (x > (aRect.Right)) Then NextLine;
						InMiddleOfWord := False;
						InMiddleOfReturn := False;
					End;
				Else
						If Not InMiddleOfWord Then WordBegin := l;
        				InMiddleOfWord := True;
						InMiddleOfReturn := False
				End;
					Inc(l);
				End;
			If InMiddleOfWord then DoWord;
			If DisposePChar then StrDispose(p);
			Result := y + TextHeight('X') - aRect.Top;
      End
      Else
      	Result := 0;
   End;
   {-----------------------------------------------------------------------------------------------}
{	Procedure TXPrinter.PutPageNums;
	Var
		i: Integer;
		s: String;
		o: Integer;
	Begin
		o := CurrentPage;
		For i := 1 to PageCount Do
      Begin
			s := Format('Page %d of %d', [i, PageCount]);
			CurrentPage := i;
			Case Alignment Of
				taLeftJustify: TextOut(X, Y, s);
				taCenter     : TextOutCenter(X, Y, s);
				taRightJustify: TextOutRight(X, Y, s);
			End;
  		End;
		CurrentPage := o;
	End;
   {-----------------------------------------------------------------------------------------------}

{***************** this is for the preview form ****************}

procedure TPreview.PrintClick(Sender: TObject);
begin
Ry.PrintIt;
end;

procedure TPreview.FirstClick(Sender: TObject);
begin
Ry.DisplayPage(1);
end;

procedure TPreview.PriorClick(Sender: TObject);
begin
Ry.DisplayPage(PageDisplaying - 1);
end;

procedure TPreview.NextClick(Sender: TObject);
begin
Ry.DisplayPage(PageDisplaying + 1);
end;

procedure TPreview.LastClick(Sender: TObject);
begin
Ry.DisplayPage(Ry.PageCount);
end;

Procedure TPreview.FormCreate(Sender: TObject);
Const
	crZoom = 5;
Begin
	Screen.Cursors[crZoom] := LoadCursor(HInstance, 'ZOOM');

	sbFirst.Glyph.Handle := LoadBitmap(HInstance, 'DBN_FIRST');
	sbPrior.Glyph.Handle := LoadBitmap(HInstance, 'DBN_PRIOR');
	sbNext.Glyph.Handle := LoadBitmap(HInstance, 'DBN_NEXT');
	sbLast.Glyph.Handle := LoadBitmap(HInstance, 'DBN_LAST');
	sbPrint.Glyph.Handle := LoadBitmap(HInstance, 'PRINT');
	sbPrintPage.Glyph.Handle := LoadBitmap(HInstance, 'PRINT_PAGE');
	sbPageWidth.Glyph.Handle := LoadBitmap(HInstance, 'PAGE_WIDTH');
	sbPageFull.Glyph.Handle := LoadBitmap(HInstance, 'PAGE_FULL');
	sbClose.Glyph.Handle := LoadBitmap(HInstance, 'BBCLOSE');
	PaintBox1.Cursor := crZoom;

	PageDisplaying := 1;
	FormResize(Self);
End;

procedure TPreview.PageFullClick(Sender: TObject);
begin
	PaintBox1.Visible := False;
	PaintBox1.Top := 15;
	PaintBox1.Height := Sb.height - 30;
	PaintBox1.Width := (Longint(PaintBox1.Height) * Longint(Ry.PageSize.X)) div
	Longint(Ry.PageSize.Y);
	PaintBox1.Left := (Width - PaintBox1.Width) Div 2;
	PaintBox1.Visible := True;

	Sb.VertScrollBar.Range := PaintBox1.Height;
	Sb.HorzScrollBar.Range := PaintBox1.Width;
end;

procedure TPreview.PaintBox1Paint(Sender: TObject);
begin
	Ry.DisplayPage(PageDisplaying);
end;

procedure TPreview.PageWidthClick(Sender: TObject);
begin
	PaintBox1.Visible := False;
	PaintBox1.Top := Panel1.Height + 15;
	PaintBox1.Left := 15;
	PaintBox1.Width := ClientWidth - 45;
	PaintBox1.Height := (Longint(PaintBox1.Width) * Longint(Ry.PageSize.Y)) div
	Longint(Ry.PageSize.X);
	PaintBox1.Visible := True;
	Sb.VertScrollBar.Range := PaintBox1.Height;
	Sb.HorzScrollBar.Range := PaintBox1.Width;
end;

Procedure TPreview.PrintPageClick(Sender: TObject);
var
 s: String;
begin
	s := '1';
	if InputQuery('Imprimir P�gina', 'N�mero', s) then begin
		Ry.PrintPage(StrToInt(s));
	end;
end;

procedure TPreview.PaintBox1MouseDown(Sender: TObject;
	Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
 i: Integer;
begin
case Button of
	mbLeft: begin
		PaintBox1.Visible := False;
		PaintBox1.Top := (ClientHeight div 2) - Y;
		PaintBox1.Left := (ClientWidth div 2) - X;
		i := Trunc(PaintBox1.Width * 0.35) div 2;
		PaintBox1.Width := PaintBox1.Width + i*2;
		PaintBox1.Left := PaintBox1.Left - i;
		i := Trunc(PaintBox1.Height * 0.35) div 2;
		PaintBox1.Height := PaintBox1.Height + i*2;
		PaintBox1.Top := PaintBox1.Top - i;
		PaintBox1.Visible := True;
		end;
	mbRight: begin
//		PaintBox1.Visible := False;
		i := Trunc(PaintBox1.Width * 0.35) div 2;
		PaintBox1.Width := PaintBox1.Width - i*2;
		PaintBox1.Left := PaintBox1.Left + i;
		i := Trunc(PaintBox1.Height * 0.35) div 2;
		PaintBox1.Height := PaintBox1.Height - i*2;
		PaintBox1.Top := PaintBox1.Top + i;
		PaintBox1.Top := (ClientHeight div 2) - Y;
		PaintBox1.Left := (ClientWidth div 2) - X;
		PaintBox1.Visible := True;
		end;
	end;
end;

{procedure TPreview.Button3Click(Sender: TObject);
var
	s: String;
begin
	s := 'REPORT';
	if InputQuery('Save Printout as WMF', 'Enter filename', s) then
		Ry.SaveToFile(s);
end;}

procedure TPreview.CloseClick(Sender: TObject);
begin
	ModalResult := mrCancel
end;

procedure TPreview.FormResize(Sender: TObject);
begin
	If sbPageFull.Down Then	PageFullClick(Self)
	Else If sbPageWidth.Down Then	PageWidthClick(Self);

//	Logotipo.Left := BarraEstado.Width - 147;
end;

procedure TPreview.PaintBox1Click(Sender: TObject);
begin
	if sbPageFull.Down then	begin
		sbPageWidth.Down := True;
		PageWidthClick(Self);
	end else if sbPageWidth.Down then	begin
		sbPageFull.Down := True;
		PageFullClick(Self);
	end;
end;



procedure TPreview.SaveClick(Sender: TObject);
begin
	with TSaveDialog.Create(Application) do begin
		DefaultExt := 'WMF';
		Filter := 'Windows Metafile (*.wmf)|*.WMF|Todos los archivos (*.*)|*.*';
		Options := [ofPathMustExist, ofFileMustExist];
		if Execute then Ry.SaveToFile(FileName);
		Free;
	end;
end;

end.
{----TextBlock------}
{		Function Signal(Value: Real): Integer;
			Begin
				If Value > 0 Then
					Result := Round(Value)
				 Else
					Result := 0;
			End;}
(*        Case (Align And 3) Of
					atLeft:   X := XMilimeters(X1) + Signal(- TextWidth(Text) * Cos(Convert * Angle))
											+ Signal(- TextHeight(Text) * Sin(Convert * Angle));
					atCenter: X := (XMilimeters(X1 + X2) - Round(TextWidth(Text) * Cos(Convert * Angle))
											- Round(TextHeight(Text) * Sin(Convert * Angle))) Div 2;
					atRight:  X := XMilimeters(X2) - Signal(TextWidth(Text) * Cos(Convert * Angle))
											- Signal(TextHeight(Text) * Sin(Convert * Angle));
				End;
				Case (Align And 12) Of
					 atBottom: Y := YMilimeters(Y2) - Signal(TextHeight(Text) * Cos(Convert * Angle))
											 - Signal(TextWidth(Text) * Sin(Convert * Angle));
					 atMiddle: Y := (YMilimeters(Y1 + Y2) - Round(TextHeight(Text) * Cos(Convert * Angle))
											 + Round(TextWidth(Text) * Sin(Convert * Angle))) Div 2;
					 atTop:    Y := YMilimeters(Y1) + Signal(- TextHeight(Text) * Cos(Convert * Angle))
											 + Signal(TextWidth(Text) * Sin(Convert * Angle));
				End;*)

