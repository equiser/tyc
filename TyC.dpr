program TyC;

uses
  Forms,
  Articulo in 'Articulo.pas',
  Codigo in 'Codigo.pas',
  Editdef in 'Editdef.pas' {EditDefinition},
  Examinar in 'Examinar.pas' {Principal},
  Imagen in 'Imagen.pas',
  Nosotros in 'Nosotros.pas' {About},
  Pagina in 'Pagina.pas' {PrintInfo},
  Recuadro in 'Recuadro.pas',
  TablPack in 'Tablpack.pas',
  Tarjeta in 'Tarjeta.pas',
  Xprinter in 'Xprinter.pas' {Preview},
  ExportSipred in 'ExportSipred.pas' {Sipred},
  RepBase in 'RepBase.pas' {FormPreviewBase},
  ExtActns in 'C:\Archivos de programa\Borland\Delphi7\Source\Vcl\ExtActns.pas';

{$R TyC.RES}
{$R RECURSOS.RES}
begin
  Application.Title := 'Tarjetas & Credenciales';
  Application.HelpFile := 'D:\Proyectos\tyc\TyC.HLP';
  Application.CreateForm(TPrincipal, Principal);
  Application.CreateForm(TEditDefinition, EditDefinition);
  Application.CreateForm(TAbout, About);
  Application.CreateForm(TSipred, Sipred);
  Application.CreateForm(TFormPreviewBase, FormPreviewBase);
  Application.Run;
end.
