�
 TEDITDEFINITION 0�  TPF0TEditDefinitionEditDefinitionLeft� TopPWidth�Height�CaptionEditar DefinicionFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameSystem
Font.Style MenuMainMenuOnCloseQueryFormCloseQueryOnCreate
FormCreate	OnDestroyFormDestroyPixelsPerInch`
TextHeight TMemoEditLeft Top$Width�Height_HelpContextAlignalClientFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameCourier New
Font.Style Lines.StringsEdit 
ParentFont
ScrollBars
ssVerticalTabOrder WordWrapOnChange
EditChange  
TStatusBar	StatusBarLeft Top�Width�HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold Panels 
ParentFontSimplePanel	  TPanelPanel1Left Top Width�Height$AlignalTop
BevelOuterbvNoneTabOrder TSpeedButtonsbExitLeft� TopWidthHeightHintTerminar
AllowAllUp	Flat		NumGlyphsParentShowHintShowHint	OnClick	ExitClick  TSpeedButtonsbSearchLeft^TopWidthHeightHintBuscar
AllowAllUp	Flat	ParentShowHintShowHint	  TSpeedButtonsbOpenLeftTopWidthHeightHintAbrir Definici�n
AllowAllUp	Flat	ParentShowHintShowHint	OnClick	OpenClick  TBevelBevelLeft Top Width�HeightAlignalTop  TSpeedButtonsbSaveLeft TopWidthHeightHintGuardar Definici�n
AllowAllUp	Flat	ParentShowHintShowHint	OnClick	SaveClick  TSpeedButtonsbPrintLeft<TopWidthHeightHintImprimir
AllowAllUp	Flat	ParentShowHintShowHint	   TOpenDialog
OpenDialog
DefaultExtDEFOptionsofExtensionDifferentofPathMustExist LefthTopT  TSaveDialog
SaveDialog
DefaultExtDEFOptionsofExtensionDifferentofPathMustExist LeftTop�   	TMainMenuMainMenuLeftTopT 	TMenuItemmbFileCaption&Archivo 	TMenuItemmiNewCaption&Nuevo  	TMenuItemmiOpenCaption	&Abrir...OnClick	OpenClick  	TMenuItemmiSaveCaption&Guardar...OnClick	SaveClick  	TMenuItemmiSaveAsCaptionG&uardar como...OnClickSaveAsClick  	TMenuItemN3Caption-  	TMenuItemmiPrintCaption&Imprimir...Enabled  	TMenuItemN4Caption-HintSalir  	TMenuItemmiExitCaption&SalirOnClick	ExitClick   	TMenuItemmbEditCaption&Edicion 	TMenuItemmiUndoCaption	&DeshacerShortCutZ@OnClickmiUndoClick  	TMenuItemN1Caption-  	TMenuItemmiCutCaptionCor&tarShortCutX@OnClickCutClick  	TMenuItemmiCopyCaption&CopiarShortCutC@OnClick	CopyClick  	TMenuItemmiPasteCaption&PegarShortCutV@OnClick
PasteClick  	TMenuItemN2Caption-  	TMenuItemmiFindCaption
&Buscar...ShortCutF@OnClick	FindClick  	TMenuItem	miReplaceCaptionR&eemplazar...ShortCutR@   	TMenuItemmbHelpCaptionAyuda 	TMenuItemContenidos1Caption	ContenidoHelpContextOnClickContenidos1Click  	TMenuItemN5Caption-  	TMenuItemmiAboutCaption&Acerca de...    TReplaceDialogReplaceDialogLefthTop�    