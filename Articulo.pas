Unit Articulo;

Interface

	Uses
		SysUtils, WinTypes, WinProcs, Classes, Graphics,IniFiles, Base, XPrinter, DBTables;

  Type
	  TTextBlock = Class(TArea)
   	Private
      FAngle: Integer;
      FFont: TFont;
      FAlign: Integer;
      FText: String;
      Procedure SetAngle(Value: Integer);
      Procedure SetAlign(Value: Integer);
    Public
      Property Font: TFont Read FFont;
      Property Align: Integer Read FAlign Write SetAlign;
      Property Angle: Integer Read FAngle Write SetAngle;
      Property Text: String Read FText Write FText;

      Constructor Create;
      Procedure Assign(Source: TTextBlock);
      Procedure Load(Section: String; Source: TCustomIniFile);
      Procedure Print(OffsetX, OffsetY: Real; MColor: TColor; Table: TTable; Destination: TXPrinter);
    End;
Implementation

	{Metodos Privados ---------------------------------------------------------}
   Procedure TTextBlock.SetAngle;
   Var
   	Aux: Integer;
   Begin
		If Value > 0 Then
      	Aux := Value Mod 360
      Else
      	Aux := -(Abs(Value) Mod 360);

      If Aux > 180 Then	FAngle := Aux - 360;
      If Aux < -180 Then FAngle := Aux + 360;
   End;
   {--------------------------------------------------------------------------}
   Procedure TTextBlock.SetAlign;
   Begin
    	If Value In [0,1,2,4,4,6,8,9,10] Then
      	FAlign := Value;
   End;
	{Metodos Publicos ---------------------------------------------------------}
   Constructor TTextBlock.Create;
   Begin
   	Inherited Create;
		FFont := TFont.Create;
		FAngle := 0;
		FAlign := 0;
   End;
   {--------------------------------------------------------------------------}
	Procedure TTextBlock.Assign;
   Begin
   	Inherited Assign(Source);
      FFont.Assign(Source.Font);
      FAngle := Source.Angle;
      FAlign := Source.Align;
   End;
   {--------------------------------------------------------------------------}
   Procedure TTextBlock.Load;
   Var
      Coma: Integer;
   	Cadena: String;
      Posicion: Integer;
      SubCadena: String;
      TempColor: TColor;
   Begin
   	Inherited Load(Section, Source);
      FAlign := Source.ReadInteger(Section, 'Alineacion', 0);
      FText  := Source.ReadString(Section, 'Texto', '');
			FAngle := Source.ReadInteger(Section, 'Angulo', 0);

			TempColor := clBlack;
		Cadena := Source.ReadString(Section,'Letra','')+',';
		Posicion := 0;
		Coma := InStr(Cadena, ',');
		While Coma <> 0 Do
			Begin
				Subcadena := Copy(Cadena, 1, Coma - 1);
				Cadena := Copy(Cadena, Coma + 1, Length(Cadena));
				Posicion := Posicion + 1;
				Coma := InStr(Cadena, ',');
				Case Posicion Of
					 1: If Subcadena <> '' Then FFont.Name := Subcadena;
					2: If Subcadena <> '' Then FFont.Size := StrToInt(SubCadena);
					3: If InStr(UCase(SubCadena), 'N') > 0 Then
								FFont.Style := []
							Else
							 Begin
								If InStr(UCase(SubCadena), 'B') > 0 Then
									FFont.Style := FFont.Style + [fsBold];
								If InStr(UCase(SubCadena), 'I') > 0 Then
									FFont.Style := FFont.Style + [fsItalic];
								If InStr(UCase(SubCadena), 'S') > 0 Then
									FFont.Style := FFont.Style + [fsUnderline];
								If InStr(UCase(SubCadena), 'T') > 0 Then
									FFont.Style := FFont.Style + [fsStrikeOut];
							End;
					4: If Subcadena <> '' Then SetRValue(TempColor, StrToInt(Subcadena));
					5: If Subcadena <> '' Then SetGValue(TempColor, StrToInt(Subcadena));
					6: If Subcadena <> '' Then SetBValue(TempColor, StrToInt(Subcadena));
				 End;
			End;
			Font.Color := TempColor;
	 End;
	 {--------------------------------------------------------------------------}
	Procedure TTextBlock.Print;

		Function Buscar(Cadena: String; Delimitador: String): String;
		Var
			SubCadena: String;
		Begin
			Buscar := '';
			If Pos(Delimitador, Cadena) > 0 Then
				Begin
					SubCadena := Copy(Cadena, Pos(Delimitador, Cadena) + 1, Length(Cadena));
				If Pos(Delimitador, SubCadena) > 0 Then
					Buscar := Copy(Cadena, Pos(Delimitador, Cadena) + 1, Pos(Delimitador, SubCadena) - 1);
				End;
		 End;

    Function Reemplazar(Cadena, Campo, Valor: String): String;
    Var
		Posicion: Integer;
		Begin
			Posicion := Pos(Campo, Cadena);
			If Posicion > 0 Then
  		  Reemplazar := Copy(Cadena, 1, Posicion - 1) + Valor +
			  						Copy(Cadena, Posicion + Length(Campo), Length(Cadena))
			Else
			  Reemplazar := Cadena;
		End;

    procedure Recortar(var S: string; Count: Integer);
    begin
      if Count < 0 then      // recortamos a Count caracteres desde atras
        S := Copy(S, Length(S) + 1 + Count, - Count)
      else if Count > 0 then // recortamos a Count caracteres desde adelante
        S := Copy(S,1,Count);
    end;

  Var
		Field: String;
		Value: String;
    Longitud: Integer;
		Cadena: String;
		SubCadena: String;
	Begin
		If Visible Then
			Begin
				Cadena := FText;
				While Buscar(Cadena,'?') <> '' Do
				 Begin
					SubCadena := Buscar(Cadena, '?');
						Field := Buscar(SubCadena, '&');
						If  Field <> '' Then
						Begin
							Try
								Value := Table.FieldByName(Field).Text;
							Except
								Value := '';
							End;

							 If Value <> '' Then
								Cadena := Reemplazar(Cadena, '?' + SubCadena + '?', SubCadena)
							 Else
								Cadena := Reemplazar(Cadena, '?' + SubCadena + '?', '');
						End;
				 End;

				while Buscar(Cadena,'&') <> '' do begin
          Field := Buscar(Cadena, '&');
          Longitud := Pos('^',Field);
          if Longitud <> 0 then begin
            SubCadena := Copy(Field, Longitud+1, Length(Field));
            Field := Copy(Field,1,Longitud -1);
            try
              Longitud := StrToInt(SubCadena);
            except
              Longitud := 0;
              Field := Buscar(Cadena, '&');
            end;
          end;
          try
            if UpperCase(Field) = 'RZTARJETA' then begin
              SubCadena := Table.FieldByName('Tarjeta').Text;
              Recortar(SubCadena,Longitud);
              while SubCadena[1] = '0' do Delete(Subcadena,1,1);
            end else if UpperCase(Field) = 'RETARJETA' then begin
              SubCadena := Table.FieldByName('Tarjeta').Text;
              while Pos(' ',SubCadena) <> 0 do
                Delete(Subcadena,Pos(' ',SubCadena),1);
              Recortar(SubCadena,Longitud);
            end else if UpperCase(Field) = 'REZTARJETA' then begin
              SubCadena := Table.FieldByName('Tarjeta').Text;
              while Pos(' ',SubCadena) <> 0 do
                Delete(Subcadena,Pos(' ',SubCadena),1);
              Recortar(SubCadena,Longitud);
              while SubCadena[1] = '0' do Delete(Subcadena,1,1);
            end else begin
              SubCadena := Table.FieldByName(Field).Text;
              Recortar(SubCadena,Longitud);
            end;
          except
            SubCadena := '';
          end;
          if Longitud <> 0 then begin
            Cadena := Reemplazar(Cadena, '&' + Field +
                                 '^' + IntToStr(Longitud)+ '&', SubCadena);
          end else
            Cadena := Reemplazar(Cadena, '&' + Field + '&', SubCadena);

        end;
      	While Buscar(Cadena,'#') <> '' Do
         Begin
         	Field := Buscar(Cadena, '#');
            Try
            	Cadena := Reemplazar(Cadena, '#' + Field + '#', Chr(StrToInt(Buscar(Cadena, '#'))));
            Except
               Cadena := Reemplazar(Cadena, '#' + Field + '#', '');
            End;
         End;

         Destination.TextBlock(OffsetX + Left, OffsetY + Top,
         	OffsetX + Left + Width, OffsetY + Top + Height, Align, Font, Angle, Cadena, MColor);
      End;
   End;

End.
