unit Pagina;

interface

uses
	SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Forms,
	Dialogs, StdCtrls, Buttons, ExtCtrls, DBTables, IniFiles, DB,
	Base, Tarjeta, XPrinter, ComCtrls;

type
	TPrintInfo = class(TForm)
		FileName: TPanel;
		ProgressInfo: TPanel;
		ProgressBar: TProgressBar;
		TrjInfo: TPanel;
		sbCancel: TButton;
		procedure FormCreate(Sender: TObject);
		procedure sbCancelClick(Sender: TObject);
	private
		{ Private declarations }
		FCanceled: Boolean;
	public
		{ Public declarations }
		property Canceled: Boolean Read FCanceled;
	end;

	TPage = Class(TArea)
	private
		FPrintInfo: TPrintInfo;
		FRows: Integer;
		FCols: Integer;
		FReverse: Boolean;
		FRowSeparation: Real;
		FColSeparation: Real;
		FCard: TCard;
		FDescription: String;
		FFileName: String;
    FMirror: Boolean;
		procedure SetRows(Value: Integer);
		procedure SetCols(Value: Integer);
		procedure SetRowSeparation(Value: Real);
		procedure SetColSeparation(Value: Real);
	public
		property Card:TCard Read FCard;

		constructor Create;
		property Reverse:Boolean Read FReverse Write FReverse;
		property Rows:Integer Read FRows Write SetRows;
		property Cols:Integer Read FCols Write SetCols;
		property RowSeparation:Real Read FRowSeparation Write SetRowSeparation;
		property ColSeparation:Real Read FColSeparation Write SetColSeparation;
		property Description: String Read FDescription Write FDescription;
		property FileName: String Read FFileName;
    property Mirror: Boolean read FMirror Write FMirror;
		procedure Load(Source: String);
		procedure Print(Table: TTable; Start, Stop: Integer; Preview: Boolean; MColor: TColor);
	end;

var
	Credenciales: TPage;

implementation

{$R *.DFM}
{Procedimientos del Cuadro de Dialogo --------------------------------------}
procedure TPrintInfo.FormCreate(Sender: TObject);
begin
	FCanceled := False;
end;
{---------------------------------------------------------------------------}
procedure TPrintInfo.sbCancelClick(Sender: TObject);
begin
	FCanceled := True;
	ProgressInfo.Caption := ' Cancelado por el usuario';
end;

{Metodos Privados de la Pagina ---------------------------------------------}
procedure TPage.SetRows;
begin
	if (Value > 0) And (Value < 100) then FRows := Value;
end;
{---------------------------------------------------------------------------}
procedure TPage.SetCols;
begin
	if (Value > 0) And (Value < 500) then FCols := Value;
end;
{---------------------------------------------------------------------------}
procedure TPage.SetRowSeparation;
begin
	if (Value > 0) then FRowSeparation := Value;
end;
{---------------------------------------------------------------------------}
procedure TPage.SetColSeparation;
begin
	if (Value > 0) then FColSeparation := Value;
end;

{Metodos Publicos de la P�gina ---------------------------------------------}
Constructor TPage.Create;
begin
	inherited Create;

	Left := 5;
	Top := 5;

	FCols := 2;
	FRows := 5;
	FReverse := False;
	FDescription := '';
	FFileName := '';


	FRowSeparation := 5;
	FColSeparation := 5;

	FCard := TCard.Create;
 end;
{---------------------------------------------------------------------------}
procedure TPage.Load;
var
	Coma: Integer;
	Cadena: String;
//	IniFile: TIniFile;
  IniFile: TMemIniFile;
begin
	FFileName := Source;
//	IniFile := TIniFile.Create(Source);
	IniFile := TMemIniFile.Create(Source);

	Cadena := IniFile.ReadString('Pagina', 'Margen', '');
		Coma := InStr(Cadena,',');
		if Coma > 1 then
			Left := StrToReal(Copy(Cadena, 1, Coma - 1), 5);
		if Coma < Length(Cadena) then
			Top := StrToReal(Copy(Cadena, Coma + 1, Length(Cadena)), 5);

	FDescription := IniFile.ReadString('Pagina', 'Descripcion', FFileName);
	FReverse := IniFile.ReadBool('Pagina', 'Reverso', False);
	FRows := IniFile.ReadInteger('Pagina', 'Filas', 5);
	FCols := IniFile.ReadInteger('Pagina', 'Columnas', 2);
	FMirror := IniFile.ReadBool('Pagina', 'Espejo', False);

	Cadena := IniFile.ReadString('Pagina', 'Separacion', '');
		Coma := InStr(Cadena,',');
		If Coma > 1 then
			FColSeparation := StrToReal(Copy(Cadena, 1, Coma - 1), 5);
		If Coma < Length(Cadena) then
			FRowSeparation := StrToReal(Copy(Cadena, Coma + 1, Length(Cadena)), 5);
	FCard.Load(IniFile);

	IniFile.Free
end;
{---------------------------------------------------------------------------}
procedure TPage.Print;
var
	MaxPage, MinPage: Integer;
	NumPag: Integer;
	PrxPag: Integer;
	NumTrj: Integer;
	TrjActual: Integer;
	Indice: String;
	Actual: TBookMark;
	Posicion: TSmallintField;
	Continuar: Boolean;
	Destination: TXprinter;
begin
	FPrintInfo := TPrintInfo.Create(Application);
	FPrintInfo.Visible := True;

	Table.DisableControls;
	Indice := Table.IndexName;
	Actual := Table.GetBookmark;
	Table.IndexName := 'Posicion';
	Posicion := TSmallintField(Table.FieldByName('Posicion'));

	Table.Last;
	MaxPage := (Posicion.Value - 1) div (Rows * Cols) + 1;
	Table.First;
	MinPage := (Posicion.Value - 1) div (Rows * Cols) + 1;
	try
		FPrintInfo.ProgressBar.Max := (Table.RecordCount * (Stop -Start)) div (MaxPage - MinPage);
	except
	  FPrintInfo.ProgressBar.Max := Table.RecordCount;
	end;
	FPrintInfo.ProgressBar.Min := 0;
	TrjActual := 0;
	FPrintInfo.ProgressBar.Position := TrjActual;
	FPrintInfo.FileName.Caption := ' Imprimiendo '+ FDescription;
	FPrintInfo.ProgressInfo.Caption := Format(' P�gina %d de %d', [Start+1, Stop+1]);
	FPrintInfo.TrjInfo.Caption := Format(' Tarjeta %d de %d',
						[TrjActual, FPrintInfo.ProgressBar.Max]);

	Destination := TXPrinter.Create;
	Destination.NewJob(Mirror);
	NumPag := (Posicion.Value - 1) div (FRows * FCols);

	if Preview then FCard.MColor := MColor
	else FCard.MColor := clNone; // clNone = marco transparente

	Continuar := True;
	while Continuar and not FPrintInfo.Canceled do begin
			Application.ProcessMessages;

		if NumPag >= Start then begin
			NumTrj := (Posicion.Value - 1) mod (FRows * FCols);
			FCard.Top := Top + (FCard.Height + FRowSeparation) * (NumTrj Div FCols);
			if Reverse then
				FCard.Left := Left + (FCard.Width + FColSeparation) * (FCols - (NumTrj mod FCols) - 1)
			else
				FCard.Left := Left + (FCard.Width + FColSeparation) * (NumTrj mod FCols);

			FCard.Print(Table, Destination, Mirror);
			Inc(TrjActual);
			FPrintInfo.TrjInfo.Caption := Format(' Tarjeta %d de %d',
																	[TrjActual, FPrintInfo.ProgressBar.Max]);
		end;

		if not Table.EOF then	begin
			FPrintInfo.ProgressBar.Position := TrjActual;
			Table.Next;
			PrxPag := (Posicion.Value - 1) Div (FRows * FCols);
			if NumPag <> PrxPag then begin
				Continuar := PrxPag <= Stop;
				if (NumPag >= Start) and Continuar then Destination.NewPage;
					NumPag := PrxPag;
				FPrintInfo.ProgressInfo.Caption := Format(' P�gina %d de %d', [NumPag+1, Stop+1]);
			end;
		end else Continuar := False;
	end;
	Continuar := not FPrintInfo.Canceled;
	FPrintInfo.Hide;
	FPrintInfo.Free;

	if Continuar then begin
		if Preview then Destination.Preview	else Destination.PrintIt;
	end;
	Destination.Free;

	Table.IndexName := Indice;
	Table.GotoBookmark(Actual);
	Table.FreeBookmark(Actual);
	Table.EnableControls;
end;


begin
	Credenciales := TPage.Create;
end.
