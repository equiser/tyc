(*{$DEFINE DEMOVERSION}*)
Unit Examinar;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Grids, DBGrids, Menus, DB, DBTables, Buttons, ExtCtrls,
  Printers, Base, Pagina, DBCtrls, StdCtrls, Mask, Clipbrd, Nosotros,
  EditDef, ComCtrls, ExtDlgs;

type
  TPrincipal = class(TForm)
    Datos: TDataSource;
    DialogoAbrir: TOpenDialog;
    GridPopupMenu: TPopupMenu;
    SinOrdenar: TMenuItem;
    OrdendeImpresion: TMenuItem;
    N3: TMenuItem;
    OrdenarPorNombre: TMenuItem;
    OrdenarPorLegajo: TMenuItem;
    OrdenarPorTarjeta: TMenuItem;
    OrdenarPorDocumento: TMenuItem;
    N11: TMenuItem;
    Buscar1: TMenuItem;
    MainMenu: TMainMenu;
    Archivo: TMenuItem;
    miNueva: TMenuItem;
    miAbrirTabla: TMenuItem;
    N9: TMenuItem;
    miAbrirDefinicion: TMenuItem;
    N5: TMenuItem;
    miPreview: TMenuItem;
    miPrintTarjetas: TMenuItem;
    miPrintBase: TMenuItem;
    N4: TMenuItem;
    Exit1: TMenuItem;
    Edicion: TMenuItem;
    PrincipalDeshacer: TMenuItem;
    N8: TMenuItem;
    PrincipalCortar: TMenuItem;
    PrincipalCopiar: TMenuItem;
    PrincipalPegar: TMenuItem;
    N7: TMenuItem;
    PrincipalBuscar: TMenuItem;
    PrincipalRemplazar: TMenuItem;
    OrdenarPor: TMenuItem;
    SinOrdenar1: TMenuItem;
    OrdendeImpresion1: TMenuItem;
    N6: TMenuItem;
    OrdenarPorNombre1: TMenuItem;
    OrdenarPorLegajo1: TMenuItem;
    OrdenarPorTarjeta1: TMenuItem;
    OrdenarPorDocumento1: TMenuItem;
    Herramientas: TMenuItem;
    miMarkNone: TMenuItem;
    N12: TMenuItem;
    miPack: TMenuItem;
    miReindex: TMenuItem;
    Ayuda: TMenuItem;
    Contenido1: TMenuItem;
    N14: TMenuItem;
    miAcercaDe: TMenuItem;
    DialogoGuardar: TSaveDialog;
    DialogoBuscar: TFindDialog;
    DialogoImprimir: TPrintDialog;
    ReplaceDialog: TReplaceDialog;
    Origen: TTable;
    Tabla: TTable;
    TablaPOSICION: TFloatField;
    TablaAPELLIDO: TStringField;
    TablaNOMBRE: TStringField;
    TablaDOCUMENTO: TStringField;
    TablaLEGAJO: TStringField;
    TablaTARJETA: TFloatField;
    TablaSECCION: TStringField;
    TablaAREA: TStringField;
    TablaFOTO: TStringField;
    DBGrid: TDBGrid;
    pnStatusBar: TStatusBar;
    sbSpeedBar: TPanel;
    sbExit: TSpeedButton;
    sbMarkNone: TSpeedButton;
    sbEditar: TSpeedButton;
    sbBuscar: TSpeedButton;
    sbAbrirDefincion: TSpeedButton;
    Bevel2: TBevel;
    sbAbrirTabla: TSpeedButton;
    sbPrint: TSpeedButton;
    sbPreview: TSpeedButton;
    DBNavigator2: TDBNavigator;
    DBNavigator1: TDBNavigator;
    pnFicha: TPanel;
    Bevel1: TBevel;
    reLegajo: TDBEdit;
    lbLegajo: TPanel;
    rePosicion: TDBEdit;
    lbPosicion: TPanel;
    lbApellido: TPanel;
    reApellido: TDBEdit;
    lbSeccion: TPanel;
    reSeccion: TDBEdit;
    reArea: TDBEdit;
    lbArea: TPanel;
    reNombre: TDBEdit;
    lbNombre: TPanel;
    lbDocumento: TPanel;
    reDocumento: TDBEdit;
    lbFoto: TPanel;
    reFoto: TDBEdit;
    AbrirFoto: TButton;
    lbTarjeta: TPanel;
    reTarjeta: TDBEdit;
    N13: TMenuItem;
    MarcosdeTexto: TMenuItem;
    Transparente1: TMenuItem;
    Negro1: TMenuItem;
    Rojo1: TMenuItem;
    Verde1: TMenuItem;
    Azul1: TMenuItem;
    Amarillo1: TMenuItem;
    miMarkAll: TMenuItem;
    sbMarkAll: TSpeedButton;
    Blanco1: TMenuItem;
    N15: TMenuItem;
    N16: TMenuItem;
    N17: TMenuItem;
    miAutoFormat: TMenuItem;
    miFormatApellido: TMenuItem;
    miFormatNombre: TMenuItem;
    miAutoCopy: TMenuItem;
    miCopyApellido: TMenuItem;
    miCopyNombre: TMenuItem;
    miCopySeccion: TMenuItem;
    miCopyArea: TMenuItem;
    miCopyFoto: TMenuItem;
    miImportarBasedeDatos1: TMenuItem;
    miExportarASipred: TMenuItem;
    miAutoNum: TMenuItem;
    miTarjeta: TMenuItem;
    miLegajo: TMenuItem;
    miPosicion: TMenuItem;
    miTarjetaPlus: TMenuItem;
    miLegajoPlus: TMenuItem;
    miEditarDefinicion: TMenuItem;
    N18: TMenuItem;
    DialogoAbrirFoto: TOpenPictureDialog;
    PaintBoxFoto: TPaintBox;
    Ver1: TMenuItem;
    miBarradeHerramientas: TMenuItem;
    miFicha: TMenuItem;
    miBarradeEstado: TMenuItem;
    miNoActualizarFoto: TMenuItem;
    miInsertarRegistro: TMenuItem;
    procedure SalirClick(Sender: TObject);
    procedure OrdenNombre(Sender: TObject);
    procedure OrdenNinguno(Sender: TObject);
    procedure OrdenPosicion(Sender: TObject);
    procedure OrdenLegajo(Sender: TObject);
    procedure OrdenTarjeta(Sender: TObject);
    procedure OrdenDocumento(Sender: TObject);
    procedure AbrirTabla(Sender: TObject);
    procedure Imprimir(Sender: TObject);
    procedure NuevaTabla(Sender: TObject);
    procedure AbrirDefinicion(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SearchClick(Sender: TObject);
    procedure OnFind(Sender: TObject);
    procedure AbrirFotoClick(Sender: TObject);
    procedure FotoClick(Sender: TObject);
    procedure Editar(Sender: TObject);
    procedure DatosDataChange(Sender: TObject; Field: TField);
    procedure Pegar(Sender: TObject);
    procedure Copiar(Sender: TObject);
    procedure Cortar(Sender: TObject);
    procedure miMarkNoneClick(Sender: TObject);
    procedure miMarkAllClick(Sender: TObject);
    procedure miAcercaDeClick(Sender: TObject);

	 {Procedimientos Agregados Manualmente}
    Procedure OpenTable(FileName:String; AsReadOnly: Boolean);
    procedure ReplaceClick(Sender: TObject);
    procedure miPackClick(Sender: TObject);
    procedure Contenido(Sender: TObject);
    procedure TablaBeforeInsert(DataSet: TDataset);
    procedure pnFichaResize(Sender: TObject);
    procedure miReindexClick(Sender: TObject);
    procedure SetMColor(Sender: TObject);
		procedure DataGridDrawColumnCell(Sender: TObject; const Rect: TRect;
              DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure miAutoNumClick(Sender: TObject);
    procedure TablaBeforeDelete(DataSet: TDataSet);
    procedure TablaAPELLIDOSetText(Sender: TField; const Text: String);
    procedure TablaNOMBRESetText(Sender: TField; const Text: String);
    procedure TablaAPELLIDOGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure TablaNOMBREGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure miApNomFormatoClick(Sender: TObject);
    procedure miAutoCopyClick(Sender: TObject);
    procedure miPrintBaseClick(Sender: TObject);
    procedure PaintBoxFotoPaint(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure miExportarASipredClick(Sender: TObject);
    procedure miVerClick(Sender: TObject);
    procedure miInsertarRegistroClick(Sender: TObject);
  private
		FMColor: TColor;
    FPicture: TPicture;
    procedure CargarFoto;
  public
    { Public declarations }
  end;

var
  Principal: TPrincipal;


implementation

{$R *.DFM}
uses RepBase, ExportSipred;



{$IFDEF DEMOVERSION}
procedure NoEnDemo;
begin
  Application.MessageBox('Funcion No disponible en la version Demo', 'Informacion',
    mb_Ok + mb_TaskModal + mb_IconInformation);
end;
{$ENDIF}

procedure TPrincipal.SalirClick(Sender: TObject);
begin
	Close
end;

procedure TPrincipal.OrdenNombre(Sender: TObject);
begin
	sbMarkAll.Enabled := True;
	miMarkAll.Enabled := True;
	sbMarkNone.Enabled := True;
	miMarkNone.Enabled := True;
  miPosicion.Enabled := True;
	SinOrdenar.Checked := False;
	SinOrdenar1.Checked := False;
	OrdenarPorNombre.Checked := True;
	OrdenarPorNombre1.Checked := True;
  TablaPOSICION.ReadOnly := False;
  if pnStatusBar.Panels[1].Text <> 'X'then begin
    miLegajo.Enabled := True;
    miTarjeta.Enabled := True;
    miTarjetaPlus.Enabled := True;
    TablaAPELLIDO.ReadOnly := True;
    TablaLEGAJO.ReadOnly := False;
    TablaDOCUMENTO.ReadOnly := False;
  end;
	Tabla.IndexName := 'Nombre';
end;

procedure TPrincipal.OrdenNinguno(Sender: TObject);
begin
	sbMarkAll.Enabled := True;
	miMarkAll.Enabled := True;
	sbMarkNone.Enabled := True;
	miMarkNone.Enabled := True;
  miPosicion.Enabled := True;
	SinOrdenar.Checked := True;
	SinOrdenar1.Checked := True;
	OrdenDeImpresion.Checked := False;
	OrdenarPorNombre.Checked := False;
	OrdenarPorLegajo.Checked := False;
	OrdenarPorTarjeta.Checked := False;
	OrdenarPorDocumento.Checked := False;
	OrdenDeImpresion1.Checked := False;
	OrdenarPorNombre1.Checked := False;
	OrdenarPorLegajo1.Checked := False;
	OrdenarPorTarjeta1.Checked := False;
	OrdenarPorDocumento1.Checked := False;
  TablaPOSICION.ReadOnly := False;
  if pnStatusBar.Panels[1].Text <> 'X'then begin
    miLegajo.Enabled := True;
    miTarjeta.Enabled := True;
    miTarjetaPlus.Enabled := True;
    TablaAPELLIDO.ReadOnly := False;
    TablaLEGAJO.ReadOnly := False;
    TablaDOCUMENTO.ReadOnly := False;
    TablaTARJETA.ReadOnly := False;
  end;
	Tabla.IndexName := '';
end;

procedure TPrincipal.OrdenPosicion(Sender: TObject);
begin
	sbMarkAll.Enabled := False;
	miMarkAll.Enabled := False;
	sbMarkNone.Enabled := False;
	miMarkNone.Enabled := False;
  miPosicion.Enabled := False;
	SinOrdenar.Checked := False;
	SinOrdenar1.Checked := False;
	OrdenDeImpresion.Checked := True;
	OrdenDeImpresion1.Checked := True;
  TablaPOSICION.ReadOnly := True;
  if pnStatusBar.Panels[1].Text <> 'X'then begin
    miLegajo.Enabled := True;
    miTarjeta.Enabled := True;
    miTarjetaPlus.Enabled := True;
    TablaAPELLIDO.ReadOnly := False;
    TablaLEGAJO.ReadOnly := False;
    TablaDOCUMENTO.ReadOnly := False;
    TablaTARJETA.ReadOnly := False;
  end;
	Tabla.IndexName := 'POSICION';
end;

procedure TPrincipal.OrdenLegajo(Sender: TObject);
begin
	sbMarkAll.Enabled := True;
	miMarkAll.Enabled := True;
	sbMarkNone.Enabled := True;
	miMarkNone.Enabled := True;
  miLegajo.Enabled := False;
  miPosicion.Enabled := True;
	SinOrdenar.Checked := False;
	SinOrdenar1.Checked := False;
	OrdenarPorLegajo.Checked := True;
	OrdenarPorLegajo1.Checked := True;
  TablaPOSICION.ReadOnly := False;
  if pnStatusBar.Panels[1].Text <> 'X'then begin
    miTarjeta.Enabled := True;
    miTarjetaPlus.Enabled := True;
    TablaAPELLIDO.ReadOnly := False;
    TablaLEGAJO.ReadOnly := True;
    TablaDOCUMENTO.ReadOnly := False;
    TablaTARJETA.ReadOnly := False;
  end;
	Tabla.IndexName := 'Legajo';
end;

procedure TPrincipal.OrdenTarjeta(Sender: TObject);
begin
	sbMarkAll.Enabled := True;
	miMarkAll.Enabled := True;
	sbMarkNone.Enabled := True;
	miMarkNone.Enabled := True;
  miPosicion.Enabled := True;
  miTarjeta.Enabled := False;
  miTarjetaPlus.Enabled := False;
	SinOrdenar.Checked := False;
	SinOrdenar1.Checked := False;
	OrdenarPorTarjeta.Checked := True;
	OrdenarPorTarjeta1.Checked := True;
  TablaPOSICION.ReadOnly := False;
  if pnStatusBar.Panels[1].Text <> 'X'then begin
    miLegajo.Enabled := True;
    TablaAPELLIDO.ReadOnly := False;
    TablaLEGAJO.ReadOnly := False;
    TablaDOCUMENTO.ReadOnly := False;
    TablaTARJETA.ReadOnly := True;
  end;
	Tabla.IndexName := 'Tarjeta';
end;

Procedure TPrincipal.OrdenDocumento(Sender: TObject);
Begin
	sbMarkAll.Enabled := True;
	miMarkAll.Enabled := True;
	sbMarkNone.Enabled := True;
	miMarkNone.Enabled := True;
  miPosicion.Enabled := True;
	SinOrdenar.Checked := False;
	SinOrdenar1.Checked := False;
	OrdenarPorDocumento.Checked := True;
	OrdenarPorDocumento1.Checked := True;
  TablaPOSICION.ReadOnly := False;
  if pnStatusBar.Panels[1].Text <> 'X'then begin
    miLegajo.Enabled := True;
    miTarjeta.Enabled := True;
    miTarjetaPlus.Enabled := True;
    TablaAPELLIDO.ReadOnly := False;
    TablaLEGAJO.ReadOnly := False;
    TablaDOCUMENTO.ReadOnly := True;
    TablaTARJETA.ReadOnly := False;
  end;
	Tabla.IndexName := 'Documento';
End;
{******************************************************************************}
Procedure TPrincipal.OpenTable(FileName: String; AsReadOnly: Boolean);
Begin
	Tabla.Active := False;
	 If FileExists(FileName) Then
	 Begin
			Tabla.DataBaseName := ExtractFilePath(FileName);
			Tabla.TableName := ExtractFileName(Filename);

			If UpperCase(Filename) = UpperCase(ExtractFilePath(ParamStr(0)))+'NUEVA.DBF' Then
			Begin
				Tabla.ReadOnly := True;
				pnStatusBar.Panels[2].Text := '';
				pnStatusBar.Panels[1].Text := 'X';
				Edicion.Visible := False;
				OrdenarPor.Visible := False;
				Herramientas.Visible := False;
				sbPrint.Enabled := False;
				miPrintTarjetas.Enabled := False;
				miPrintBase.Enabled := False;
        miExportarASipred.Enabled := False;
				sbPreview.Enabled := False;
				miPreview.Enabled := False;
				sbBuscar.Enabled := False;
{        miAuto.Enabled := False;}
				sbMarkAll.Enabled := False;
{				miMarkAll.Enabled := False;}
				sbMarkNone.Enabled := False;
{				miMarkNone.Enabled := False;}
				DBGrid.PopupMenu := nil;
{				miPack.Enabled := False;
				miReindex.Enabled := False;}
		End	Else Begin
				Tabla.ReadOnly := False;
        if AsReadOnly then pnStatusBar.Panels[1].Text := 'X'
        else pnStatusBar.Panels[1].Text := '';
				pnStatusBar.Panels[2].Text := DialogoAbrir.Filename;
				Edicion.Visible := True;
				OrdenarPor.Visible := True;
				Herramientas.Visible := True;
				sbPrint.Enabled := True;
				miPrintTarjetas.Enabled := True;
				miPrintBase.Enabled := True;
        miExportarASipred.Enabled := True;
				sbPreview.Enabled := True;
				miPreview.Enabled := True;
				sbBuscar.Enabled := True;
{        miAuto.Enabled := True;}
				sbMarkAll.Enabled := True;
{				miMarkAll.Enabled := True;}
				sbMarkNone.Enabled := True;
{				miMarkNone.Enabled := True;}
				DBGrid.PopupMenu := GridPopupMenu;
{				miPack.Enabled := True;
				miReindex.Enabled := True;}
		End;
	End;
	Tabla.Active := True;

  TablaPOSICION.ReadOnly := False;

//  miFormatApellido.Checked := False;
//  miFormatNombre.Checked := False;

  TablaLEGAJO.ReadOnly := AsReadOnly;
  TablaAPELLIDO.ReadOnly := AsReadOnly;
  TablaNOMBRE.ReadOnly := AsReadOnly;
  TablaDOCUMENTO.ReadOnly := AsReadOnly;
  TablaTARJETA.ReadOnly := AsReadOnly;
  TablaSECCION.ReadOnly := AsReadOnly;
  TablaAREA.ReadOnly := AsReadOnly;
  TablaFOTO.ReadOnly := AsReadOnly;

  if AsReadOnly or Tabla.ReadOnly then begin
    miTarjeta.Enabled := False;
    miTarjetaPlus.Enabled := False;
    miAutoCopy.Enabled := False;
    miAutoFormat.Enabled := False;
  end else begin
    miTarjeta.Enabled := True;
    miTarjetaPlus.Enabled := True;
    miAutoCopy.Enabled := True;
    miAutoFormat.Enabled := True;
  end;

  OrdenNinguno(nil);
{	If Tabla.RecordCount > 15 Then
	Begin
		Application.MessageBox('Tabla demasiado grande para ' + #13 +
		'la version demo', 'Violaci�n de Licencia',mb_Ok + mb_TaskModal + mb_IconStop);

		Tabla.Active := False;
		Tabla.ReadOnly := False;
		Tabla.TableName := ExtractFilePath(ParamStr(0))+'NUEVA.DBF';

		lbBaseDeDatos.Caption := '';
		miPrintTarjetas.Enabled := True;
		miPrintBase.Enabled := True;
		sbPrint.Enabled := True;
		miPreview.Enabled := True;
		sbPreview.Enabled := True;
    miAuto.Enabled := True;
		sbNinguno.Enabled := True;

		miPack.Enabled := True;
		miReindex.Enabled := True;
		Tabla.Active := True;
	End;}
End;
{******************************************************************************}
Procedure TPrincipal.NuevaTabla(Sender: TObject);
Begin
  {
	DialogoGuardar.Title := 'Crear Base de Datos Para Tarjetas';
	DialogoGuardar.DefaultExt := 'DBF';
	DialogoGuardar.Filter := 'Base de Datos dBase |*.DBF|';
	DialogoGuardar.Options := [ofPathMustExist,ofHideReadOnly];

	If DialogoGuardar.Execute Then
  If ofExtensionDifferent In DialogoGuardar.Options Then
		Application.MessageBox('La extensi�n de la base de datos ' + #13 +
				'debe ser necesariamente DBF', 'Error en la Extensi�n',mb_Ok + mb_TaskModal + mb_IconStop)
	Else If UpperCase(DialogoGuardar.Filename) = UpperCase(ExtractFilePath(ParamStr(0)))+'NUEVA.DBF' Then
		Application.MessageBox('La base de datos origen no puede ser sobreescrita ',
													 'Error',mb_Ok + mb_TaskModal + mb_IconStop)
	Else Begin
		Try
			Screen.Cursor := crHourGlass;
			Origen.Active := True;
			Tabla.Active := False;
			Origen.CopyTable(DialogoGuardar.FileName);
		Except
			On EMyDB_FileExists Do
			Begin
				Screen.Cursor := crDefault;
				if Application.MessageBox('La tabla especificada ya exite. Desea sobreescribirla ?',
													 'Confirmar',mb_YesNo + mb_TaskModal + mb_IconQuestion) = mrYes Then
					Try
							Screen.Cursor := crHourGlass;
							Origen.Overwrite := True;
						Origen.CopyTable(DialogoGuardar.FileName);
							Origen.Overwrite := False;
					Except
							Screen.Cursor := crDefault;
							MessageDlg(Exception(ExceptObject).Message, mtError, [mbOk], 0);
					End;
				End	Else Begin
					Screen.Cursor := crDefault;
					MessageDlg(Exception(ExceptObject).Message, mtError, [mbOk], 0);
				End;
		End;
	End;
	Origen.Active := False;
	OpenTable(DialogoGuardar.FileName,False);
	Tabla.EnableControls;
	Screen.Cursor := crDefault;
  }
End;
{******************************************************************************}
procedure TPrincipal.AbrirTabla(Sender: TObject);
begin
	DialogoAbrir.Title := 'Abrir Base de Datos Para Tarjetas';
	DialogoAbrir.DefaultExt := 'DBF';
	DialogoAbrir.Filter := 'Base de Datos dBase |*.DBF|';
	DialogoAbrir.Options := [ofFileMustExist{,ofReadOnly}];
	DialogoAbrir.Filename := pnStatusBar.Panels[2].Text;
	If DialogoAbrir.Execute Then OpenTable(DialogoAbrir.FileName,
  																			 ofReadOnly in DialogoAbrir.Options);
End;

Procedure TPrincipal.Imprimir(Sender: TObject);
Var
	Indice: String;
	Actual: TBookMark;

  procedure Salir;
  begin
    Tabla.IndexName := Indice;
    Tabla.GotoBookmark(Actual);
    Tabla.FreeBookmark(Actual);
    Tabla.EnableControls;
	  DBGrid.Enabled := True;
  end;

Begin
  DBGrid.Enabled := False;
	Tabla.DisableControls;
	Indice := Tabla.IndexName;
	Actual := Tabla.GetBookmark;
	Tabla.IndexName := 'Posicion';
	Tabla.First;
  if Tabla.EOF then begin
    MessageBeep(0);
    Application.MessageBox('Debe especificar la posici�n'+#13+
    											 'de impresion de al menos un registro','Error',
                           mb_Ok + mb_TaskModal + mb_IconStop);
    Salir;
    Exit;
  end;
  if Credenciales.Description = '' then begin
    MessageBeep(0);
    Application.MessageBox('Debe especificar una'+#13+
    											 'definici�n','Error',
                           mb_Ok + mb_TaskModal + mb_IconStop);
    Salir;
    Exit;
  end;
	DialogoImprimir.MinPage := (TSmallintField(Tabla.FieldByName('Posicion')).Value - 1)
										 Div (Credenciales.Rows * Credenciales.Cols) + 1;
	Tabla.Last;
	DialogoImprimir.MaxPage := (TSmallintField(Tabla.FieldByName('Posicion')).Value - 1)
											 Div (Credenciales.Rows * Credenciales.Cols) + 1;


	DialogoImprimir.FromPage := DialogoImprimir.MinPage;
	DialogoImprimir.ToPage := DialogoImprimir.MaxPage;
	if DialogoImprimir.Execute then	begin
		if (Sender = sbPrint) Or (Sender = miPrintTarjetas) then
      {$IFNDEF DEMOVERSION}
				Credenciales.Print(Tabla, DialogoImprimir.FromPage - 1, DialogoImprimir.ToPage - 1, False, clNone)
      {$ELSE}
        NoEnDemo
      {$ENDIF}
		else if (Sender = sbPreview) Or (Sender = miPreview) then
				Credenciales.Print(Tabla, DialogoImprimir.FromPage - 1, DialogoImprimir.ToPage - 1, True, FMColor)
	end;
  Salir;
end;

procedure TPrincipal.AbrirDefinicion(Sender: TObject);
begin
	DialogoAbrir.Title := 'Abrir Definici�n Para Tarjetas';
	DialogoAbrir.DefaultExt := 'DEF';
	DialogoAbrir.Filter := 'Definici�n de Tarjetas |*.DEF|';
	DialogoAbrir.Options := [ofFileMustExist,ofHideReadOnly];
	DialogoAbrir.Filename := Credenciales.FileName;
	if DialogoAbrir.Execute then begin
		Credenciales.Load(DialogoAbrir.Filename);
		TablaTARJETA.DisplayFormat := Credenciales.Card.DisplayFormat;
		pnStatusBar.Panels[3].Text := Credenciales.Description;
    miEditarDefinicion.Enabled := True;
	end;
end;

procedure TPrincipal.CargarFoto;
var
  FullName: string;
  ValidPicture: Boolean;

  function ValidFile(const FileName: string): Boolean;
  begin
    Result := GetFileAttributes(PChar(FileName)) <> $FFFFFFFF;
  end;

begin
  FullName := TablaFoto.Text;
  ValidPicture := FileExists(FullName) and ValidFile(FullName);
  if ValidPicture then
  try
    FPicture.LoadFromFile(FullName);
  except
    ValidPicture := False;
  end;
  if not ValidPicture then
  begin
    FPicture.Assign(nil);
  end;
  PaintBoxFoto.Invalidate;
end;

procedure TPrincipal.FormDestroy(Sender: TObject);
begin
  FPicture.Free;
end;

procedure TPrincipal.FormCreate(Sender: TObject);
begin
	Application.HelpFile := ExtractFilePath(Application.ExeName) +'T&C.HLP';
	{$IFDEF DEMOVERSION}
  Caption := 'Tarjetas & Credenciales - Version de Evaluacion';
  {$ENDIF}

  //if Height < ReSizeLimit.MinHeight then Height := ReSizeLimit.MinHeight;
  //if Width < ReSizeLimit.MinWidth then Width := ReSizeLimit.MinWidth;
  FMColor := clNone;
  FPicture := TPicture.Create;
	sbAbrirDefincion.Glyph.Handle := LoadBitmap(HInstance, 'DEF_OPEN');
	sbAbrirTabla.Glyph.Handle := LoadBitmap(HInstance, 'TABLE_OPEN');
	sbPrint.Glyph.Handle := LoadBitmap(HInstance, 'PRINT');
	sbPreview.Glyph.Handle := LoadBitmap(HInstance, 'PREVIEW');
	sbBuscar.Glyph.Handle := LoadBitmap(HInstance, 'FIND');
	sbEditar.Glyph.Handle := LoadBitmap(HInstance, 'DEF_EDIT');
	sbMarkNone.Glyph.Handle := LoadBitmap(HInstance, 'NOPRINT');
	sbMarkAll.Glyph.Handle := LoadBitmap(HInstance, 'PRINTALL');
	sbExit.Glyph.Handle := LoadBitmap(HInstance, 'BBCLOSE');
	Origen.DataBaseName := ExtractFilePath(ParamStr(0));
	Origen.TableName := 'NUEVA.DBF';
	Origen.ReadOnly := True;

	Tabla.DataBaseName := ExtractFilePath(ParamStr(0));
	Tabla.TableName := 'NUEVA.DBF';
	Tabla.ReadOnly := True;
	Tabla.Active := True;
end;

procedure TPrincipal.SearchClick(Sender: TObject);
begin
	DialogoBuscar.Position := Point(Left + 100, Top + 100);
	DialogoBuscar.Execute
end;

procedure TPrincipal.OnFind(Sender: TObject);
Var
	ToFind: string;         {String to find}
	FindIn: string;         {String to search}
	Found: integer;         {String found indicator}
//	FoundLen: integer;      {Length of found text}

	Actual: TBookMark;
	{Offset: TPosicion;}
	 Continuar: Boolean;
Begin
	 Tabla.DisableControls;
	 Actual := Tabla.GetBookmark;
	 If Not(frFindNext In DialogoBuscar.Options) Then
	 Begin
		If frDown In DialogoBuscar.Options Then
				Tabla.First
			Else
				Tabla.Last
	 End
	 Else
		Tabla.Next;

	 If Not(frMatchCase In DialogoBuscar.Options) Then
		ToFind := UCase(DialogoBuscar.FindText)
	 Else
		ToFind := DialogoBuscar.FindText;
//	FoundLen := Length(ToFind);

	Continuar := True;
	While Continuar Do Begin
		If Tabla.IndexName = 'Nombre' Then
			FindIn := Tabla.FieldByName('Apellido').Text
		Else If Tabla.IndexName = 'Documento' Then
				FindIn := Tabla.FieldByName('Documento').Text
		Else If Tabla.IndexName = 'Tarjeta' Then
				FindIn := Tabla.FieldByName('Tarjeta').Text
		Else If Tabla.IndexName = 'Legajo' Then
				FindIn := Tabla.FieldByName('Legajo').Text
		Else
			FindIn := Tabla.FieldByName('Apellido').Text;

	 	If Not(frMatchCase In DialogoBuscar.Options) Then FindIn := UCase(FindIn);

	 	Found := Pos(ToFind, FindIn);
	 	If Found > 0 then
			Continuar := False
	 	Else
	 	if frDown In DialogoBuscar.Options then begin
			If Not Tabla.EOF Then
				Tabla.Next
			Else
				Continuar := False;
		end else
		 	If Not Tabla.BOF Then
				Tabla.Prior
			Else
				Continuar := False;
	End;
	DialogoBuscar.CloseDialog;
	If Found = 0 Then	Begin
		Tabla.GotoBookmark(Actual);
		Application.MessageBox('No se encontr� Coincidecia', 'Informaci�n',
												 mb_Ok + mb_IconInformation);
	End;
	Tabla.FreeBookmark(Actual);
	Tabla.EnableControls;
End;

procedure TPrincipal.AbrirFotoClick(Sender: TObject);
begin
  if Tabla.ReadOnly or TablaFoto.ReadOnly then begin
    MessageBeep(0);
  	Exit;
  end;
  if ExtractFileName(TablaFoto.Text) = '' then
    DialogoAbrirFoto.Filename := ExtractFilePath(TablaFoto.Text)
  else
    DialogoAbrirFoto.Filename := TablaFoto.Text;
	if DialogoAbrirFoto.Execute then begin
    Tabla.Edit;
    TablaFoto.Text := DialogoAbrirFoto.Filename;
    FotoClick(Sender);
	end;
end;

procedure TPrincipal.FotoClick(Sender: TObject);
begin
  PaintBoxFoto.Invalidate;
end;

Procedure TPrincipal.Editar(Sender: TObject);
Begin
	EditDefinition.OpenFile(Credenciales.FileName);
  if EditDefinition.WindowState = wsNormal then begin
    EditDefinition.Left := Left;
    EditDefinition.Top := Top;
    EditDefinition.Width := Width;
    EditDefinition.Height := Height;
  end;  
	EditDefinition.Show;
End;

procedure TPrincipal.DatosDataChange(Sender: TObject; Field: TField);
begin
  if (Field = nil) or         //cambio de un registro a otro
     (Field = TablaFOTO) then  CargarFoto; //se edit� el campo foto
end;

Procedure TPrincipal.Cortar(Sender: TObject);
Begin
	If (ActiveControl.ClassName = 'TDBEdit') And Not(Tabla.ReadOnly) Then
  Begin
   	Tabla.Edit;
		Clipboard.AsText := TDBEdit(ActiveControl).SelText;
		TDBEdit(ActiveControl).SelText := ''
  End Else If (ActiveControl.ClassName = 'TDBGrid') And Not(Tabla.ReadOnly) Then
	Begin
   	Tabla.Edit;
   	Clipboard.AsText := TDBGrid(ActiveControl).SelectedField.Text;
   	TDBGrid(ActiveControl).SelectedField.Text := '';
  End;
End;

Procedure TPrincipal.Copiar(Sender: TObject);
Begin
	If ActiveControl.ClassName = 'TDBEdit' Then
		Clipboard.AsText := TDBEdit(ActiveControl).SelText
  Else If ActiveControl.ClassName = 'TDBGrid' Then
   	Clipboard.AsText := TDBGrid(ActiveControl).SelectedField.Text;
End;

Procedure TPrincipal.Pegar(Sender: TObject);
Begin
	If (ActiveControl.ClassName = 'TDBEdit') And Not(Tabla.ReadOnly) Then
  Begin
   	Tabla.Edit;
		TDBEdit(ActiveControl).SelText := Clipboard.AsText
  End Else If (ActiveControl.ClassName = 'TDBGrid') And Not(Tabla.ReadOnly) Then
	Begin
   	Tabla.Edit;
   	TDBGrid(ActiveControl).SelectedField.Text := Clipboard.AsText;
  End;
End;

Procedure TPrincipal.miMarkNoneClick(Sender: TObject);
Var
	Actual: TBookMark;
Begin
  if Tabla.ReadOnly then Exit;
  Tabla.DisableControls;
  Actual := Tabla.GetBookmark;
  Tabla.First;

  While Not Tabla.EOF Do Begin
    Tabla.Edit;
    Tabla.FieldByName('Posicion').Text := '';
    Tabla.Post;
    If Not Tabla.EOF Then Tabla.Next;
  End;
  Tabla.GotoBookmark(Actual);
  Tabla.FreeBookmark(Actual);
  Tabla.EnableControls;
End;

procedure TPrincipal.miMarkAllClick(Sender: TObject);
var
	Actual: TBookMark;
  Pos: LongInt;
begin
  if Tabla.ReadOnly then Exit;
  Tabla.DisableControls;
  Actual := Tabla.GetBookmark;
  Tabla.First;
  Pos := 0;
  while not Tabla.EOF do begin
    Inc(Pos);
    Tabla.Edit;
    Tabla.FieldByName('Posicion').Text := IntToStr(Pos);
    Tabla.Post;
    if not Tabla.EOF then Tabla.Next;
  end;
  Tabla.GotoBookmark(Actual);
  Tabla.FreeBookmark(Actual);
  Tabla.EnableControls;
end;

procedure TPrincipal.miAcercaDeClick(Sender: TObject);
begin
  About.ShowModal;
end;

procedure TPrincipal.ReplaceClick(Sender: TObject);
begin
	ReplaceDialog.Position := Point(Left + 100, Top + 100);
	ReplaceDialog.Execute;
end;

procedure TPrincipal.miPackClick(Sender: TObject);
begin
  {
   If Tabla.Active And Not Tabla.ReadOnly Then
   Begin
   	Tabla.Active := False;
      Tabla.Exclusive := True;
      Try
      	Tabla.Active := True;
      	Tabla.Pack;
        Tabla.Active := False;
      Finally
        Tabla.Active := False;
      	Tabla.Exclusive := False;
        Tabla.Active := True;
      End;
   End
   }
end;

procedure TPrincipal.miReindexClick(Sender: TObject);
begin
  {
	If Tabla.Active And Not Tabla.ReadOnly Then
	Begin
		Tabla.Active := False;
		Tabla.Exclusive := True;
		Try
			Tabla.Active := True;
			Tabla.Reindex;
			Tabla.Active := False;
		Finally
			Tabla.Active := False;
			Tabla.Exclusive := False;
			Tabla.Active := True;
		End;
	End;
  }
end;

procedure TPrincipal.Contenido(Sender: TObject);
begin
 	 Application.HelpCommand(HELP_CONTENTS, 0);
end;

procedure TPrincipal.TablaBeforeInsert(DataSet: TDataset);
begin
(*   if Tabla.RecordCount > 15 then
   begin
   	Application.MessageBox('Tabla demasiado grande para ' + #13 +
      	'la version demo!!!', 'Violaci�n de Licencia',mb_Ok + mb_TaskModal + mb_IconStop);
		Tabla.Cancel;
   end; *)
end;

procedure TPrincipal.pnFichaResize(Sender: TObject);
begin
  Bevel1.Left := pnFicha.ClientWidth - 5 - Bevel1.Width;
  PaintBoxFoto.Left := Bevel1.Left +2;
  AbrirFoto.Left := Bevel1.Left - 5 - AbrirFoto.Width;
  reFoto.Width := AbrirFoto.Left - 5 -reFoto.Left;
  lbFoto.Width := PaintBoxFoto.Left - 5 -lbFoto.Left;
  lbDocumento.Width := Bevel1.Left - 5 -lbDocumento.Left;
  reDocumento.Width := Bevel1.Left - 5 -reDocumento.Left;
end;

procedure TPrincipal.SetMColor(Sender: TObject);
begin
  TMenuItem(Sender).Checked := True;
  if Sender = Transparente1 then FMColor := clNone else
  if Sender = Negro1 then FMColor := clBlack else
  if Sender = Blanco1 then FMColor := clWhite else
  if Sender = Rojo1 then FMColor := clRed else
  if Sender = Verde1 then FMColor := clGreen else
  if Sender = Azul1 then FMColor := clBlue else
  if Sender = Amarillo1 then FMColor := clYellow;
end;

procedure TPrincipal.DataGridDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
   pnStatusBar.Panels[0].Text := IntToStr(DBGrid.SelectedRows.Count) + ' regs.';
end;

procedure TPrincipal.miAutoNumClick(Sender: TObject);
var
  InputStr: String;
  Actual: TBookMark;
  Num, Cont: LongInt;

  procedure MensajeError;
  begin
 		Application.MessageBox('Valor Incorrecto', 'Error',mb_Ok + mb_TaskModal + mb_IconError);
  end;

begin
	if Tabla.ReadOnly then Exit;
	DBGrid.SelectedRows.Refresh;
	Tabla.DisableControls;
	Actual := Tabla.GetBookmark;
	if DBGrid.SelectedRows.Count < 1 then begin
		MessageBeep(0);
		Tabla.GotoBookmark(Actual);
		Tabla.FreeBookMark(Actual);
		Tabla.EnableControls;
		Exit;
	end;
	if Sender = miPosicion then begin
		Cont := 0;
		Tabla.Bookmark := DBGrid.SelectedRows[Cont];
		InputStr := Tabla.FieldByName('Posicion').Text;
		if InputQuery('Auto numerar Posicion', 'Comenzar en:', InputStr) then begin
			try
				Num := StrToInt(InputStr);
				Tabla.Edit;
				Tabla.FieldByName('Posicion').Text := IntToStr(Num);
				Tabla.Post;
				Inc(Num); Inc(Cont);
				while Cont < DBGrid.SelectedRows.Count do begin
					Tabla.Bookmark := DBGrid.SelectedRows[Cont];
					Tabla.Edit;
					Tabla.FieldByName('Posicion').Text := IntToStr(Num);
					Tabla.Post;
					Inc(Num); Inc(Cont);
				end;
			except MensajeError; end;
		end;
	end else
	if Sender = miTarjeta then begin
		Cont := 0;
		Tabla.Bookmark := DBGrid.SelectedRows[Cont];
		InputStr := Tabla.FieldByName('Tarjeta').Text;
		if InputQuery('Auto numerar Tarjeta', 'Comenzar en:', InputStr) then begin
			try
				Num := StrToInt(InputStr);
				Tabla.Edit;
				Tabla.FieldByName('Tarjeta').Value := Num;
				Tabla.Post;
				Inc(Num); Inc(Cont);
				while Cont < DBGrid.SelectedRows.Count do begin
					Tabla.Bookmark := DBGrid.SelectedRows[Cont];
					Tabla.Edit;
					Tabla.FieldByName('Tarjeta').Value := Num;
					Tabla.Post;
					Inc(Num); Inc(Cont);
				end;
			except MensajeError; end;
		end;
	end else
	if Sender = miTarjetaPlus then begin
		Cont := 0;
		Tabla.Bookmark := DBGrid.SelectedRows[Cont];
		InputStr := Tabla.FieldByName('Tarjeta').Text;
		if InputQuery('Auto numerar Tarjeta', 'Incrementar en:', InputStr) then begin
			try
				Num := StrToInt(InputStr);
				Tabla.Edit;
        Tabla.FieldByName('Tarjeta').Value :=
                                      Tabla.FieldByName('Tarjeta').Value +Num;
        Tabla.Post;
        Inc(Num); Inc(Cont);
        while Cont < DBGrid.SelectedRows.Count do begin
          Tabla.Bookmark := DBGrid.SelectedRows[Cont];
          Tabla.Edit;
          Tabla.FieldByName('Tarjeta').Value :=
                                      Tabla.FieldByName('Tarjeta').Value +Num;
          Tabla.Post;
          Inc(Num); Inc(Cont);
        end;
      except MensajeError; end;
    end;
  end;
  Tabla.GotoBookmark(Actual);
  Tabla.FreeBookMark(Actual);
  Tabla.EnableControls;
end;

procedure TPrincipal.TablaBeforeDelete(DataSet: TDataSet);
const
  Borrando: Boolean = False;
begin
  if Tabla.ReadOnly then Abort;
  if Borrando then Exit;
  DBGrid.SelectedRows.Refresh;
  if DBGrid.SelectedRows.Count > 1 then begin
 		if Application.MessageBox('Borrar Todos los registros seleccionados?',
               'Confirmar',MB_YESNO + MB_TASKMODAL + MB_ICONQUESTION) = IDYES then begin
       Borrando := True;
       DBGrid.SelectedRows.Delete;
       Borrando := False;
       Abort;
    end else Abort;
  end else begin
 		if Application.MessageBox('Borrar el registro seleccionado?',
               'Confirmar',MB_YESNO + MB_TASKMODAL + MB_ICONQUESTION) = IDNO then Abort;
  end;
end;

function DelSpace1(const S: string): string;
var
  I: Integer;
begin
  Result := S;
  for I := Length(Result) downto 2 do begin
    if (Result[I] = ' ') and (Result[I - 1] = ' ') then
      Delete(Result, I, 1);
  end;
end;

function Proper(const S: String): String;
var
  SLen, I: Cardinal;
begin
  Result := AnsiLowerCase(S);
  I := 1;
  SLen := Length(Result);
  while I <= SLen do begin
    while (I <= SLen) and (Result[I] = ' ') do Inc(I);
    if I <= SLen then Result[I] := AnsiUpperCase(Result[I])[1];
    while (I <= SLen) and not (Result[I] = ' ') do Inc(I);
  end;
end;

procedure TPrincipal.TablaAPELLIDOSetText(Sender: TField;
  const Text: String);
begin
  if miFormatApellido.Checked then
    TablaAPELLIDO.AsString := UpperCase(DelSpace1(Trim(Text)))
  else
    TablaAPELLIDO.AsString := Text;
end;

procedure TPrincipal.TablaAPELLIDOGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
{  if miFormatApellido.Checked then
    Text := UpperCase(DelSpace1(Trim(TablaAPELLIDO.AsString)))
  else}
    Text := TablaAPELLIDO.AsString;
end;

procedure TPrincipal.TablaNOMBRESetText(Sender: TField;
  const Text: String);
begin
  if miFormatNombre.Checked then
    TablaNOMBRE.AsString := Proper(DelSpace1(Trim(Text)))
  else
    TablaNOMBRE.AsString := Text;
end;

procedure TPrincipal.TablaNOMBREGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
{  if miFormatNombre.Checked then
    Text := Proper(DelSpace1(Trim(TablaNOMBRE.AsString)))
  else}
    Text := TablaNOMBRE.AsString;
end;

procedure TPrincipal.miApNomFormatoClick(Sender: TObject);
begin
  with Sender as TMenuItem do Checked := not Checked;
end;

procedure TPrincipal.miAutoCopyClick(Sender: TObject);
var
  InputStr, Campo: String;
  Actual: TBookMark;
  Cont: LongInt;

  procedure MensajeError;
  begin
 		Application.MessageBox('No se pudo copiar.', 'Error',mb_Ok + mb_TaskModal + mb_IconError);
  end;

begin
  if Tabla.ReadOnly then Exit;
  DBGrid.SelectedRows.Refresh;
  Tabla.DisableControls;
  Actual := Tabla.GetBookmark;
  if DBGrid.SelectedRows.Count < 1 then begin
    MessageBeep(0);
    Tabla.GotoBookmark(Actual);
    Tabla.FreeBookMark(Actual);
    Tabla.EnableControls;
  	Exit;
  end;

  if Sender = miCopyApellido then begin
    Campo := 'Apellido';
  end else
  if Sender = miCopyNombre then begin
    Campo := 'Nombre';
  end else
  if Sender = miCopyArea then begin
    Campo := 'Area';
  end else
  if Sender = miCopySeccion then begin
    Campo := 'Seccion';
  end else
  if Sender = miCopyFoto then begin
    Campo := 'Foto';
  end;

  Cont := 0;
  Tabla.Bookmark := DBGrid.SelectedRows[Cont];
  InputStr := Tabla.FieldByName(Campo).Text;
  if InputQuery('Auto Copiar ' + Campo, 'Completar con:', InputStr) then begin
    try
      while Cont < DBGrid.SelectedRows.Count do begin
        Tabla.Bookmark := DBGrid.SelectedRows[Cont];
        Tabla.Edit;
        Tabla.FieldByName(Campo).Text := InputStr;
        Tabla.Post;
        Inc(Cont);
      end;
    except MensajeError; end;
  end;

  Tabla.GotoBookmark(Actual);
  Tabla.FreeBookMark(Actual);
  Tabla.EnableControls;
end;
procedure TPrincipal.miPrintBaseClick(Sender: TObject);
begin
  Enabled := False;
    with FormPreviewBase do begin
      FormPreviewBase.QuickRepBaseDatos.ReportTitle := pnStatusBar.Panels[2].Text;
      FormPreviewBase.QuickRepBaseDatos.Preview;
    end;
  Enabled := True;
end;

procedure TPrincipal.PaintBoxFotoPaint(Sender: TObject);
var
  DrawRect: TRect;
  SNone: string;
begin
  if miNoActualizarFoto.Checked then begin
    with TPaintBox(Sender) do begin
      Canvas.Brush.Color := Color;
      DrawRect := ClientRect;
      with DrawRect, Canvas do begin
        PolyLine([Point(Left,Top),Point(Right,Bottom)]);
        PolyLine([Point(Left,Bottom),Point(Right,Top)]);
      end;
    end;
  end else
  with TPaintBox(Sender) do begin
    Canvas.Brush.Color := Color;
    DrawRect := ClientRect;
    if FPicture.Width > 0 then begin
      with DrawRect do
        if (FPicture.Width > Right - Left) or (FPicture.Height > Bottom - Top) then
        begin
          if FPicture.Width > FPicture.Height then
            Bottom := Top + MulDiv(FPicture.Height, Right - Left, FPicture.Width)
          else
            Right := Left + MulDiv(FPicture.Width, Bottom - Top, FPicture.Height);
          if DrawRect.Right < ClientRect.Right then
          	OffsetRect(DrawRect, (ClientRect.Right -DrawRect.Right) div 2, 0);
          if DrawRect.Bottom < ClientRect.Bottom then
          	OffsetRect(DrawRect, 0, (ClientRect.Bottom -DrawRect.Bottom) div 2);
          Canvas.StretchDraw(DrawRect, FPicture.Graphic);
        end else with DrawRect do
          Canvas.Draw(Left + (Right - Left - FPicture.Width) div 2, Top + (Bottom - Top -
                      FPicture.Height) div 2, FPicture.Graphic);
    end else with DrawRect, Canvas do begin
      if TablaFoto.Text <> '' then SNone := 'No se Encontr�'
      else SNone := 'Sin Imagen';
      TextOut(Left + (Right - Left - TextWidth(SNone)) div 2, Top + (Bottom -
              Top - TextHeight(SNone)) div 2, SNone);
    end;
  end;
end;

procedure TPrincipal.miExportarASipredClick(Sender: TObject);
begin
  if Sipred.ShowModal = mrOk then
  {$IFNDEF DEMOVERSION}
    Sipred.Exportar;
  {$ELSE}
    NoEnDemo;
  {$ENDIF}
end;

procedure TPrincipal.miVerClick(Sender: TObject);
begin
  if Sender = miBarradeHerramientas then begin
    miBarradeHerramientas.Checked := not miBarradeHerramientas.Checked;
    sbSpeedBar.Visible := miBarradeHerramientas.Checked;
    sbSpeedBar.Top := 0;
  end else if Sender = miFicha then begin
    miFicha.Checked := not miFicha.Checked;
    pnFicha.Visible := miFicha.Checked;
    pnFicha.Top := sbSpeedBar.Height;
  end else if Sender = miBarradeEstado then begin
    miBarradeEstado.Checked := not miBarradeEstado.Checked;
    pnStatusBar.Visible := miBarradeEstado.Checked;
  end else if Sender = miNoActualizarFoto then begin
    miNoActualizarFoto.Checked := not miNoActualizarFoto.Checked;
		PaintBoxFoto.Invalidate;
	end;
end;

procedure TPrincipal.miInsertarRegistroClick(Sender: TObject);
begin
if Tabla.ReadOnly then Exit;
if (Tabla.State = dsEdit) or (Tabla.State = dsInsert) then Tabla.Post;
	Tabla.Append;
	DBGrid.SelectedIndex := 1;
end;

End.
