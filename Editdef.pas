unit Editdef;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, ExtCtrls, Buttons, StdCtrls, Pagina, Menus, ComCtrls;

type
  TEditDefinition = class(TForm)
    OpenDialog: TOpenDialog;
    SaveDialog: TSaveDialog;
    MainMenu: TMainMenu;
    mbFile: TMenuItem;
    mbHelp: TMenuItem;
    mbEdit: TMenuItem;
    miUndo: TMenuItem;
    N1: TMenuItem;
    miCopy: TMenuItem;
    miPaste: TMenuItem;
    miCut: TMenuItem;
    N2: TMenuItem;
    miFind: TMenuItem;
    miReplace: TMenuItem;
    ReplaceDialog: TReplaceDialog;
    miNew: TMenuItem;
    miOpen: TMenuItem;
    miSave: TMenuItem;
    miSaveAs: TMenuItem;
    N3: TMenuItem;
    miPrint: TMenuItem;
    N4: TMenuItem;
    miExit: TMenuItem;
    miAbout: TMenuItem;
    Contenidos1: TMenuItem;
    N5: TMenuItem;
    Edit: TMemo;
    StatusBar: TStatusBar;
    Panel1: TPanel;
    sbExit: TSpeedButton;
    sbSearch: TSpeedButton;
    sbPrint: TSpeedButton;
    sbSave: TSpeedButton;
    sbOpen: TSpeedButton;
    Bevel: TBevel;
    procedure ExitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure OpenClick(Sender: TObject);
    procedure SaveClick(Sender: TObject);
    procedure CutClick(Sender: TObject);
    procedure CopyClick(Sender: TObject);
    procedure PasteClick(Sender: TObject);
    procedure FindClick(Sender: TObject);
    procedure SaveAsClick(Sender: TObject);
    procedure EditChange(Sender: TObject);
    procedure miUndoClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Contenidos1Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
    FModified: Boolean;
    FFileName: String;
    FBuffer1: PChar;
    FBuffer2: PChar;
  public
    { Public declarations }
    Procedure OpenFile(Source: String);
    Procedure SaveFile;
  end;

var
  EditDefinition: TEditDefinition;

implementation

Uses Examinar;
{$R *.DFM}

procedure TEditDefinition.ExitClick(Sender: TObject);
begin
	Close;
	If Not Visible Then Principal.Show;
end;

procedure TEditDefinition.FormCreate(Sender: TObject);
begin
	FBuffer1 := StrAlloc(1);
	FBuffer2 := StrAlloc(1);
	FBuffer1^ := #0;
	FBuffer2^ := #0;
	FModified := False;

	sbOpen.Glyph.Handle := LoadBitmap(HInstance, 'DEF_OPEN');
	sbSave.Glyph.Handle := LoadBitmap(HInstance, 'DEF_SAVE');
	sbPrint.Glyph.Handle := LoadBitmap(HInstance, 'PRINT');
	sbSearch.Glyph.Handle := LoadBitmap(HInstance, 'FIND');
	sbExit.Glyph.Handle := LoadBitmap(HInstance, 'BBCLOSE');
end;

Procedure TEditDefinition.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
Var
	Result: Word;
Begin
	If FModified Then
	 Begin
		Result := Application.MessageBox('Desea salvar las modificaciones?',
													 'Confirmar',mb_YesNoCancel + mb_TaskModal + mb_IconQuestion);
		If Result = mrYes Then
			SaveFile
		Else If Result = mrNo Then
			FModified := False
		Else If Result = mrCancel Then
			CanClose := False;
	 End
end;


Procedure TEditDefinition.OpenFile;
Var
	Option: Word;
	 Continue: Boolean;
Begin
	if FModified then begin
		Option := Application.MessageBox('Desea salvar las modificaciones?',
													 'Confirmar',mb_YesNoCancel + mb_TaskModal + mb_IconQuestion);
		if Option = mrYes then SaveFile
		else if Option = mrNo then FModified := False;
		Continue := (Option <> mrCancel);
	end else Continue := True;

	if Continue then begin
		if (FileExists(Source)) then begin
			StatusBar.SimpleText := Source;
			FFileName := Source;
			Edit.Lines.LoadFromFile(Source);
		end	else begin
			StatusBar.SimpleText := 'Sin Nombre';
			FFileName := '';
			Edit.Lines.Clear;
		end;
		StrDispose(FBuffer1);
		StrDispose(FBuffer2);
		FBuffer1 := StrAlloc(Edit.GetTextLen + 1);
		FBuffer2 := StrAlloc(Edit.GetTextLen + 1);
		Edit.GetTextBuf(FBuffer1, StrBufSize(FBuffer1));
		Edit.GetTextBuf(FBuffer2, StrBufSize(FBuffer2));
		FModified := False;
	end;
End;

Procedure TEditDefinition.SaveFile;
Begin
	SaveDialog.Title := 'Salvar Definición Para Tarjetas';
	SaveDialog.DefaultExt := 'DEF';
	SaveDialog.Filter := 'Definición de Tarjetas |*.DEF|';
	SaveDialog.Options := [ofPathMustExist];
	SaveDialog.Filename := '';
	if (FFileName = '') And SaveDialog.Execute then begin
		StatusBar.SimpleText := SaveDialog.FileName;
		FFileName := SaveDialog.FileName;
	end;
	if FFileName <> '' then begin
			Edit.Lines.SaveToFile(FFileName);
			if Credenciales.FileName = FFileName then begin
				Credenciales.Load(FFileName);
				Principal.TablaTARJETA.DisplayFormat := Credenciales.Card.DisplayFormat;
			end;
			FModified := False;
	end;
End;

procedure TEditDefinition.OpenClick(Sender: TObject);
begin
	OpenDialog.Title := 'Abrir Definición Para Tarjetas';
   OpenDialog.DefaultExt := 'DEF';
   OpenDialog.Filter := 'Definición de Tarjetas |*.DEF|';
   OpenDialog.Options := [ofFileMustExist];
   OpenDialog.Filename := FFileName;
	If OpenDialog.Execute Then
   	OpenFile(OpenDialog.FileName);
end;

procedure TEditDefinition.SaveClick(Sender: TObject);
begin
	SaveFile
end;

Procedure TEditDefinition.CutClick(Sender: TObject);
Begin
	Edit.CutToClipboard
End;

Procedure TEditDefinition.CopyClick(Sender: TObject);
Begin
	Edit.CopyToClipboard
End;

Procedure TEditDefinition.PasteClick(Sender: TObject);
Begin
	Edit.PasteFromClipboard
End;

Procedure TEditDefinition.FindClick(Sender: TObject);
Begin
  ReplaceDialog.Position := Point(Left + 100, Top + 100);
	ReplaceDialog.Execute
End;

procedure TEditDefinition.SaveAsClick(Sender: TObject);
Var
	Aux: String;
begin
	Aux := FFileName;
  FFileName := '';
	SaveFile;
  If FFileName = '' Then FFileName := Aux;
end;

procedure TEditDefinition.EditChange(Sender: TObject);
begin
	If Edit.Modified Then
   Begin
   	FModified := True;
   	Edit.Modified := False;

      StrDispose(FBuffer2);
      FBuffer2 := FBuffer1;
      FBuffer1 := StrAlloc(Edit.GetTextLen + 1);
		Edit.GetTextBuf(FBuffer1,StrBufSize(FBuffer1));
   End;
end;

procedure TEditDefinition.miUndoClick(Sender: TObject);
Var
	Aux:	PChar;
begin
   Aux := FBuffer1;
   FBuffer1 := FBuffer2;
   FBuffer2 := Aux;

   Edit.SetTextBuf(FBuffer1);
end;

procedure TEditDefinition.FormDestroy(Sender: TObject);
begin
	StrDispose(FBuffer1);
	StrDispose(FBuffer2);
end;

procedure TEditDefinition.Contenidos1Click(Sender: TObject);
begin
 	 Application.HelpCommand(HELP_CONTENTS, 0);
end;

procedure TEditDefinition.FormResize(Sender: TObject);
begin
	Bevel.Width := Width+50
end;

end.
