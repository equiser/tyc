unit Imagen;

Interface
	Uses
   	Classes, IniFiles, Graphics, Base, Printers, DBTables, SysUtils, XPrinter;

  Type
	TXGraphic = Class(TArea)
 	Private
    FFileName: String;
  Public
    Property FileName: String Read FFileName Write FFileName;

    Constructor Create;
    Procedure Assign(Source: TXGraphic);
    Procedure Load(Section: String; Source: TCustomIniFile);
    Procedure Print(OffsetX, OffsetY: Real; Table: TTable; Destination: TXPrinter);
  End;

Implementation
	{Metodos Publicos ---------------------------------------------------------}
   Constructor TXGraphic.Create;
   Begin
   	Inherited Create;
      FFileName := '';
   End;

   Procedure TXGraphic.Assign;
   Begin
   	Inherited Assign(Source);
      FFileName := Source.FileName;
   End;

   Procedure TXGraphic.Load;
   Begin
   	Inherited Load(Section, Source);
      FFileName := Source.ReadString(Section, 'Archivo', '');
   End;

	Procedure TXGraphic.Print;

		Function Buscar(Cadena: String; Delimitador: String): String;
		Var
	   	SubCadena: String;
		Begin
	   	Buscar := '';
	   	If Pos(Delimitador, Cadena) > 0 Then
	      Begin
	      	SubCadena := Copy(Cadena, Pos(Delimitador, Cadena) + 1, Length(Cadena));
		   	If Pos(Delimitador, SubCadena) > 0 Then
	         	Buscar := Copy(Cadena, Pos(Delimitador, Cadena) + 1, Pos(Delimitador, SubCadena) - 1);
	      End;
	   End;

	   Function Reemplazar(Cadena, Campo, Valor: String): String;
	   Var
	   	Posicion: Integer;
	   Begin
	     	Posicion := Pos(Campo, Cadena);
	      If Posicion > 0 Then
   	   	Reemplazar := Copy(Cadena, 1, Posicion - 1) + Valor +
      	   				  Copy(Cadena, Posicion + Length(Campo), Length(Cadena))
	      Else
	      	Reemplazar := Cadena;
	   End;

   Var
      Cadena: String;
      Campo: String;
      Bitmap: TBitmap;
      MetaFile: TMetafile;
      Icon: TIcon;
   Begin
   	If Visible Then
      Begin
				 Cadena := FileName;
				While Buscar(Cadena,'&') <> '' Do
				 Begin
					Campo := Buscar(Cadena, '&');
						Try
							Cadena := Reemplazar(Cadena, '&' + Campo + '&', Table.FieldByName(Campo).Text);
						Except
							 Cadena := Reemplazar(Cadena, '&' + Campo + '&', '');
						End;
				 End;
			If FileExists(Cadena) Then
				 Begin
					If UCase(ExtractFileExt(Cadena)) = '.BMP' Then
						Begin
							Bitmap := TBitmap.Create;
							try
								Bitmap.LoadFromFile(Cadena);
                Destination.Bitmap(OffsetX + Left, OffsetY + Top,
                  OffsetX + Left + Width, OffsetY + Top + Height, Bitmap);
							finally
								Bitmap.Free;
							end;
						End	Else If UCase(ExtractFileExt(Cadena)) = '.WMF' Then	Begin
							MetaFile := TMetafile.Create;
							try
								MetaFile.LoadFromFile(Cadena);
								Destination.Metafile(OffsetX + Left, OffsetY + Top,
								  OffsetX + Left + Width, OffsetY + Top + Height, MetaFile);
							finally
								MetaFile.Free;
							end;
						End	Else If UCase(ExtractFileExt(Cadena)) = '.ICO' Then	Begin
							Bitmap := TBitmap.Create;
							try
								Icon := TIcon.Create;
								Icon.LoadFromFile(Cadena);
								Bitmap.Height := Icon.Height;
								Bitmap.Width := Icon.Width;
								Bitmap.Canvas.Draw(0,0,Icon);
								Destination.Bitmap(OffsetX + Left, OffsetY + Top,
								  OffsetX + Left + Width, OffsetY + Top + Height, Bitmap);
							finally
								Icon.Free;
								Bitmap.Free;
							end;
						End;
				End;
			End;
	 End;


End.
