�
 TSIPRED 0U  TPF0TSipredSipredLeft� TopwBorderStylebsDialogCaptionExportar Base de Datos a SipredClientHeight6ClientWidth�Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style PixelsPerInch`
TextHeight TPanel	pnArchivoLeft TopWidth�Height
BevelOuter	bvLoweredTabOrder  TPanel	lbArchivoLeftTopWidth5Height	AlignmenttaLeftJustifyCaption ArchivoFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TEdit	reArchivoLeft<TopWidth�HeightTabOrderTextLegajos.txt  TButton	pbArchivoLeft�TopWidthHeightCaption...TabOrderOnClickpbArchivoClick   TPanelpnPrimerLeft Top� Width� Heighti
BevelOuter	bvLoweredTabOrder TPanellbPrimerLeftTopWidth� Height	AlignmenttaLeftJustifyCaption 1� Nivel de AgrupamientoFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TRadioButtonrbPrimerSectorLeftTop WidthqHeightCaptionCampo SectorTabOrderOnClickrbPrimerClick  TRadioButtonrbPrimerAreaLeftTop8WidthqHeightCaption
Campo AreaTabOrderOnClickrbPrimerClick  TRadioButtonrbPrimerPropioLeftTopPWidthqHeightChecked	TabOrderTabStop	OnClickrbPrimerClick  TEditrePrimerPropioLeftTopNWidth� HeightTabOrder   TPanel	pnSegundoLeft� Top� Width� Heighti
BevelOuter	bvLoweredTabOrder TPanel	lbSegundoLeftTopWidth� Height	AlignmenttaLeftJustifyCaption 2� Nivel de AgrupamientoFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TRadioButtonrbSegundoSectorLeftTop WidthqHeightCaptionCampo SectorTabOrderOnClickrbSegundoClick  TRadioButtonrbSegundoAreaLeftTop8WidthqHeightCaption
Campo AreaTabOrderOnClickrbSegundoClick  TRadioButtonrbSegundoPropioLeftTopPWidthqHeightChecked	TabOrderTabStop	OnClickrbSegundoClick  TEditreSegundoPropioLeftTopNWidth� HeightTabOrder   TPanel	pnTerceroLeftPTop� Width� Heighti
BevelOuter	bvLoweredTabOrder TPanellbTercerLeftTopWidth� Height	AlignmenttaLeftJustifyCaption 3� Nivel de AgrupamientoFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TRadioButtonrbTercerSectorLeftTop WidthqHeightCaptionCampo SectorTabOrderOnClickrbTercerClick  TRadioButtonrbTercerAreaLeftTop8WidthqHeightCaption
Campo AreaTabOrderOnClickrbTercerClick  TRadioButtonrbTercerPropioLeftTopPWidthqHeightChecked	TabOrderTabStop	OnClickrbTercerClick  TEditreTercerPropioLeftTopNWidth� HeightTabOrder   TPanelpnOcupacionLeft Top$Width� Heighti
BevelOuter	bvLoweredTabOrder TRadioButtonrbOcupacionPropioLeftTopPWidthqHeightCaptionRadioButton3Checked	TabOrderTabStop	OnClickrbOcupacionClick  TPanellbOcupacionLeftTopWidth� Height	AlignmenttaLeftJustifyCaption
 Ocupaci�nFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TRadioButtonrbOcupacionSectorLeftTop Width]HeightCaptionCampo SectorTabOrderOnClickrbOcupacionClick  TRadioButtonrbOcupacionAreaLeftTop8Width]HeightCaption
Campo AreaTabOrderOnClickrbOcupacionClick  TRadioButtonrbOcupacionDocumentoLeftpTop WidthqHeightCaptionCampo DocumentoTabOrderOnClickrbOcupacionClick  TEditreOcupacionPropioLeftTopNWidth� HeightTabOrder  TRadioButtonrbOcupacionFotoLeftpTop8WidthqHeightCaption
Campo FotoTabOrderOnClickrbOcupacionClick   TPanelpnDatosVariosLeft� Top$Width� Heighti
BevelOuter	bvLoweredTabOrder TRadioButtonrbVariosPropioLeftTopPWidthqHeightCaptionRadioButton3TabOrderOnClickrbVariosClick  TPanellbDatosVariosLeftTopWidth� Height	AlignmenttaLeftJustifyCaption Datos VariosFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TRadioButtonrbVariosSectorLeftTop Width]HeightCaptionCampo SectorTabOrderOnClickrbVariosClick  TRadioButtonrbVariosAreaLeftTop8Width]HeightCaption
Campo AreaTabOrderOnClickrbVariosClick  TRadioButtonrbVariosDocumentoLeftpTop WidthqHeightCaptionCampo DocumentoChecked	TabOrderTabStop	OnClickrbVariosClick  TEditreVariosPropioLeftTopNWidth� HeightEnabledTabOrder  TRadioButtonrbVariosFotoLeftpTop8WidthqHeightCaption
Campo FotoTabOrderOnClickrbVariosClick   TPanelpnVariosLeft Top� WidthMHeight9
BevelOuter	bvLoweredTabOrder 	TCheckBox	cbHorarioLeftTop WidtheHeightCaptionCumple HorarioState	cbCheckedTabOrder   	TCheckBoxcbBajaLeftvTop WidtheHeightCaptionDado de BajaTabOrder  	TCheckBoxcbExtrasLeft� Top WidtheHeightCaptionExtrasState	cbCheckedTabOrder  TPanellbMiscelaneosLeftTopWidthEHeight	AlignmenttaLeftJustifyCaption MicelaneosFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TButton
pbCancelarLeft�TopWidthKHeightCancel	CaptionCancelarModalResultTabOrder  TButton	pbAceptarLeftTTopWidthKHeightCaptionAceptarDefault	ModalResultTabOrderOnClickpbAceptarClick  TSaveDialogDialogoGuardar
DefaultExtTXTFilter/Archivos de texto |*.TXT|Todos los archivos|*.*OptionsofHideReadOnlyofPathMustExist TitleArchivo de DestinoLeftTop�    