�
 TPRINCIPAL 0f>  TPF0
TPrincipal	PrincipalLeftYTop� WidthHeight�HorzScrollBar.VisibleCaptionTarjetas & CredencialesColor	clBtnFaceConstraints.MinWidthFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameSystem
Font.Style MenuMainMenuOldCreateOrder	Position	poDefaultScaledOnCreate
FormCreate	OnDestroyFormDestroyPixelsPerInch`
TextHeight TDBGridDBGridLeft Top� WidthwHeightAlignalClientCtl3D	
DataSourceDatosFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style Options	dgEditingdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgAlwaysShowSelectiondgCancelOnExitdgMultiSelect ParentCtl3D
ParentFontTabOrder TitleFont.CharsetANSI_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.StylefsBold OnDrawColumnCellDataGridDrawColumnCellOnEditButtonClickAbrirFotoClickColumnsExpanded	FieldNamePOSICIONTitle.Caption	   PosiciónWidth7Visible	 Expanded	FieldNameLEGAJOVisible	 Expanded	FieldNameAPELLIDOWidth@Visible	 Expanded	FieldNameNOMBREWidth@Visible	 Expanded	FieldName	DOCUMENTOWidth@Visible	 Expanded	FieldNameTARJETAWidth@Visible	 Expanded	FieldNameSECCIONTitle.CaptionSeccionWidth@Visible	 ButtonStylecbsEllipsisExpanded	FieldNameFOTOWidth@Visible	    
TStatusBarpnStatusBarLeft Top�WidthwHeightPanelsWidthP BevelpbNoneTextXWidth Width, Width, BevelpbNoneWidth2    TPanel
sbSpeedBarLeft Top WidthwHeight$AlignalTop
BevelOuterbvNoneTabOrder TSpeedButtonsbExitLeftTopWidthHeightHintTerminar
AllowAllUp	Flat		NumGlyphsParentShowHintShowHint	OnClick
SalirClick  TSpeedButton
sbMarkNoneLeft�TopWidthHeightHintNo Imprmir Ninguno
AllowAllUp	EnabledFlat	ParentShowHintShowHint	OnClickmiMarkNoneClick  TSpeedButtonsbEditarLeft� TopWidthHeightHint   Editar Definición
AllowAllUp	Flat	ParentShowHintShowHint	OnClickEditar  TSpeedButtonsbBuscarLeftxTopWidthHeightHintBuscar
AllowAllUp	EnabledFlat	ParentShowHintShowHint	OnClickSearchClick  TSpeedButtonsbAbrirDefincionLeftTopWidthHeightHint   Abrir Definición
AllowAllUp	Flat	ParentShowHintShowHint	OnClickAbrirDefinicion  TBevelBevel2Left Top WidthwHeightAlignalTop  TSpeedButtonsbAbrirTablaLeftTopWidthHeightHintAbrir Base de Datos
AllowAllUp	Flat	ParentShowHintShowHint	OnClick
AbrirTabla  TSpeedButtonsbPrintLeft:TopWidthHeightHintImprimir Tarjetas
AllowAllUp	EnabledFlat	ParentShowHintShowHint	OnClickImprimir  TSpeedButton	sbPreviewLeftVTopWidthHeightHint   Presentación Preliminar
AllowAllUp	EnabledFlat	ParentShowHintShowHint	OnClickImprimir  TSpeedButton	sbMarkAllLeft�TopWidthHeightHintImprmir Todos
AllowAllUp	EnabledFlat	ParentShowHintShowHint	OnClickmiMarkAllClick  TDBNavigatorDBNavigator2LeftLTopWidthlHeight
DataSourceDatosVisibleButtonsnbFirstnbPriornbNextnbLast Flat	Hints.StringsPrimer registroRegistro anteriorRegistro siguiente   Último registro ParentShowHintConfirmDeleteShowHint	TabOrder   TDBNavigatorDBNavigator1Left� TopWidth� Height
DataSourceDatosVisibleButtonsnbInsertnbDeletenbEditnbPostnbCancel Flat	Hints.Strings    Insertar registroBorrar registroEditar registro   Aceptar edición   Cancelar edición ParentShowHintConfirmDeleteShowHint	TabOrder   TPanelpnFichaLeft Top$WidthwHeightgAlignalTop
BevelOuter	bvLoweredFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnResizepnFichaResize TBevelBevel1Left"TopWidthQHeight`StylebsRaised  	TPaintBoxPaintBoxFotoLeft$TopWidthMHeight\OnClick	FotoClick
OnDblClickAbrirFotoClickOnPaintPaintBoxFotoPaint  TDBEditreLegajoLeftBTopWidthBHeightAutoSize	DataFieldLEGAJO
DataSourceDatosFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TPanellbLegajoLeftBTopWidthBHeight	AlignmenttaLeftJustifyCaption  LegajoFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder  TDBEdit
rePosicionLeftTopWidth:HeightAutoSize	DataFieldPOSICION
DataSourceDatosFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TPanel
lbPosicionLeftTopWidth:Height	AlignmenttaLeftJustifyCaption
    PosiciónFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TPanel
lbApellidoLeft� TopWidth� Height	AlignmenttaLeftJustifyCaption
  ApellidoFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder  TDBEdit
reApellidoLeft� TopWidth� HeightAutoSize	DataFieldAPELLIDO
DataSourceDatosFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TPanel	lbSeccionLeftpTop6Width� Height	AlignmenttaLeftJustifyCaption
     SecciónFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder  TDBEdit	reSeccionLeftpTopNWidth� HeightAutoSize	DataFieldSECCION
DataSourceDatosFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBEditreAreaLeft$TopNWidth� HeightAutoSize	DataFieldAREA
DataSourceDatosFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TPanellbAreaLeft$Top6Width� Height	AlignmenttaLeftJustifyCaption  AreaFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder  TDBEditreNombreLeft(TopWidth� HeightAutoSize	DataFieldNOMBRE
DataSourceDatosFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TPanellbNombreLeft(TopWidth� Height	AlignmenttaLeftJustifyCaption  NombreFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder  TPanellbDocumentoLeft�TopWidthSHeight	AlignmenttaLeftJustifyCaption
 DocumentoFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder  TDBEditreDocumentoLeft�TopWidthSHeightAutoSize	DataField	DOCUMENTO
DataSourceDatosFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder	  TPanellbFotoLeft�Top6WidthKHeight	AlignmenttaLeftJustifyCaption FotoFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder  TDBEditreFotoLeft�TopNWidth3HeightAutoSizeCharCaseecUpperCase	DataFieldFOTO
DataSourceDatosFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TButton	AbrirFotoLeftTopNWidthHeightCaption...TabOrderOnClickAbrirFotoClick  TPanel	lbTarjetaLeftTop6WidthiHeight	AlignmenttaLeftJustifyCaption	  TarjetaFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder
  TDBEdit	reTarjetaLeftTopNWidthiHeightAutoSize	DataFieldTARJETA
DataSourceDatosFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TDataSourceDatosDataSetTablaOnDataChangeDatosDataChangeLeftpTop  TOpenDialogDialogoAbrirOptions
ofReadOnlyofFileMustExist Left� Top�   
TPopupMenuGridPopupMenuLeft0Top�  	TMenuItem
SinOrdenarCaptionSin OrdenarChecked	OnClickOrdenNinguno  	TMenuItemOrdendeImpresionCaption   Posición de Impresión	RadioItem	OnClickOrdenPosicion  	TMenuItemN3Caption-	RadioItem	  	TMenuItemOrdenarPorNombreCaptionOrdenar Por Nombre	RadioItem	OnClickOrdenNombre  	TMenuItemOrdenarPorLegajoCaptionOrdenar Por Legajo	RadioItem	OnClickOrdenLegajo  	TMenuItemOrdenarPorTarjetaCaptionOrdenar Por Tarjeta	RadioItem	OnClickOrdenTarjeta  	TMenuItemOrdenarPorDocumentoCaptionOrdenar Por Documento	RadioItem	OnClickOrdenDocumento  	TMenuItemN11Caption-  	TMenuItemBuscar1Caption	Buscar...OnClickSearchClick   	TMainMenuMainMenuLeft0Top�  	TMenuItemArchivoCaption&Archivo 	TMenuItemmiNuevaCaption&Nueva Base de Datos...EnabledShortCutN@OnClick
NuevaTabla  	TMenuItemmiAbrirTablaCaption&Abrir Base de Datos...ShortCutB@OnClick
AbrirTabla  	TMenuItemN18Caption-  	TMenuItemmiImportarBasedeDatos1Caption&Importar Base de DatosEnabled  	TMenuItemmiExportarASipredCaption&Exportar a SipredEnabledOnClickmiExportarASipredClick  	TMenuItemN9Caption-  	TMenuItemmiAbrirDefinicionCaptionAbrir &Definicion...ShortCutD@OnClickAbrirDefinicion  	TMenuItemmiEditarDefinicionCaption&Editar Definicion...EnabledShortCutE@OnClickEditar  	TMenuItemN5Caption-  	TMenuItem	miPreviewCaption   Presentación Preliminar...EnabledOnClickImprimir  	TMenuItemmiPrintTarjetasCaption&Imprimir Tarjetas...EnabledShortCutP@OnClickImprimir  	TMenuItemmiPrintBaseCaptionImprimir Base de Datos...EnabledOnClickmiPrintBaseClick  	TMenuItemN4Caption-  	TMenuItemExit1Caption&SalirOnClick
SalirClick   	TMenuItemEdicionCaption	   &EdiciónVisible 	TMenuItemPrincipalDeshacerCaption	&DeshacerShortCutZ@  	TMenuItemN8Caption-  	TMenuItemPrincipalCortarCaptionCor&tarShortCutX@OnClickCortar  	TMenuItemPrincipalCopiarCaption&CopiarShortCutC@OnClickCopiar  	TMenuItemPrincipalPegarCaption&PegarShortCutV@OnClickPegar  	TMenuItemN7Caption-  	TMenuItemPrincipalBuscarCaption
&Buscar...ShortCutF@OnClickSearchClick  	TMenuItemPrincipalRemplazarCaptionR&eemplazar...ShortCutR@OnClickReplaceClick   	TMenuItemVer1Caption&Ver 	TMenuItemmiBarradeHerramientasCaptionBarra de &HerramientasChecked	OnClick
miVerClick  	TMenuItemmiFichaCaption&FichaChecked	OnClick
miVerClick  	TMenuItemmiBarradeEstadoCaptionBarra de &EstadoChecked	OnClick
miVerClick  	TMenuItemmiNoActualizarFotoCaptionNo Actualizar FotoOnClick
miVerClick   	TMenuItem
OrdenarPorCaption&Ordenar PorVisible 	TMenuItemSinOrdenar1CaptionSin OrdenarChecked	ShortCut{@OnClickOrdenNinguno  	TMenuItemOrdendeImpresion1Caption   Posición de Impresion	RadioItem	ShortCutp@OnClickOrdenPosicion  	TMenuItemN6Caption-	RadioItem	  	TMenuItemOrdenarPorNombre1CaptionOrdenar Por Nombre	RadioItem	ShortCutq@OnClickOrdenNombre  	TMenuItemOrdenarPorLegajo1CaptionOrdenar Por Legajo	RadioItem	ShortCutr@OnClickOrdenLegajo  	TMenuItemOrdenarPorTarjeta1CaptionOrdenar Por Tarjeta	RadioItem	ShortCuts@OnClickOrdenTarjeta  	TMenuItemOrdenarPorDocumento1CaptionOrdenar Por Documento	RadioItem	ShortCutt@   	TMenuItemHerramientasCaption&HerramientasVisible 	TMenuItemmiInsertarRegistroCaptionInsertar RegistroShortCut-@OnClickmiInsertarRegistroClick  	TMenuItem	miAutoNumCaptionAuto numerar 	TMenuItemmiLegajoCaption&LegajoVisible  	TMenuItemmiLegajoPlusCaption
&Legajo ++Visible  	TMenuItem
miPosicionCaption	&PosicionOnClickmiAutoNumClick  	TMenuItem	miTarjetaCaption&TarjetaOnClickmiAutoNumClick  	TMenuItemmiTarjetaPlusCaption&Tarjeta ++OnClickmiAutoNumClick   	TMenuItem
miAutoCopyCaptionAuto copiar 	TMenuItemmiCopyApellidoCaption	&ApellidoOnClickmiAutoCopyClick  	TMenuItemmiCopyNombreCaption&NombreOnClickmiAutoCopyClick  	TMenuItemmiCopySeccionCaption&SeccionOnClickmiAutoCopyClick  	TMenuItem
miCopyAreaCaptionA&reaOnClickmiAutoCopyClick  	TMenuItem
miCopyFotoCaption&FotoOnClickmiAutoCopyClick   	TMenuItemmiAutoFormatCaptionAuto formato 	TMenuItemmiFormatApellidoCaption	&ApellidoChecked	OnClickmiApNomFormatoClick  	TMenuItemmiFormatNombreCaption&NombreChecked	OnClickmiApNomFormatoClick   	TMenuItemN17Caption-  	TMenuItem
miMarkNoneCaption&No Imprimir NingunoOnClickmiMarkNoneClick  	TMenuItem	miMarkAllCaptionImprimir &TodosOnClickmiMarkAllClick  	TMenuItemN12Caption-  	TMenuItemmiPackCaption&Compactar Base de DatosEnabledOnClickmiPackClick  	TMenuItem	miReindexCaption&Regenerar IndicesEnabledOnClickmiReindexClick  	TMenuItemN13Caption-  	TMenuItemMarcosdeTextoCaptionMarco de Texto 	TMenuItemTransparente1Caption&TransparenteChecked	
GroupIndex	RadioItem	OnClick	SetMColor  	TMenuItemN15Caption-
GroupIndex  	TMenuItemBlanco1Caption&Blanco
GroupIndex	RadioItem	OnClick	SetMColor  	TMenuItemNegro1Caption&Negro
GroupIndex	RadioItem	OnClick	SetMColor  	TMenuItemN16Caption-
GroupIndex  	TMenuItemRojo1Caption&Rojo
GroupIndex	RadioItem	OnClick	SetMColor  	TMenuItemVerde1Caption&Verde
GroupIndex	RadioItem	OnClick	SetMColor  	TMenuItemAzul1Caption&Azul
GroupIndex	RadioItem	OnClick	SetMColor  	TMenuItem	Amarillo1Caption	A&marillo
GroupIndex	RadioItem	OnClick	SetMColor    	TMenuItemAyudaCaptionA&yuda 	TMenuItem
Contenido1Caption	ContenidoOnClick	Contenido  	TMenuItemN14Caption-  	TMenuItem
miAcercaDeCaption&Acerca de ...OnClickmiAcercaDeClick    TSaveDialogDialogoGuardarLeft� Top�   TFindDialogDialogoBuscarOptionsfrDown	frReplace OnFindOnFindLeftTop  TPrintDialogDialogoImprimirOptions
poPageNums	poWarningpoDisablePrintToFile LeftTop�   TReplaceDialogReplaceDialogOnFindOnFindLeft� Top  TTableOrigen	TableName	NUEVA.DBF	TableTypettDBaseLeftpTop�   TTableTablaBeforeDeleteTablaBeforeDelete	TableNameFEDE.dbfLeftpTop�  TFloatFieldTablaPOSICIONDisplayLabel
    PosiciónDisplayWidth	FieldNamePOSICIONMaxValue       �@  TStringFieldTablaLEGAJO	AlignmenttaRightJustifyDisplayLabel LegajoDisplayWidth	FieldNameLEGAJOSize  TStringFieldTablaAPELLIDODisplayLabel	 ApellidoDisplayWidth	FieldNameAPELLIDO	OnGetTextTablaAPELLIDOGetText	OnSetTextTablaAPELLIDOSetText  TStringFieldTablaNOMBREDisplayLabel NombreDisplayWidth	FieldNameNOMBRE	OnGetTextTablaNOMBREGetText	OnSetTextTablaNOMBRESetText  TStringFieldTablaDOCUMENTO	AlignmenttaRightJustifyDisplayLabel
 DocumentoDisplayWidth	FieldName	DOCUMENTOSize  TFloatFieldTablaTARJETADisplayLabel TarjetaDisplayWidth	FieldNameTARJETADisplayFormat00000
EditFormat00000  TStringFieldTablaSECCIONDisplayLabel	    SecciónDisplayWidth	FieldNameSECCIONVisibleSize  TStringField	TablaAREADisplayLabel AreaDisplayWidth	FieldNameAREAVisibleSize  TStringField	TablaFOTODisplayLabel FotoDisplayWidthB	FieldNameFOTOVisibleSizeB   TOpenPictureDialogDialogoAbrirFotoFilter�lmagenes (*.bmp;*.ico;*.emf;*.wmf)|*.bmp;*.ico;*.emf;*.wmf|Bitmaps (*.bmp)|*.bmp|Iconos (*.ico)|*.ico|Enhanced Metafiles (*.emf)|*.emf|Metafiles (*.wmf)|*.wmfOptionsofHideReadOnlyofFileMustExist TitleBuscar Archivo de FotoLeftTop�    