unit ExportSipred;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Db;

type
  TSipred = class(TForm)
    pnArchivo: TPanel;
    lbArchivo: TPanel;
    reArchivo: TEdit;
    pbArchivo: TButton;
    pnPrimer: TPanel;
    lbPrimer: TPanel;
    rbPrimerSector: TRadioButton;
    rbPrimerArea: TRadioButton;
    rbPrimerPropio: TRadioButton;
    rePrimerPropio: TEdit;
    pnSegundo: TPanel;
    lbSegundo: TPanel;
    rbSegundoSector: TRadioButton;
    rbSegundoArea: TRadioButton;
    rbSegundoPropio: TRadioButton;
    reSegundoPropio: TEdit;
    pnTercero: TPanel;
    lbTercer: TPanel;
    rbTercerSector: TRadioButton;
    rbTercerArea: TRadioButton;
    rbTercerPropio: TRadioButton;
    reTercerPropio: TEdit;
    pnOcupacion: TPanel;
    lbOcupacion: TPanel;
    rbOcupacionSector: TRadioButton;
    rbOcupacionArea: TRadioButton;
    rbOcupacionDocumento: TRadioButton;
    reOcupacionPropio: TEdit;
    rbOcupacionPropio: TRadioButton;
    rbOcupacionFoto: TRadioButton;
    pnDatosVarios: TPanel;
    rbVariosPropio: TRadioButton;
    lbDatosVarios: TPanel;
    rbVariosSector: TRadioButton;
    rbVariosArea: TRadioButton;
    rbVariosDocumento: TRadioButton;
    reVariosPropio: TEdit;
    rbVariosFoto: TRadioButton;
    pnVarios: TPanel;
    cbHorario: TCheckBox;
    cbBaja: TCheckBox;
    cbExtras: TCheckBox;
    lbMiscelaneos: TPanel;
    pbCancelar: TButton;
    pbAceptar: TButton;
    DialogoGuardar: TSaveDialog;
    procedure pbArchivoClick(Sender: TObject);
    procedure rbOcupacionClick(Sender: TObject);
    procedure rbVariosClick(Sender: TObject);
    procedure rbPrimerClick(Sender: TObject);
    procedure rbSegundoClick(Sender: TObject);
    procedure rbTercerClick(Sender: TObject);
    procedure pbAceptarClick(Sender: TObject);
  private
    Function GetOcupacion: String;
    Function GetDatosVarios: String;
    Function GetPrimerNivel: String;
    Function GetSegundoNivel: String;
    Function GetTercerNivel: String;
		Function GetCumpleHorario: String;
		Function GetBaja: String;
		Function GetCobraExtras: String;
  public
    Procedure Exportar;
  end;

var
  Sipred: TSipred;

implementation

uses Examinar;

{$R *.DFM}

procedure TSipred.pbArchivoClick(Sender: TObject);
begin
  DialogoGuardar.FileName := reArchivo.Text;
	if DialogoGuardar.Execute then
    reArchivo.Text := DialogoGuardar.FileName;
end;

procedure TSipred.rbOcupacionClick(Sender: TObject);
begin
  reOcupacionPropio.Enabled := rbOcupacionPropio.Checked;
end;

procedure TSipred.rbVariosClick(Sender: TObject);
begin
  reVariosPropio.Enabled := rbVariosPropio.Checked;
end;

procedure TSipred.rbPrimerClick(Sender: TObject);
begin
  rePrimerPropio.Enabled := rbPrimerPropio.Checked;
end;

procedure TSipred.rbSegundoClick(Sender: TObject);
begin
  reSegundoPropio.Enabled := rbSegundoPropio.Checked;
end;

procedure TSipred.rbTercerClick(Sender: TObject);
begin
  reTercerPropio.Enabled := rbTercerPropio.Checked;
end;

procedure TSipred.pbAceptarClick(Sender: TObject);
begin
  if FileExists(reArchivo.Text) then begin
      Case Application.MessageBox('El archivo ya existe. Desea Sobreescribirlo?',
          'Confirmacion',MB_ICONQUESTION or MB_YESNOCANCEL) of
        IDYES: ModalResult := mrOk;
        IDCANCEL: ModalResult := mrCancel;
      end;
  end;
end;

Function TSipred.GetOcupacion;
begin
  With Principal do begin
    if rbOcupacionSector.Checked then
      Result := TablaSECCION.Text
    else if rbOcupacionDocumento.Checked then
      Result := TablaDOCUMENTO.Text
    else if rbOcupacionArea.Checked then
      Result := TablaAREA.Text
    else if rbOcupacionFoto.Checked then
      Result := TablaFOTO.Text
    else
      Result := reOcupacionPropio.Text;
    end;
end;

Function TSipred.GetDatosVarios;
begin
  With Principal do begin
    if rbVariosSector.Checked then
      Result := TablaSECCION.Text
    else if rbVariosDocumento.Checked then
      Result := TablaDOCUMENTO.Text
    else if rbVariosArea.Checked then
      Result := TablaAREA.Text
    else if rbVariosFoto.Checked then
      Result := TablaFOTO.Text
    else
      Result := reVariosPropio.Text;
    end;
end;

Function TSipred.GetPrimerNivel;
begin
  With Principal do begin
    if rbPrimerSector.Checked then
      Result := TablaSECCION.Text
    else if rbPrimerArea.Checked then
      Result := TablaAREA.Text
    else
      Result := rePrimerPropio.Text;
    end;
end;

Function TSipred.GetSegundoNivel;
begin
  With Principal do begin
    if rbSegundoSector.Checked then
      Result := TablaSECCION.Text
    else if rbSegundoArea.Checked then
      Result := TablaAREA.Text
    else
      Result := reSegundoPropio.Text;
    end;
end;

Function TSipred.GetTercerNivel;
begin
  With Principal do begin
    if rbTercerSector.Checked then
      Result := TablaSECCION.Text
    else if rbTercerArea.Checked then
      Result := TablaAREA.Text
    else
      Result := reTercerPropio.Text;
    end;
end;

Function TSipred.GetCumpleHorario;
Begin
	If cbHorario.Checked Then Result := '1' Else Result := '0';
End;

Function TSipred.GetCobraExtras;
Begin
	If cbExtras.Checked Then Result := '1' Else Result := '0';
End;

Function TSipred.GetBaja;
Begin
	If cbBaja.Checked Then Result := '1' Else Result := '0';
End;

procedure TSipred.Exportar;
var
  Sipred: TextFile;
	Actual: TBookMark;
begin
  If Principal.Tabla.RecordCount = 0 Then Begin
    MessageBeep(0);
    Application.MessageBox('La tabla est� vacia','Error', mb_Ok + mb_TaskModal + mb_IconStop)
  End Else
  With Principal Do Begin
    AssignFile(Sipred, reArchivo.Text);
    ReWrite(Sipred);
    WriteLn(Sipred,'!   Archivo de incorporacion de legajos creado automaticamente por T&C');
    WriteLn(Sipred,'!   ------------------------------------------------------------------');
    WriteLn(Sipred,'!');

    DBGrid.Enabled := False;
    Tabla.DisableControls;
    Actual := Tabla.GetBookmark;
    Tabla.First;

    While Not Tabla.EOF Do Begin
      If Trim(TablaLEGAJO.Text) <> '' Then Begin
        WriteLn(Sipred,'L ' + TablaLEGAJO.Text  + ',' + TablaTARJETA.Text + ',' +
                              TablaAPELLIDO.Text + ' ' + TablaNOMBRE.Text + ',' +
                              GetOcupacion + ',' + GetDatosVarios + ',' );
        WriteLn(Sipred,'  ' + GetPrimerNivel + ',' + GetSegundoNivel + ',' +
                              GetTercerNivel + ',' + ',');
        WriteLn(Sipred,'  ' + GetCumpleHorario + ',' + GetCobraExtras + ',' + GetBaja + ',');
        WriteLn(Sipred,'  ' +',,,,,');  //VIG-PRI, HORARIO, VIG-SEC, HORARIO, ER
      end;
      Tabla.Next;
    end;
    Tabla.GotoBookmark(Actual);
    Tabla.FreeBookmark(Actual);
    Tabla.EnableControls;
    DBGrid.Enabled := True;
    CloseFile(Sipred);
    Screen.Cursor := crDefault;
  end;
end;

end.

