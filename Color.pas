unit Color;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Mask;

type
  TForm2 = class(TForm)
    Label11: TLabel;
    Label10: TLabel;
    Label5: TLabel;
    teRellenoRojo: TMaskEdit;
    teRellenoVerde: TMaskEdit;
    teRellenoAzul: TMaskEdit;
    sbRellenoRojo: TScrollBar;
    sbRellenoVerde: TScrollBar;
    sbRellenoAzul: TScrollBar;
    lbRellenoRojo: TLabel;
    lbRellenoVerde: TLabel;
    lbRellenoAzul: TLabel;
    pnRellenoPreview: TPanel;
		procedure RellenoColorBarChange(Sender: TObject);
		procedure RellenoColorEditChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.DFM}


end.
